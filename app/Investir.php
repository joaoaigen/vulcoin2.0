<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Investir extends Model
{
    
    protected $connection= 'mysql';

    protected $table = 'investimentos';
    
    protected $fillable = ['id', 'usuario', 'ico_price', 'qntd_vulcoins', 'criado_em', 'atualizao_em'];
}
