<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Investimento
{
   public static function ativar($username, $valor)
   {
   	$user = DB::table('users')->where('username', $username)->first();
   	
   	if ($user == null)
   	{
   		return false;
   	}
   	
   	$novoValorInvestido = $user->valorinvestido + $valor;
	DB::table('users')->where('username', $username)->update(['ativo' => 1, 'valorinvestido' => $novoValorInvestido]);

	
	app(\App\User::class)->indicacaoUpgrade($user->id, $valor);

	
	return true;
   }
}

?>
