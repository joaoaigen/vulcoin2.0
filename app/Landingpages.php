<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Landingpages extends Model {

    public $timestamps = false;
    protected $fillable = ['user_id', 'email', 'youtube_video','facebook_link','id','twitter_link'];

}
