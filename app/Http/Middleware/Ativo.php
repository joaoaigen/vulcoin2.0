<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Google2FA;
use DB;
class Ativo
{

    private $fileName = 'google2fasecret.key';
    private $name = 'PragmaRX';
    private $email = 'google2fa@pragmarx.com';
    private $secretKey;
    private $keySize = 25;
    private $keyPrefix = '';
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (Auth::check() && Auth::user()->ativo) {
            $user = \Auth::user();
            $fa = auth()->user()->auth2fa;
            $fa_ativo = auth()->user()->fa_ativo;
            $secret = auth()->user()->google2fa_secret;


            if ($fa_ativo == 1) {
                if ($secret == '') {

                    return redirect('/novo-qrcode');
                }

                if ($fa == 0) {
                    return redirect('/auth2fa');
                }
            }
        }

        return $next($request);
    }

    private function getLicence(){
        $arquivo = public_path() . "/uploads/licences.json";
        $fp = fopen($arquivo, "r");
        $conteudo = fread($fp, filesize($arquivo));
        fclose($fp);
        return json_decode($conteudo);
    }
}
