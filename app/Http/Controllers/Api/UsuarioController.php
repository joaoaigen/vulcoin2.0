<?php

namespace App\Http\Controllers\Api;

use Illuminate\Support\Str;
use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\User;

class UsuarioController extends Controller
{
    
    public $successStatus = 200;/**
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */

    public function login() {
        if (Auth::attempt(['username' => request('username'), 'password' => request('password'), 'admin' => request('admin')])) {
            $user = Auth::user();
            $success['token'] = $user->createToken('MyApp')->accessToken;
            return response()->json(['success' => $success], $this->successStatus);
        } else {
            return response()->json(['error' => 'Unauthorised'], 401);
        }
    }
   
    public function details() {
        $user = Auth::user();
        return response()->json(['success' => $user], $this->successStatus);
    }
    
    public function index() {   
        $token = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImUyOTkzNjkzYzQ1ZDQ3ZWExNmIzNWQ2YTNlY2FmODM2MWJlYzhlYzJlMjg3NmViZjNhNGEyN2RiNjIyNTExN2IxZTEzMTE3MjE5NDk0MzU2In0.eyJhdWQiOiIxIiwianRpIjoiZTI5OTM2OTNjNDVkNDdlYTE2YjM1ZDZhM2VjYWY4MzYxYmVjOGVjMmUyODc2ZWJmM2E0YTI3ZGI2MjI1MTE3YjFlMTMxMTcyMTk0OTQzNTYiLCJpYXQiOjE1NjgzODgyMzQsIm5iZiI6MTU2ODM4ODIzNCwiZXhwIjoxNjAwMDEwNjM0LCJzdWIiOiIxNDkzIiwic2NvcGVzIjpbXX0.bavyUS3UeUU8KWFM1bUJJvVe-vkU28Vg15atoqL2utLTpJw5yiq4cicImqxBMi1txdNYLNrSNYz-gYZfUzBj1tb_XXjXDpfi70C_Y0n3dSXl3C-v1hkjHVJGLEMli6NWDLXLVgBNf9zNBn5_aPNTNplrnVUQIqcgT4EeyhQ8QAE1GRhyJhWAWYWlSUouWszkij-J3DCdQeS_b5I0PT7qvq_NwYXKDXAqibzidZgrgF2cmEP-432hg7RswBLYyAr_5mdEvcstAbY5HH8aHcDfmP-ADokZ0f1tkbWjD3gbd-KxjXT6GU35Bvl9oROUHJdACtlQBHVxrDlZTI2vRB0tSYPGsU1PMNBclYN_i03aHEWM0iPJiQ0bllSeEbwlxAPzEDPd4G4R3zKgpP85njZ7AMYJm6NZxYsav_GCVSYLFa4AmZHhH1eondWlYqLhZ2Bcj3WzmVCdLH5T3V-NOlVsZkfXuHBeopRuXFLyQIvkRF96qUZv1FzfwJzft0Ovl0hfdILPyJm1P3iV1ydbpDTix3YW5-UWJsYfqIVLozPaO1g0kCWSHGSGspWwLm8UEyN4t76nRLx_j3hfaREl6o3T_1GrdvBrjJ6qk6MbzlNYBeC-sHQmO43cFXGVwDFtCHAyX2t6yOr8PbExmQ7t5HIAhP7NMYgIhahlrwa5oLcDSw0';
        $client = new GuzzleHttp\Client(['base_uri' => 'http://bo.worldcryptocoin.io/api/']);
        
        $headers = [
            'Authorization' => 'Bearer ' . $token,
            'Accept' => 'application/json',
        ];
        
        $response = $client->request('POST', 'details', [
            'headers' => $headers
        ]);
        
        dd($response);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
