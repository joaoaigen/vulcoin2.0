<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Http\Requests;
use Illuminate\Http\Request;
use App\User;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class TesteController extends Controller {

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    
    /**
     * Show the application dashboard.
     *
     * @return Response
     */
    public function teste_graduacao() {
        var_dump(\Auth::user()->requisitosGraduacao(\Auth::user()->id,3));
    }
    public function show($param) {
        
    }
}
