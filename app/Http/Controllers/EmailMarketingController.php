<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;

class EmailMarketingController extends Controller {

    //
    public function getSend(Request $request) {
        $users = \App\User::where($request->coluna, $request->valorColMkt)->get();
        foreach ($users as $value) {
            $this->enviarEmail($value->id, $request->tituloMkt, $request->conteudoMkt);
        }
        echo '    <div class="alert alert-success fade in">
                    Operação realizada com sucesso.
                 </div>';
    }

}
