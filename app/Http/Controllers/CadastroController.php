<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Pacote;
use App\Referrals;
use App\User;
use App\Visitas;
use Illuminate\Http\Request;
use Validator;
use DB;

class CadastroController extends Controller {


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($indicacao, Request $request) {

        $indicador = User::where('username', $indicacao)->orWhere('email', $indicacao)->first();
        $pacotes = Pacote::all();



        Visitas::create(['ip' => $request->ip(), 'user_id' => $indicador->id]);

        return view('painel.auth.register', compact('indicador', 'pacotes'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $indicacao = $request->indicador;
        $indicador = User::where('username', $request->indicador)->first();


        if ($indicador) {
            $reffer = new Referrals();
            $systemId = $reffer->searchSystemId($indicador->id, $indicador->direcao);

            if ($request->cadastro_exterior == 1) {
                $validator = Validator::make($request->all(), [
                            'name' => 'required|max:255',
                            'email' => 'required|email|max:255|unique:users',
                            'username' => 'required|max:20|unique:users',
                            'password' => 'required|confirmed|min:6',
                            'sexo' => 'required',
                            'telefone' => 'required',
                ]);
            } else {
                $validator = Validator::make($request->all(), [
                            'name' => 'required|max:255',
                            'email' => 'required|email|max:255|unique:users',
                            'username' => 'required|max:20|unique:users',
                            'password' => 'required|confirmed|min:6',
                            'sexo' => 'required',
                            'telefone' => 'required',
                ]);
            }

            if ($validator->fails()) {
                return redirect('/cadastro/' . $indicacao)
                                ->withErrors($validator)
                                ->withInput();
            }

            $data = $request->all();

            $quantidade = \DB::table('users')->where('telefone', '=', intval($data['telefone']))->select(\DB::raw('COUNT(telefone) as quantidade'))->first();

            if($quantidade->quantidade == 3){
                return redirect('/cadastro/' . $indicacao)->with('status', 400)->with('msg', 'Ja existem 3 contas com esse telefone.')->withInput();;
            }

            $photo = asset('/img') . '/avatar-' . $data['sexo'] . '.png';

            $ativo = 0;
            $dataAtivacao = date('Y-m-d');

            $curl = curl_init();
            curl_setopt_array($curl, [
                 CURLOPT_RETURNTRANSFER => 1,
                 CURLOPT_URL => "http://64.227.0.214/api/getnewaddress?account=".$data['username']
            ]);
            $result = json_decode(curl_exec($curl));
            curl_close($curl);

            $user = User::create([
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'username' => $data['username'],
                        'password' => bcrypt($data['password']),
                        'sexo' => $data['sexo'],
                        'telefone' => $data['telefone'],
                        'pai_id' => $indicador->id,
                        'bitzpayer_id' => '',
                        'saldo_vulcoins' => 0,
                        'direcao' => $indicador->direcao,
                        'pacote' => 1,
                        'total_div' => 12,
                        'photo' => $photo,
                        'ativo' => $ativo,
                        'dataAtivacao' => $dataAtivacao,
                        'carteira' => isset($result->erro) ? $result->erro : '',
                        'pub_key' => isset($result->pub_key) ? $result->pub_key : '',
                        'mensalidade_ativo' => 1,
                        'mensalidade' => date("Y-m-d", strtotime("+31 days"))
            ]);

            $cryptoUser = [
	            'name' => $data['name'],
	            'email' => $data['email'],
	            'username' => $data['username'],
	            'password' => bcrypt($data['password']),
	            'mobile' => $data['telefone'],
	            'refer' => $indicador->id,
	            'status' => 1,
	            'balance' => '0',
	            'tauth' => 0,
	            'tfver' => 1,
	            'emailv' =>  1,
	            'smsv' =>  1,
        	];

            DB::connection('crypto')->table('users')->insert($cryptoUser);

            if ($user) {
                \App\Landingpages::create(['pilar_1_titulo' => 'Seriedade', 'pilar_1_conteudo' => 'Uma equipe competente, composta de pessoas cujo objetivo é oferecer a melhor oportunidade para você',
                    'pilar_2_titulo' => 'Visão', 'pilar_2_conteudo' => 'Qualificar pessoas a serem promotores de benefícios para sua família e para sociedade', 'pilar_3_titulo' => 'Resultado', 'pilar_3_conteudo' => 'Criamos um negócio perfeito, com o 			objetivo de trazer resultados reais e mudança de vida para todos nossos associado',
                    'pilar_4_titulo' => 'Amor',
                    'pilar_4_conteudo' => 'Realmente, nós temos paixão pelo que fazemos: oferecer uma mudança real e completa à vida das pessoas '
                    , 'video_titulo' => 'A OPORTUNIDADE', 'video_descricao' => 'Com a implementação de dois grandes mercados, iniciamos um sistema de comissionamento inteligente e sustentável, onde oferecemos a oportundiade de transformação financeira 	e social aos nossos associados.
Através de nossos produtos, podemos oferecer resultados reais, com segmentações precisas, custo baixo e ótimo custo x benefício aos nossos clientes e parceiros. Somos dententores de uma base de dados com aproximadamente quatro milhões de clientes ativos, que estão localizados em mais de quarenta países ao redor do mundo.',
                    'youtube_video' => 'https://www.youtube.com/embed/IJn9Dq0xdKg', 'user_id' => $user->id, 'email' => $user->email]);

                $reffer->user_id = $user->id;
                $reffer->pai_id = $indicador->id;
                $reffer->system_id = $systemId['pai_id'];
                $reffer->direcao = $systemId['direcao'] ? $systemId['direcao'] : $indicador->direcao;
                $reffer->save();

                foreach (\App\anuncios::where('status', 1)->get() as $value) {
                    $validade = date('y-m-d', strtotime('+ 5 days', strtotime(date('Y-m-d'))));
                    \DB::table('tarefas')->insert(['user_id' => $user->id, 'anuncio_id' => $value->id, 'data' => $validade]);
                }

                if (\Auth::login($user)) {
                    $userInfo = User::where('id', \Auth::user()->id)->first();
                    session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name']]);
                    $dataMail['subject'] = 'Cadastro  efetuado com sucesso!';
                    $dataMail['content'] = "<h5>Estamos muito felizes por seu cadastro!</h5>
                    <b>
	          Seus dados de acesso são:<br>
	          Login: " . $userInfo['username'] . "<br>
	          Email: " . $userInfo['email'] . "<br>
	          Página de login: <a href='" . url('painel/login') . "'>Login</a>";

                    $this->enviarEmail(\Auth::user()->id, 'Cadastro  efetuado com sucesso!', $dataMail['content']);
                    return redirect('/painel');
                } else {

                    $userInfo = User::where('id', \Auth::user()->id)->first();
                    session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name']]);
                    $dataMail['subject'] = 'Cadastro  efetuado com sucesso!';
                    $dataMail['content'] = "<h5>Estamos muito felizes por seu cadastro!</h5>
                    <b>
	          Seus dados de acesso são:<br>
	          Login: " . $userInfo['username'] . "<br>
	          Email: " . $userInfo['email'] . "<br>
	          Página de login: <a href='" . url('painel/login') . "'>Login</a>";

                    $this->enviarEmail(\Auth::user()->id, 'Cadastro  efetuado com sucesso!', $dataMail['content']);

                    return redirect('/painel/home');
                }
            } else {
                abort(404);

                return false;
            }
        }
    }

}
