<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Ico;

class IcoController extends Controller
{
    public function index(){

        return view('admin.pages.ico.index');
    }

    public function create(){
        return view('admin.pages.ico.create');
    }

    public function save(Request $request){        
        $ico = Ico::all()->last();
        $ico->price = $request->get('price');
        $ico->save();
        return redirect()->action('IcoController@index');
    }
}
