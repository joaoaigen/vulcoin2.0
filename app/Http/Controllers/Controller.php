<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public $valorAnterior = 0;
    function calculaFrete($cod_servico, $cep_destino, $peso, $altura = '2', $largura = '11', $comprimento = '16', $valor_declarado = '18') {
        return 67;
        try {
            # Código dos Serviços dos Correios
            # 41106 PAC sem contrato
            # 40010 SEDEX sem contrato
            # 40045 SEDEX a Cobrar, sem contrato
            # 40215 SEDEX 10, sem contrato

            $correios = "http://ws.correios.com.br/calculador/CalcPrecoPrazo.aspx" .
                    "?nCdEmpresa=&sDsSenha=&sCepOrigem=36570000" .
                    "&sCepDestino=" . $cep_destino . "&nVlPeso=" . $peso .
                    "&nCdFormato=1&nVlComprimento=" . $comprimento .
                    "&nVlAltura=" . $altura . "&nVlLargura=" . $largura .
                    "&sCdMaoPropria=n&nVlValorDeclarado=" . $valor_declarado .
                    "&sCdAvisoRecebimento=n&nCdServico=" . $cod_servico .
                    "&nVlDiametro=0&StrRetorno=xml";

            $xml = simplexml_load_file($correios);
            if ($xml->cServico->Erro == '0') {

                return $xml->cServico->Valor;
            } else {
                return false;
            }
        } catch (\Exception $ex) {
            return false;
        }
    }

    function getAmount($money) {
        $cleanString = preg_replace('/([^0-9\.,])/i', '', $money);
        $onlyNumbersString = preg_replace('/([^0-9])/i', '', $money);

        $separatorsCountToBeErased = strlen($cleanString) - strlen($onlyNumbersString) - 1;

        $stringWithCommaOrDot = preg_replace('/([,\.])/', '', $cleanString, $separatorsCountToBeErased);
        $removedThousendSeparator = preg_replace('/(\.|,)(?=[0-9]{3,}$)/', '', $stringWithCommaOrDot);

        return (float) str_replace(',', '.', $removedThousendSeparator);
    }

    public function enviarEmail($user_id, $subject, $content) {
        try {
            $userInfo = \App\User::where('id', $user_id)->first();
            session(['sendEmail' => $userInfo['email'], 'sendName' => $userInfo['name'], 'subject' => $subject]);
            $dataMail['subject'] = $subject;
            $dataMail['content'] = $content;
            Mail::send('painel.auth.emails.email', $dataMail, function ($message) {
                $message->from(env('MAIL_ADDRESS'), env('MAIL_NAME'));
                $message->to(session('sendEmail'), session('sendName'))->subject(session('subject'));
                ///$message->to('caiquemarcelinosouza@gmail.com', session('sendName'))->subject(session('subject'));
            });
        } catch (\Exception $exc) {
            return false;
        }
    }

    public function usdFormat($value) {

        return number_format($this->getAmount($value), 2, '.', '');
    }
}
