<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.2/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\config;
use Validator;
use Symfony\Component\DomCrawler\Form;
use App\graduacoes;
use App\User;

/**
 * Class HomeController
 * @package App\Http\Controllers
 */
class vouchersAnunciosController extends Controller {

    protected function validate_request($request, $mode = 1) {
        $data = $request->all();
        //validations
        if ($mode == 1) {
            $validator = Validator::make($request->all(), [
                        'key' => 'required', //chave de autenticação
                        'qntd' => 'required|integer', //código do voucher
                        'id' => 'required|integer|exists:users' //id do usuário
            ]);
        } elseif ($mode == 2) {
            $validator = Validator::make($request->all(), [
                        'key' => 'required',
                        'code_voucher' => 'required|min:30',
            ]);
        } elseif ($mode == 3) {
            $validator = Validator::make($request->all(), [
                        'key' => 'required',
                        'id' => 'required|integer',
            ]);
        }

        $retorno = array();
        $retorno['sucess'] = 0;
        if ($validator->fails()) {
            $erros = '';
            foreach ($validator->errors()->getMessages() as $key) {
                $erros.=$key[0] . "  ";
            }
            $retorno['error'] = $erros;
        } else {

            if ($data['key'] == env('KEY_VOUCHER_API')) {
                $retorno['sucess'] = 1;
            } else {
                $retorno['error'] = 'Falha na autenticação';
            }
        }

        return $retorno;
    }

    function create(Request $request) {
        $data = $request->all();
        $retorno = array();
        $retorno['sucess'] = 0;
        if ($this->validate_request($request)['sucess'] == 1) {
            $qntd = $data['qntd'];
            if ($qntd > 100) {
                $retorno['error'] = 'O limite é 100 por vez.';
            } else {
                for ($i = 1; $i <= $qntd; $i++) {
                    $code = md5(time() . $data['id'] . rand(0, 999999999));
                    \DB::table('vouchers_site')->insert(['user_id' => $data['id'], 'code' => $code]);
                }

                $retorno['error'] = '';
                $retorno['sucess'] = 1;
            }
            echo json_encode($retorno);
        } else {
            echo json_encode($this->validate_request($request));
        }
    }

    function delete(Request $request) {
        $data = $request->all();
        $retorno = array();
        $retorno['error'] = '';
        $retorno['sucess'] = 0;
        if ($this->validate_request($request, 2)['sucess'] == 1) {
            $vouchers_anuncios = \DB::table('vouchers_site')->where('code', $data['code_voucher']);

            if ($vouchers_anuncios->count() > 0) {
                $vouchers_anuncios->delete();
                $retorno['error'] = '';
                $retorno['sucess'] = 1;
            } else {
                $retorno['error'] = 'Código não encontrado.';
            }
            echo json_encode($retorno);
        } else {
            echo json_encode($this->validate_request($request, 2));
        }
    }

    function info(Request $request) {
        $data = $request->all();
        $retorno = array();
        $retorno['error'] = '';
        $retorno['sucess'] = 0;
        if ($this->validate_request($request, 2)['sucess'] == 1) {
            $vouchers_anuncios = \DB::table('vouchers_site')->where('code', $data['code_voucher']);

            if ($vouchers_anuncios->count() > 0) {
                $retorno['error'] = '';
                $retorno['sucess'] = 1;
            } else {
                $retorno['error'] = 'Código não encontrado.';
            }
            echo json_encode($retorno);
        } else {
            echo json_encode($this->validate_request($request, 2));
        }
    }

    public function lista(Request $request) {
        $data = $request->all();
        $retorno = array();
        $retorno['error'] = '';
        $retorno['sucess'] = 0;
        if ($this->validate_request($request, 3)['sucess'] == 1) {
            $vouchers_anuncios = \DB::table('vouchers_site')->where('user_id', $data['id']);

            if ($vouchers_anuncios->count() > 0) {
                $retorno['vouchers'] = $vouchers_anuncios->get();
                ;
                $retorno['error'] = '';
                $retorno['sucess'] = 1;
            } else {
                $retorno['error'] = 'Código não encontrado.';
            }
            echo json_encode($retorno);
        } else {
            echo json_encode($this->validate_request($request, 3));
        }
    }

}
