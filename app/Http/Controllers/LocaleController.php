<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Session;
use Cookie;

class LocaleController extends Controller {

    public function setLocale(Request $request) {
        setcookie('lang', $request->input('lang'), time() + (10 * 365 * 24 * 60 * 60));
        return redirect()->back();
    }

}

?>