<?php

namespace App\Http\Controllers\Admin;

use App\Suporte;
use App\SuporteMensagem;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class SuporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resolvidos = Suporte::where('status', 2)->get();
        $em_andamaneto = Suporte::where('status', 1)->get();
        $em_aberto = Suporte::where('status', 0)->get();

        return view('admin.pages.suporte')
            ->with('resolvido', $resolvidos != null ? $resolvidos : null)
            ->with('andamento', $em_andamaneto != null ? $em_andamaneto : null)
            ->with('aberto', $em_aberto != null ? $em_aberto : null);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {
        $suporte = Suporte::where('id', $request->mensagem)->first();
        $resolvidos = Suporte::where('status', 2)->get();
        $em_andamaneto = Suporte::where('status', 1)->get();
        $em_aberto = Suporte::where('status', 0)->get();
        $mensagem = SuporteMensagem::where('id_suporte', $request->mensagem)->get();

        return view('admin.pages.suporte')
            ->with('mensagens', $mensagem)
            ->with('suporte', $suporte)
            ->with('user_id', $suporte->user_id)
            ->with('suporte_user', $suporte->suporte_user)
            ->with('resolvido', $resolvidos != null ? $resolvidos : null)
            ->with('andamento', $em_andamaneto != null ? $em_andamaneto : null)
            ->with('aberto', $em_aberto != null ? $em_aberto : null);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendAjax(Request $request)
    {
        $dados = $request->all();

        $user = User::where('id', $dados['id'])->first();

        $suporte = Suporte::where('id', $dados['id_suporte'])->update([
            'suporte_user' => $dados['id'],
        ]);

        $mensagem = SuporteMensagem::create([
            'id_suporte' => $dados['id_suporte'],
            'mensagem' => $dados['body'],
            'id_user' => $dados['id'],
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return response()->json([
            'mensagem' => $mensagem->mensagem,
            'updated_at' => $mensagem->updated_at,
            'name' =>  $user->username,
            'photo' => $user->photo
        ]);
    }
}
