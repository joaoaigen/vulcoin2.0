<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Http\Request;

class AvisosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $avisos = \DB::table("avisos")->get();
        return view('admin.pages.avisos', compact('avisos'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.pages.avisos.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
       $dados=\DB::table("avisos")->where("id",$id)->first();
        return view('admin.pages.avisos.edit', compact('dados'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
  

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $toUpdate['status']=$request->status;
                $toUpdate['conteudo']=$request->conteudo;

        \DB::table("avisos")->where("id",$id)->update($toUpdate);
              return <<<EOL
                 <div class="alert alert-success fade in">
                     Sucesso.
                 </div>
EOL;
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (avisos::destroy($id)) {
            return <<<EOL
                 <div class="alert alert-success fade in">
                     Material Removido
                 </div>
EOL;
        } else {
            return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    private function getExtension($link)
    {
        return pathinfo($link)['extension'];
    }
}
