<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Password;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class PasswordController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */

    use ResetsPasswords;

    protected $redirectPath = '/painel';
    protected $subject = 'Seu link de redefinição de senha';
    /**
     * Create a new password controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    public function showResetFormUser(Request $request, $token = null)
    {       
        if (is_null($token)) {
            return $this->showLinkRequestFormUser();
        }
        
        $email = $request->input('email');

        if (view()->exists('painel.auth.passwords.reset')) {
            return view('painel.auth.passwords.reset')->with(compact('token', 'email'));
        }

        return view('painel.auth.reset')->with('token', $token);
    }

    public function showLinkRequestFormUser()
    {
        if (view()->exists('painel.auth.passwords.email')) {
            return view('painel.auth.passwords.email');
        }

        return view('painel.auth.password');
    }

    public function postEmailUser(Request $request)
    {
       $user = DB::table('users')->where('email', '=', $request->email)->first();
       
       if(isset($user->id)){
           $codigo = strtoupper(substr(bin2hex(random_bytes(8)), 1));
           DB::table('users')->where('id', '=', $user->id)->update([
               'codigo_login' => $codigo
           ]);
          
            $headers = "MIME-Version: 1.1\r\n";
            $headers .= "Content-type: text/plain; charset=UTF-8\r\n";
            $headers .= "From: noreply@bo.worldcryptocoin.io\r\n"; // remetente       
            $envio = mail($user->email, "Resetar senha", "Acesse o link para resetar sua senha: https://bo.worldcryptocoin.io/painel/password/reset/" . $codigo, $headers);
            
            if($envio == true){
                return back()->with('status', 200)->with('msg', 'E-mail enviado com sucesso!');
            }
        }else{
           return redirect('/painel/password/email')->withErrors(['Esse e-mail não existe!']);
       }
       
    }

    public function resetUser(Request $request)
    {
        $this->validate($request, [
            'token' => 'required',
            'email' => 'required|email',
            'password' => 'required|confirmed|min:6',
        ]);
        
        $data = $request->all();
        
        $usr = DB::table('users')->where('codigo_login', '=', $data['token'])->first();
        
       
        if(isset($usr->id)){
           $att = DB::table('users')->where('id', '=', $usr->id)->update([
                'password' => bcrypt($data['password'])
            ]);
           
           if(Auth::attempt(['username' => $usr->username, 'password' => $data['password'], 'admin' => 0])){
               return redirect('/painel/home')->with('status', 200)->with('msg',  'Senha atualizada com sucesso!');
           }
           
        }else{
            return back()->withErrors(['Código inválido!']);
        }
       
    }

}
