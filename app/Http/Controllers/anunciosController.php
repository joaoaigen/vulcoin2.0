<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Trade;
use App\Http\Controllers\Controller;
use Validator;
use App\User;
use App\pacoteViews;
use App\Pacote;
use App\anuncios;

class anunciosController extends Controller {

    public function index() {
        $usr = new User();

        $pacotes = pacoteViews::all();
        return view('admin.pages.pacote_views', compact('pacotes'));
    }

    public function create() {
        return view('admin.pages.pacotesViews.create');
    }

    public function view($id) {
        $dados['dados'] = anuncios::where('id', $id)->first();
        return view('admin.pages.anuncios.edit', $dados);
    }

    public function view2($id) {
        $dados['dados'] = anuncios::where('id', $id)->where('user_id', \Auth::user()->id)->first();
        return view('painel.pages.anuncios.edit', $dados);
    }

    public function store(Request $request) {
        $pacote = new pacoteViews();

        $pacote->status = $request->status;
        $pacote->nome = $request->nome;
        $pacote->views = $request->views;
        $pacote->valor = $request->valor;

        $data = $request->all();

        $valida = [
            'status' => 'required',
            'valor' => 'required',
            'views' => 'integer|required'
        ];

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            if ($pacote->save()) {
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Pacote Adicionado
                 </div>
EOL;
            } else {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
            }
        }
    }

    public function salvar2() {
        $data = \Input::all();
        $anuncio = new anuncios();
        $usr = new User();

        $valida = [
            'id' => 'required',
            'nome' => 'required|max:255',
            'url' => 'required|max:255',
            'acao' => 'required|in:1,2',
        ];

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];
            $dataSave['nome'] = $data['nome'];
            $dataSave['url'] = $data['url'];
            $dataSave['acao'] = $data['acao'];
            $dataSave['status'] = 2;
            $dataAnuncio = anuncios::where('id', $id)->first();
            $resANuncio = anuncios::where('id', $id)->where('user_id', \Auth::user()->id)->where('status', 3)->update($dataSave);

            $userData = $usr->userInfo($dataAnuncio['user_id']);
            $pacoteData = pacoteViews::where('id', $dataAnuncio['pacote_id'])->first();
            $valor = $userData['carteira_b'] - $pacoteData['valor'];
            User::where('id', $dataAnuncio['user_id'])->update(['carteira_b' => $valor]);
            if ($resANuncio) {
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Anúncio reenviado com sucesso.
                 </div>
EOL;
            } else {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                     Ocorreu um .
                 </div>
EOL;
            }
        }
    }

    public function salvar() {
        $data = \Input::all();
        $anuncio = new anuncios();
        $usr = new User();
        if ($data['status'] == 3) {
            $valida = [
                'id' => 'required',
                'nome' => 'required|max:255',
                'url' => 'required|max:255',
                'visualizacoes_restante' => 'required',
                'status' => 'required|in:1,2,3,4',
                'acao' => 'required|in:1,2',
                    //'anotacao' => 'required'
            ];
        } else {
            $valida = [
                'id' => 'required',
                'nome' => 'required|max:255',
                'url' => 'required|max:255',
                'visualizacoes_restante' => 'required',
                'acao' => 'required|in:1,2',
                'status' => 'required|in:1,2,3,4',
            ];
        }

        $validator = Validator::make($data, $valida);

        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $id = $data['id'];
            $dataSave['nome'] = $data['nome'];
            $dataSave['url'] = $data['url'];
            $dataSave['visualizacoes_restante'] = $data['visualizacoes_restante'];
            $dataSave['acao'] = $data['acao'];
            $dataSave['status'] = $data['status'];

            $dataSave['anotacao'] = @$data['anotacao'];
            $dataAnuncio = anuncios::where('id', $id)->first();
            anuncios::where('id', $id)->update($dataSave);
            $dataAnuncio2 = anuncios::where('id', $id)->first();
            if ($dataAnuncio2['status'] <> 1 and $data['status'] == 4) {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                    Só é possivel pausar anúncios que estão publicados.
                 </div>
EOL;
            }
            $totalTarefas = \DB::table('tarefas')->where('anuncio_id', $id)->count();
            if ($dataAnuncio2['status'] == 1 and $totalTarefas == 0) {
                $users = User::all();
                foreach ($users as $key => $value) {
                    $validade = date('y-m-d', strtotime('+ 3 days', strtotime(date('Y-m-d'))));
                    \DB::table('tarefas')->insert(['user_id' => $value['id'], 'anuncio_id' => $id, 'data' => $validade]);
                }
            }

            if ($dataAnuncio['status'] == 2 and $data['status'] == 3) {
                $userData = $usr->userInfo($dataAnuncio['user_id']);
                $pacoteData = pacoteViews::where('id', $dataAnuncio['pacote_id'])->first();
                $valor = $pacoteData['valor'] + $userData['carteira_b'];
                User::where('id', $dataAnuncio['user_id'])->update(['carteira_b' => $valor]);
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Anúncio Atualizado com sucesso. {$pacoteData['valor']} ponto(s) foram devolvidos.
                 </div>
EOL;
            } else {
                return <<<EOL
                 <div class="alert alert-success fade in">
                     Anúncio Atualizado com sucesso.
                 </div>
EOL;
            }
        }
    }

    public function criarAnuncio(Request $request) {

        $data = $request->all();
        $anuncio = new anuncios();
        $valida = [
            'nome' => 'required|max:255',
            'pacotes_views' => 'required|max:255',
            'link_anuncio' => 'required|url',
            'anuncio_acao' => 'required|in:1,2',
            'check' => 'required'
        ];

        $validator = Validator::make($data, $valida);
        if ($validator->fails()) {
            $res = $validator->errors()->getMessages();
            $erros = '';
            foreach ($res as $key) {
                $erros.=$key[0] . '<br>';
            }
            return <<<EOL
                 <div class="alert alert-danger fade in">
                     $erros
                 </div>
EOL;
        } else {
            $pontos = \Auth::user()->saldo;
            $pacoteData = pacoteViews::where('id', $data['pacotes_views'])->where('status', 1)->first();
            if (isset($pacoteData['id']) and ! empty($pacoteData['id'])) {
                if ($pontos >= $pacoteData['valor']) {
                    /* Array ( [pin_anuncio] => dsssssssssss [pacotes_views] => 1 [link_anuncio] =>
                     *  http://localhost:8000/painel/home [anuncio_acao] => 1 [check] => on )
                     */

                    $usr = new User();

                    anuncios::create(['visualizacoes_restante' => $pacoteData['views'], 'pacote_id' => $pacoteData['id'], 'nome' => $data['nome'], 'url' => $data['link_anuncio'], 'acao' => $data['anuncio_acao'], 'status' => 2, 'user_id' => \Auth::user()->id, 'visualizacoes' => $pacoteData['views']]);
                    $usr->removeSaldo(\Auth::user()->id, $pacoteData['valor'], 'Compra de anúncio');
                    return <<<EOL
                 <div class="alert alert-success fade in">
                     Seu anúncio encontra-se pendete de aprovação.Caso ele não seja aprovado os pontos serão devolvidos.
                 </div>
EOL;
                } else {
                    return <<<EOL
                 <div class="alert alert-danger fade in">
                     Saldo insuficiente
                 </div>
EOL;
                }
            } else {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                     O pacote não foi encontrado
                 </div>
EOL;
            }
        }
    }

    public function meus_anuncios() {
        if (isset($_GET['operacao']) and isset($_GET['id'])) {
            $id = $_GET['id'];
            $operacao = $_GET['operacao'];
            if ($operacao == 1) {
                $res = anuncios::where('id', $_GET['id'])->where('id', $id)->where('status', 4)->where('user_id', \Auth::user()->id)->update(['status' => 1]);
                if ($res) {
                    return redirect('/painel/meus-anuncios')
                                    ->with('success', ' Anúncio republicado  com sucesso!');
                } else {
                    return redirect('/painel/meus-anuncios')
                                    ->withErrors(['Ocorreu um erro!']);
                }
            }
            if ($operacao == 2) {
                $res = anuncios::where('id', $_GET['id'])->where('id', $id)->where('status', 1)->where('user_id', \Auth::user()->id)->update(['status' => 4]);
                if ($res) {
                    return redirect('/painel/meus-anuncios')
                                    ->with('success', ' Anúncio pausado  com sucesso!');
                } else {
                    return redirect('/painel/meus-anuncios')
                                    ->withErrors(['Ocorreu um erro!']);
                }
            }
        }
        return view('painel.pages.anuncios');
    }

    public function visualizar_site($id) {
        $tarefa = \DB::table('tarefas')->where('id', $id)->where('status', 0)->where('user_id', \Auth::user()->id)->first();
        if (!isset($tarefa->anuncio_id)) {
            return redirect('/painel/visitar-anuncios')
                            ->withErrors(['Não foi possivel encontrar esse anúncio.']);
        }
        $anuncio = anuncios::where('id', $tarefa->anuncio_id)->first();
        if ($anuncio['status'] <> 1 or $anuncio['acao'] <> 1) {
            return redirect('/painel/visitar-anuncios')
                            ->withErrors(['Não foi possivel encontrar esse anúncio.']);
        }
        return view('painel.pages.anuncios.visualizar', compact('anuncio'));
    }

    public function validarVisualizacao(Request $request) {
        $data = $request->all();
        if ($request->ajax()) {
            @$json = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LdPNRsTAAAAAOBmjXQWhV-gSHqo6BUg_awr4492&response=" . $_GET['g-recaptcha-response']);
            $res = json_decode($json);

            if ($res->success != 1 or $res->success == '') {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                     Desafio inválido
                 </div>
EOL;
            } else {
                $tarf = \DB::table('tarefas')->where('id', $data['id'])->where('status', 0)->where('user_id', \Auth::user()->id);
                $tarefa = $tarf->first();
                if (isset($tarefa->anuncio_id)) {
                    $tarf->update(['status' => 1]);
                    $anuncio = anuncios::where('id', $tarefa->anuncio_id);
                    if ($anuncio->first()['status'] <> 1) {
                        return redirect('/painel/visitar-anuncios')
                                        ->withErrors(['Não foi possivel encontrar esse anúncio.']);
                    }
                    $views = $anuncio->first()['visualizacoes_restante'] - 1;
                    if ($anuncio->update(['visualizacoes_restante' => $views])) {
                        return <<<EOL
                 <div class="alert alert-success fade in">
                     Visita realizada com sucesso
                 </div>
EOL;
                    } else {

                        return <<<EOL
                 <div class="alert alert-danger fade in">
                     Ocorreu um erro.
                 </div>
EOL;
                    }
                } else {

                    return <<<EOL
                 <div class="alert alert-danger fade in">
                     Tarefa não encontrada.
                 </div>
EOL;
                }
            }
        }
    }

    public function validarVisualizacao2(Request $request) {
        $data = $request->all();
        if ($request->ajax()) {
            @$json = file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6LdPNRsTAAAAAOBmjXQWhV-gSHqo6BUg_awr4492&response=" . $_GET['g-recaptcha-response']);
            $res = json_decode($json);
            if ($res->success != 1 or $res->success == '') {
                return <<<EOL
                 <div class="alert alert-danger fade in">
                     Desafio inválido
                 </div>
EOL;
            } else {
                $tarf = \DB::table('tarefas')->where('id', $data['id'])->where('status', 0)->where('user_id', \Auth::user()->id);
                $tarefa = $tarf->first();
                if (isset($tarefa->anuncio_id)) {
                    $tarf->update(['status' => 1]);
                    $anuncio = anuncios::where('id', $tarefa->anuncio_id);
                    if ($anuncio->first()['status'] <> 1) {
                        return redirect('/painel/visitar-anuncios')
                                        ->withErrors(['Não foi possivel encontrar esse anúncio.']);
                    }
                    $views = $anuncio->first()['visualizacoes_restante'] - 1;
                    if ($anuncio->update(['visualizacoes_restante' => $views])) {
                        return <<<EOL
                 <div class="alert alert-success fade in">
                     Visita realizada com sucesso
                 </div>
EOL;
                    } else {

                        return <<<EOL
                 <div class="alert alert-danger fade in">
                     Ocorreu um erro.
                 </div>
EOL;
                    }
                } else {

                    return <<<EOL
                 <div class="alert alert-danger fade in">
                     Tarefa não encontrada.
                 </div>
EOL;
                }
            }
        }
    }

}

?>
