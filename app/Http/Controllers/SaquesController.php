<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\User;
use App\extratos;
use App\Saque;
use Validator;
use App\config;

class SaquesController extends Controller {

    public function index() {
        $saques = Saque::all();
        $usuarios = new User();
        return view('admin.pages.saque', compact('saques', 'usuarios'));
    }

    public function indexUser() {
        $saques = Saque::where('user_id', \Auth::user()->id);
        $usuarios = new User();
        return view('painel.pages.saque', compact('saques', 'usuarios'));
    }

    public function cancelarSaque($id){
        $log = \App\SaqueLog::find($id);
        $log->mensagem = "Saque cancelado";
        $log->status = "0";
        $log->save();

        return redirect()
                    ->route("/admin/saque/");
    }

    public function history(){
        return view('painel.pages.extratoSaque');
    }

    public function aprovarSaque(Request $request){
        $log = \App\SaqueLog::find($request->id);
        $user = User::find($log->user_id);

        // Registrando log
        $log->mensagem = "Saque Aprovado";
        $log->comprovante = $request->comprovante;
        $log->status = "2";
        $log->save();

        // Registrando no extrado

        \App\extratos::criarExtrato($request->id, $log->saque_realizado, $log->mensagem, $request->comprovante, 5, $admin = 1);

        //Dando baixa no saldo
        $user->saldo_vulcoins = $user->saldo_vulcoins - $log->saque_realizado;
        $user->block_saque = date("Y-m-d H:i:s", strtotime("+7 days"));
        $user->save();

        return redirect()
            ->route("/admin/saque/");
    }

    public function solicitaSaque() {
        $config = new config();
        $config = $config->getConfig();
        $usr = new User();
        $usr->verificarPin($_GET['pin']);
        $valor = $_GET['valor'];
        $valor2 = str_replace(array('R', '$', ',', '.'), '', $valor);
        $valor = $this->getAmount($valor);
        $saldo = \Auth::user()->getSaldo();
        $saldo = $this->getAmount($saldo);
        $today = date("Y-m-d");
        if (\Auth::user()->pacote == 0) {
            echo 'Realize um upgrade para fazer saques.';
            exit();
        } else if ($config['permitir_saque'] == 'não'){
          echo 'Saque indisponível no momento.';

      }
      else if(\Auth::user()->emAtraso()) {
        echo 'Regularize sua conta para realizar saques.';
        exit();
    } else if (is_numeric($valor2) and $valor2 >= ($config['saque_minimo'] * 100) and $valor2 <= ($config['saque_max'] * 100)) {
        $dadosbancarios = User::where('id', \Auth:: user()->id)->first();

        if ($saldo > $valor or $saldo == $valor) {
            $novoSaldo = $saldo - $valor;
            $dadosConta = "Carteira : {$dadosbancarios['bitzpayer_id']} <br>";
            $dadosConta .= "Banco: {$dadosbancarios['banco']} <br>";
            $dadosConta .= "Agência: {$dadosbancarios['agencia']} <br>";
            $dadosConta .= "Conta: {$dadosbancarios['conta'] }  <br>";
            $dadosConta .= " Tipo: {$dadosbancarios['tipo_conta']} <br>";
            $dadosConta .= "Operação:  {$dadosbancarios['operacao']} <br>";

            User::where('id', \Auth::user()->id)->update(['saldo' => $novoSaldo]);
            Saque::create(['valor' => $valor, 'status' => 0, 'user_id' => \Auth::user()->id, 'conta' => $dadosConta]);
            extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => 'Saque', 'valor' => ($valor) * (-1)
                , 'beneficiado' => \Auth::user()->id]);

            echo 'O setor financeiro pode demorar para transferir para sua conta.';
        } else {
            echo 'Saldo Indisponivel';
        }
    } else {

        echo 'Erro';
    }
}

public function store(Request $request) {
    $rules = array('status' => 'required|integer',
        'mensagem' => 'required',
        );

    $validator = Validator::make($request->all(), $rules);
    if ($validator->passes()) {

        $data = '';
        if ($request->status == 1) {
            $data = date("Y-m-d H:i:s");
        }


        if (Saque::where('id', $request->id)->update(['data_deposito' => $data, 'status' => $request->status, 'mensagem' => $request->mensagem])) {
           dd('teste');
     } else {
        return '
        <div class="alert alert-danger fade in">
          Ocorreu um erro! Por favor tente novamente.
      </div>
      ';
  }
} else {
    return '
    <div class="alert alert-danger fade in">
      Ocorreu um erro na validação dos dados! Por favor tente novamente.
  </div>';
}
}

}
