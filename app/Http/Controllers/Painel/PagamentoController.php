<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Repositories\PagSeguroRepository;
use Illuminate\Http\Request;
use App\User;
use Iugu;
use App\config;
use App\Pacote;
use App\Pagamentos;
use Gerencianet\Exception\GerencianetException;
use Gerencianet\Gerencianet;
use Session;

class PagamentoController extends Controller {

    public function pagSeguroConfig() {
        if (env('PAGSEGURO_SANDBOX') == true) {
            return ['token' => env('PAGSEGURO_TOKEN_SANDBOX'), 'email' => env('PAGSEGURO_EMAIL'), 'url' => 'sandbox.pagseguro.uol.com.br'];
        } else {
            return ['token' => env('PAGSEGURO_TOKEN_PROD'), 'email' => env('PAGSEGURO_EMAIL'), 'url' => 'pagseguro.uol.com.br'];
        }
    }

    public function loadPagseguro() {

        /* Variáveis para enviar */

        $this->add_field('email', $this->pagSeguroConfig()['email']);
        $this->add_field('token', $this->pagSeguroConfig()['token']);
        $this->add_field('currency', 'BRL');
        $this->add_field('redirectURL', env('PAGSEGURO_REDIRECT')); // certo
        $this->add_field('reviewURL', env('PAGSEGURO_REDIRECT')); // errado
        $this->add_field('notificationURL', env('PAGSEGURO_NOTIFICATION')); //notificação
    }

    public function add_field($field, $value) {
        $this->fields[$field] = utf8_decode($value);
    }

    public function gerar_link($metodo, $pacote, $userId, $tipo, $valor = 0, $pagamentoParcial = false) {
        $config = new config();
        $config = $config->getConfig();
        $usr = new User();
        $preferencial = 1;
        $userInfo = $usr->userInfo($userId);
        $pacoteInfo = Pacote::where('id', $pacote)->first();
        $vencimento = date('Y-m-d', strtotime("+5 days"));
        $hoje = date('Y-m-d');

        $pacoteValor = $pacoteInfo['valor'];
        if ($metodo == 1) {
            Iugu::setApiKey($config['iugu_token']);
            $this->invoice = \Iugu_Invoice::create(
                            Array(
                                "email" => $userInfo['email'],
                                "due_date" => $vencimento,
                                "notification_url" => url('notificacao/1'),
                                "return_url" => url('painel/home'),
                                "items" =>
                                Array(
                                    Array(
                                        "description" => $tipo . '-' . $pacoteInfo['nome'],
                                        "quantity" => "1",
                                        "price_cents" => $pacoteValor * 100
                                    )
                                )
                            )
            );
            $retorno = $this->invoice;
            if (isset($retorno['secure_url'])) {
                $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'iugu', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $retorno['id'], 'paymentLink' => $retorno['secure_url'], 'pacote' => $pacote, 'tipo' => $tipo]);
                if ($userInfo['ativo'] == 1) {
                    $preferencial = 1;
                }
                $usr->criarFatura($pacote, $userId, $preferencial, $idPay, 0);
                return $retorno['secure_url'];
            } else {
                return false;
            }
        }//iugu
        if ($metodo == 2) {
            if ($config['gnt_ambiente'] == 'teste') {
                $sandbox = true;
            } else {
                $sandbox = false;
            }
            $this->api = new Gerencianet([
                'client_id' => $config['gnt_client'],
                'client_secret' => $config['gnt_secret'],
                'sandbox' => $sandbox
            ]);

            $body = [
                'items' => [
                    [
                        'name' => $tipo . '-' . $pacoteInfo['nome'],
                        'amount' => 1,
                        'value' => $pacoteValor * 100
                    ]
                ],
                'metadata' => [
                    'custom_id' => "SB-" . strval($userInfo['id']) . "-" . md5(time()),
                    'notification_url' => url('notificacao/2')
                ]
            ];
            $charge = $this->api->createCharge([], $body);
            $params2 = [
                'id' => $charge['data']['charge_id']
            ];
            $vencimento = date('Y-m-d', strtotime("+5 days"));
            $body2 = [
                'payment' => [
                    'banking_billet' => [
                        'expire_at' => strval($vencimento),
                        'customer' => [
                            'name' => $userInfo['name'],
                            'cpf' => str_replace(['.', '-'], '', $userInfo['cpf']),
                            'phone_number' => str_replace(['-'], '', $userInfo['telefone']),
                            'email' => $userInfo['email']
                        ]
                    ]
                ]
            ];
            $retorno = $this->api->payCharge($params2, $body2);
            if (isset($retorno['data']['link'])) {
                $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'gnt', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $charge['data']['custom_id'], 'paymentLink' => $retorno['data']['link'], 'pacote' => $pacote, 'tipo' => $tipo]);
                if ($userInfo['ativo'] == 1) {
                    $preferencial = 1;
                }
                $usr->criarFatura($pacote, $userId, $preferencial, $idPay, 0);
                return $retorno['data']['link'];
            } else {
                return false;
            }
        }//gerencianet
        else if ($metodo == 3) {
            $reference = md5(time()) . \Auth::user()->id . rand(0, 1000);
            $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'bitzpayer', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $reference, 'paymentLink' => url("/painel/ver_fatura?novopagamento=1&metodo=3&tipo=$tipo&pacote=$pacote&ref=$reference"), 'pacote' => $pacote, 'tipo' => $tipo]);
            return url("/painel/ver_fatura?novopagamento=1&metodo=3&tipo=$tipo&pacote=$pacote&ref=$reference");
        } else if ($metodo == 5) {
            $reference = md5(time()) . \Auth::user()->id . rand(0, 1000);

            $this->add_field('itemId1', $pacoteInfo['id']);
            $this->add_field('itemDescription1', $tipo . '-' . $pacoteInfo['nome']);
            $this->add_field('itemQuantity1', 1);
            $amo = $pacoteInfo['valor'];
            if ($tipo == 'Upgrade') {
                $currentPck = Pacote::where('id', $userInfo['pacote'])->first();
                if ($currentPck) {
                    if ($currentPck['valor'] >= $pacoteInfo['valor']) {
                        exit();
                    } else {
                        $amo = number_format((float) $pacoteInfo['valor'], 2, '.', '');
                        $this->add_field('itemAmount1', $amo);
                    }
                } else {
                    exit();
                }
            } else {
                $this->add_field('itemAmount1', number_format((float) $pacoteInfo['valor'], 2, '.', ''));
            }
            \Auth::user()->telefone = str_replace(['(', ')', '-'], '', \Auth::user()->telefone);
            $this->add_field('reference', $reference);
            $this->add_field('senderAreaCode', substr(\Auth::user()->telefone, 1, 2));
            $this->add_field('senderPhone', substr(str_replace('-', '', \Auth::user()->telefone), 2, 8));
            $this->add_field('senderEmail', \Auth::user()->email);
            $this->add_field('senderPhone', substr(str_replace('-', '', \Auth::user()->telefone), 2, 8));
            $this->add_field('senderEmail', \Auth::user()->email);
            $this->loadPagseguro();
            $curl = curl_init('https://ws.' . $this->pagSeguroConfig()['url'] . '/v2/checkout');
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            $fields = $this->fields;
            curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->fields));
            $xml = curl_exec($curl);

            /* Tratando erros */
            if ($xml == 'Unauthorized') /* Gerar Log de erro */
                dd('erro');

            curl_close($curl);

            $xml = simplexml_load_string($xml);

            if (count($xml->error) > 0) {/* Gerar Log de erro */
                $erros = $xml->error;
                foreach ($erros as $value) {
                    return json_encode(['url' => 'error', 'message' => $value->message]);
                }
            } else {
                $preferencial = 0;
                $urlPay = 'https://' . $this->pagSeguroConfig()['url'] . '/v2/checkout/' . 'payment.html?code=' . $xml->code;
                $idPay = \DB::table('pagamentos')->insertGetId(['valor' => $amo, 'paymentMethod' => 'PagSeguro', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $reference, 'paymentLink' => $urlPay, 'pacote' => $pacote, 'tipo' => $tipo]);
                $usr->criarFatura($pacote, $userId, $preferencial, $idPay, 0);
                if ($urlPay == '') {
                    echo json_encode(['url' => 'error', 'message' => 'Ocorreu um erro.']);
                } else {
                    echo json_encode(['url' => $urlPay, 'code' => $xml->code]);
                }
            }
        
        }
        
        else if ($metodo == 6)
        
        {
        	 $saldo = \Auth::user()->saldo;
            $usrModel = new User();
            $descontoPacote = \Auth::user()->descontoUpgrade();
            /* if ($tipo == 'Upgrade') {
              $pacoteInfo['valor'] = $pacoteInfo['valor'] - $descontoPacote;
              } */

            if ($pagamentoParcial) {
                if ($saldo > 0 && $pacoteInfo['valor'] > $saldo && $tipo == 'Upgrade') {
                    $usrModel->removeSaldo(\Auth::user()->id, $saldo, 'Upgrade parcial-' . $pacoteInfo['nome']);
                    $pacoteInfo['valor'] = $pacoteInfo['valor'] - $saldo;
                }
            }


            if ($valor > 0) {
                $pacoteInfo['valor'] = $valor;
            }


            /*
             * Exemplo de criação de um boleto utilizando o método HTTP POST. e exibe o conteúdo PDF retornado no navegador.
             * Após obter o PDF do boleto o arquivo é enviado como resposta para visualização no navegador.
             *
             * cURL - http://php.net/manual/pt_BR/book.curl.php
             * */

#Dados do boleto
            $ref = \Auth::user()->id . '-' . rand(0, 99999);

            $file = "";

            $urlPay = url('painel/ver_fatura?ref=' . $ref);
            $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'boleto', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $ref, 'paymentLink' => $urlPay, 'pacote' => $pacote, 'tipo' => $tipo,
                'valor' => $pacoteInfo['valor']]);
            $values['user_id'] = $userId;
            $values['status'] = 0;
            $values['validade'] = $vencimento;
            $values['data'] = $hoje;
            $values['pagamento_id'] = $idPay;
            \DB::table('faturas')->insert($values);
            return $urlPay;


            /*
             * Para saber mais sobre tratamento de erros veja a seção Status & Erros 
             * */
        }
    }

    public function sendPayment($ref) {
        $this->loadPagseguro();
        $curl = curl_init('https://ws.' . $this->pagSeguroConfig()['url'] . '/v2/checkout');
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
        $fields = $this->fields;
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($this->fields));


        $xml = curl_exec($curl);


        /* Tratando erros */
        if ($xml == 'Unauthorized') /* Gerar Log de erro */
            dd('erro');

        curl_close($curl);

        $xml = simplexml_load_string($xml);

        if (count($xml->error) > 0) {/* Gerar Log de erro */
            dd($xml);
            return false;
        } else {
            $preferencial = 0;
            $urlPay = 'https://' . $this->pagSeguroConfig()['url'] . '/v2/checkout/' . 'payment.html?code=' . $xml->code;
            $reference = md5(time()) . \Auth::user()->id . rand(0, 1000);
            $idPay = \DB::table('pagamentos')->insertGetId(['paymentMethod' => 'PagSeguro', 'status' => 'Pendente', 'date' => $hoje, 'user_id' => $userId, 'reference' => $reference, 'paymentLink' => $urlPay, 'pacote' => $pacote, 'tipo' => $tipo]);
            $usr->criarFatura($pacote, $userId, $preferencial, $idPay, 0);
            return $url;
        }
    }

    public function notificacao($param) {
        $usr = new User();
        $voucher = new \App\Http\Controllers\Admin\VoucherController;
        if ($param == 1) {

            $data = $_POST['data'];
            $status = $data['status'];
            $id = $data['id'];
            $pagamentoInfo = Pagamentos::where('reference', $id)->first();
            $userInfo = $usr->userInfo($pagamentoInfo['user_id']);
            $tipo = $pagamentoInfo['tipo'];
            if ($status == 'paid' and $pagamentoInfo['status'] == 'Pendente') {
                if ($tipo == 'Ativação de pacote') {
                    $voucher->ativarUsr($pagamentoInfo['user_id']);
                } elseif ($tipo == 'Upgrade') {
                    $voucher->mudarPacote($pagamentoInfo['user_id'], $pagamentoInfo['pacote']);
                } elseif ($tipo == 'Renovação') {
                    $voucher->ativarUsr($pagamentoInfo['user_id']);
                }
                Pagamentos::where('id', $pagamentoInfo['id'])->update(['status' => 'Paga']);
            }
        }//iugu
        if ($param == 2) {
            $not = $_POST['notification'];
            $res = $this->notificationGnt($not)['data'];
            $retorno = array_pop($res);
            $status = $retorno['status']['current'];
            $id = $retorno['custom_id'];

            $pagamentoInfo = Pagamentos::where('reference', $id)->first();
            $userInfo = $usr->userInfo($pagamentoInfo['user_id']);
            $tipo = $pagamentoInfo['tipo'];
            if ($status == 'waiting' and $pagamentoInfo['status'] == 'Pendente') {
                if ($tipo == 'Ativação de pacote') {
                    $voucher->ativarUsr($pagamentoInfo['user_id']);
                } elseif ($tipo == 'Upgrade') {
                    $voucher->mudarPacote($pagamentoInfo['user_id'], $pagamentoInfo['pacote']);
                } elseif ($tipo == 'Renovação') {
                    $voucher->ativarUsr($pagamentoInfo['user_id']);
                }
                Pagamentos::where('id', $pagamentoInfo['id'])->update(['status' => 'Paga']);
            }
            $res['status'] = $status;
            $res['user_id'] = $pagamentoInfo['user_id'];
            $res['tipo'] = $tipo;

            \DB::table('reponse')->insert(['body' => json_encode($res)]);
        }
        //bitpayer
        if ($param == 3) {
            $sandbox = true;

            if ($sandbox) {
                $return = '{"status":"payment_received",
                "err":"",
                "private_key_hash":"' . hash("sha512", env('PRIVATE_GOURL')) . '", 
                "box":"120",
                "boxtype":"paymentbox",
                "order":"' . $_GET['ref'] . '",
                "user":"user26",
                "usercountry":"USA",
                "amount":"0.0479166",
                "amountusd":"' . $_GET['usd'] . '",
                "coinlabel":"BTC",
                "coinname":"bitcoin",
                "addr":"14dt2cSbvwghDcETJDuvFGHe5bCsCPR9jW",
                "tx":"95ed924c215f2945e75acfb5650e28384deac382c9629cf0d3f31d0ec23db08d",
                "confirmed":1,
                "timestamp":"1422624765 ",
                "date":"30 January 2015",
                "datetime":"2015-01-30 13:32:45"
            }';

                $return = json_decode($return, true);

                $_POST = $return;
            }
            $html = "";
            if (isset($_POST["status"]) && in_array($_POST["status"], array("payment_received")) &&
                    $_POST["box"] && $_POST["box"] > 0 && $_POST["amount"] && $_POST["amount"] > 0 &&
                    strtolower($_POST["private_key_hash"]) == strtolower(hash("sha512", env('PRIVATE_GOURL')))) {
                $paymentID = false;
                $txConfirmed = true;

                // check if new payment; sql example
                $pagamento = Pagamentos::where('reference', $_POST['order'])->where('status', 'Pendente')->first();
                if (isset($pagamento['id']) && round($pagamento['valor']) == $_POST['amountusd']) {
                    $paymentID = true;
                    $txConfirmed = false;
                    $faturaId = \DB::table('faturas')->where('pagamento_id', $pagamento['id'])->first()->id;
                } else {
                    $paymentID = true;
                    $txConfirmed = true;
                }
                // Save new payment details in your database
                if (!$paymentID) {
                    // your code here; for example update user membership
                    $html = "cryptobox_newrecord"; // don't change text; it use by gourl.io server
                }
                // Update record; payment confirmed
                elseif ($_POST["confirmed"] && !$txConfirmed) {

                    $pagamentoInfo = $pagamento;
                    if (isset($pagamentoInfo['id']) and $pagamentoInfo['status'] != 'Paga') {
                        $pack = new \App\Http\Controllers\Admin\PacoteController();
                        $pack->liberar_fatura($faturaId);
                    }

                    // your code here
                    $html = "cryptobox_updated"; // don't change text
                } else {
                    // your code here
                    $html = "cryptobox_nochanges"; // don't change text
                }
            } else
                $html = "Only POST Data Allowed"; // don't change text

            echo $html;
        }
        if ($param == 5) {
            $url = 'https://ws.' . $this->pagSeguroConfig()['url'] . '/v2/transactions/notifications/' . $_POST['notificationCode'] . "?email=" . $this->pagSeguroConfig()['email'] . "&token=" . $this->pagSeguroConfig()['token'];

            $curl = curl_init($url);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

            $response = curl_exec($curl);       // Utilizado para consultar status
            $http = curl_getinfo($curl);    // Utilizado para o log

            if ($response == 'Unauthorized')
                $this->index(); /* Gerar log de erro */

            curl_close($curl);

            $response = simplexml_load_string($response);

            if (count($response->error) > 0)
                $this->index(); /* Gerar log de erro */


            // Código para verificar o instant payment
            $response->notificationCode = $_POST['notificationCode'];
            $response->nm_pagamento = 'pagseguro';


            # ---------------------------------------------------------------------- ----------------------------------------------------------------------

            if (empty($response))
                return redirect('painel');

            /*
              código 3 => 'Pago'
             */
            if ($response->status == 3) {


                $pagamentoInfo = Pagamentos::where('reference', $response->reference)->first();

                $userInfo = $usr->userInfo($pagamentoInfo['user_id']);
                $tipo = $pagamentoInfo['tipo'];

                if (isset($pagamentoInfo['id']) and $pagamentoInfo['status'] != 'Paga') {
                    if ($tipo == 'Ativação de pacote') {
                        $voucher->ativarUsr($pagamentoInfo['user_id']);
                    } elseif ($tipo == 'Upgrade') {
                        $voucher->mudarPacote($pagamentoInfo['user_id'], $pagamentoInfo['pacote']);
                    } elseif ($tipo == 'Renovação') {
                        $voucher->ativarUsr($pagamentoInfo['user_id']);
                    } elseif ($tipo == 'Compra') {
                        $pck = new \App\Http\Controllers\Admin\PacoteController();
                        $fatura_id = \DB::table('faturas')->where('pagamento_id', $pagamentoInfo['id'])->where('user_id', $pagamentoInfo['user_id'])->first()->id;
                        $pck->liberar_fatura($fatura_id);
                    }
                    Pagamentos::where('id', $pagamentoInfo['id'])->update(['status' => 'Paga']);
                }
            }
        }
    }

    public function notificationGnt($token) {
        $config = new config();
        $config = $config->getConfig();
        if ($config['gnt_ambiente'] == 'teste') {
            $sandbox = true;
        } else {
            $sandbox = false;
        }
        $this->api = new Gerencianet([
            'client_id' => $config['gnt_client'],
            'client_secret' => $config['gnt_secret'],
            'sandbox' => $sandbox
        ]);

        $params = [
            'token' => $token
        ];

        try {
            $notification = $this->api->getNotification($params, []);
            return $notification;
        } catch (GerencianetException $e) {
            exit($e->getMessage() . ' - E:' . $e->getCode());
        } catch (\Exception $e) {
            var_dump($e->getMessage());
        }
    }

    public function retorno($retorno) {
        $usr = new User();
        $voucher = new \App\Http\Controllers\Admin\VoucherController;
        if ($retorno == 1) {
            echo "<center><h1>Pagamento realizado com sucesso.<br><a href='" . env('SITE_URL') . '/painel/home' . "'>Clique aqui para acessar o painel</a></h1></center>'";
        }
        if ($retorno == 2) {
            echo "<center><h1>Ocorreu uma falha no pagamento :-(</h1></center>'";
        }
        if ($retorno == 3) {
            define('MY_SHOP_LOGIN', env("MY_SHOP_LOGIN"));
            define('MY_SHOP_SECRET', env("MY_SHOP_SECRET"));
            $shop_secret = MY_SHOP_SECRET;
            $message = 'R' . $_POST['shop_login'] . $_POST['amount'] . $_POST['currency'] . $_POST['signature'] . $_POST['datetime'];
            $signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));

            if ($signature === $_POST['signature']) {

                //
                $id = $_POST['signature'];
                $pagamentoInfo = Pagamentos::where('reference', $id)->first();

                $userInfo = $usr->userInfo($pagamentoInfo['user_id']);
                $tipo = $pagamentoInfo['tipo'];
                echo "Status: " . $pagamentoInfo['status'];
            }
        }
    }

    public function ipn() {
        $url = 'https://ws.' . $this->pagSeguroConfig()['url'] . '/v2/transactions/notifications/' . $_POST['notificationCode'] . "?email=" . $this->pagSeguroConfig()['email'] . "&token=" . $this->pagSeguroConfig()['token'];

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($curl);       // Utilizado para consultar status
        $http = curl_getinfo($curl);    // Utilizado para o log

        if ($response == 'Unauthorized')
            $this->index(); /* Gerar log de erro */

        curl_close($curl);

        $response = simplexml_load_string($response);

        if (count($response->error) > 0)
            $this->index(); /* Gerar log de erro */


        // Código para verificar o instant payment
        $response->notificationCode = $_POST['notificationCode'];
        $response->nm_pagamento = 'pagseguro';


        # ---------------------------------------------------------------------- ----------------------------------------------------------------------

        if (empty($response))
            return redirect('painel');

        /*
          código 3 => 'Pago'
         */
        if ($response->status == 3) {
            \DB::table('pagamentos')
                    ->where('reference', $response->reference)
                    ->update(['status' => 'Pago']);

            $pag = Pagamentos::where('reference', $response->reference)->first();
            print_r($pag);
            if (isset($pag->id)) {
                $fatura = \DB::table('faturas')->where('pagamento_id', $pag->id)->where('user_id', $pag->user_id)->first();
                if (isset($fatura->id)) {
                    $pck = new \App\Http\Controllers\Admin\PacoteController();
                    $pck->liberar_fatura($fatura->id);
                }
            }
        }
    }
    
    public function verFatura() {

        if (isset($_GET['novopagamento'])) {

            $pay = new \App\Http\Controllers\Painel\PagamentoController();

            if (@$_GET['novopagamento'] == 1 and ( $_GET['metodo'] == 5)) {
                if ($_GET['tipo'] == 1) {
                    $tipo = 'Ativação de pacote';
                } if ($_GET['tipo'] == 2) {
                    $tipo = 'Upgrade';
                }if ($_GET['tipo'] == 5) {
                    $tipo = 'Compra';
                } else {
                    $tipo = 'Ativação de pacote';
                }
                $res = $pay->gerar_link($_GET['metodo'], $_GET['pacote'], \Auth::user()->id, $tipo);
                if ($res) {
                    echo $res;
                }
            }
        } else {
            $ref = $_GET['ref'];

            if (!isset($_GET['ref'])) {
                return redirect()->back()->withErrors(['Fatura não encontrada.']);
            }

            $pagamento = Pagamentos::where('reference', $ref)->where('status', 'Pendente')->first();

            if (!isset($pagamento['id'])) {
                return redirect()->back()->withErrors(['Fatura não encontrada.']);
            }
            $dataInvoice['valorFatura'] = $pagamento['valor'];

            $dataInvoice['faturaId'] = $pagamento['id'];
            $dataInvoice['user_id'] = $pagamento['user_id'];

            $dataInvoice['referencia'] = $ref;
            return view('painel.pages.gourl.checkout', $dataInvoice);
        }
    }

    /* public function notificacao() {



      header("access-control-allow-origin: https://sandbox.pagseguro.uol.com.br");
      //var_dump($_POST);
      $email = env('PAGSEGURO_EMAIL');
      $token = env('PAGSEGURO_TOKEN_SANDBOX');
      $noti = $_GET['not'];

      $url = 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/' . $noti . '?email=' . $email . '&token=' . $token;
      //echo '<br>' . $url . '<br>';
      $curl = curl_init($url);
      curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
      $transaction = curl_exec($curl);
      curl_close($curl);
      if ($transaction == 'Unauthorized') {
      //Insira seu código avisando que o sistema está com problemas, sugiro enviar um e-mail avisando para alguém fazer a manutenção
      exit; //Mantenha essa linha
      }
      $transaction = simplexml_load_string($transaction);
      $this->pagSeguroRepository->updateTransaction($transaction);
      } */
}
