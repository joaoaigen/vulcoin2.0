<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class PacoteController extends Controller
{
    public function index(){
        return view('admin.pages.pacote');
    }
    
    public function store(Request $request){
        $validator = Validator::make($request->all(), [
            'nome' => 'required',
            'valor' => 'required',
            'porcentagem_maxima' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect('/admin/pacote')->withErrors($validator);           
        }
        
        
        $pact = DB::table('pacotes_investimentos')->insert([
            'nome' => $request->nome,
            'valor' => $request->valor,
            'porcentagem_maxima' => $request->porcentagem_maxima,
        ]);
        
        return back()->with('status', 200)->with('msg', 'Pacote criado com sucesso!');
    }
}
