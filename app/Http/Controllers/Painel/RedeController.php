<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Referrals;
use App\User;
use Illuminate\Http\Request;
use App\Binario;

class RedeController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $binario = new Binario();
        $binario['esquerda'] = $binario->totalEsquerda(\Auth::user()->id);
        $binario['direita'] = $binario->totalDireita(\Auth::user()->id);
        $filho = \Auth::user()->getFilhos();


        
        return view('painel.pages.rede', compact('binario'));
    }

    public function interna($id) {
        $user_interna = User::where('id', $id)->first();
        if ($user_interna) {
            return view('painel.pages.rede_interna', compact('user_interna'));
        } else {
            return redirect('/painel/minha-rede')
                            ->withErrors(['Usuario não encontrado']);
        }
    }
 public function lista_indicados($user_id, $nivel = 1) {
        /*   <th>#</th>
          <th>Nome</th>
          <th>Username</th>
          <th>Status</th>
          <th>Email</th>
          <th>N���vel</th> */

        $indicados = User::where('pai_id', $user_id)->get();
        if (User::where('pai_id', $user_id)->count() >= 0) {
            foreach ($indicados as $indicado) {
                $status = \Auth::user()->getStatus($indicado->ativo);
                echo" <tr>
                        <td>{$indicado->id}</td>
                        <td><b>{$indicado->name}</b></td>
                  <td><b>{$indicado->username}</b></td>
                                        <td><b>$status</b></td>
                        <td>{$indicado->email}</td>
                        <td>$nivel</td>
                        <td>$indicado->direcao</td>
                        </tr>";
            }
            $nivel = $nivel + 1;
            foreach ($indicados as $indicado) {

                $this->lista_indicados($indicado->id, $nivel);
            }
        }
    }
    public function unilevel() {
        $bin = new Binario();
        $id = \Auth::user()->id;
        $filhos = $bin->getFilhosLevel($id);
        $ordenados = array();
        //gambiara,dps eu errumo
        $key = md5(time() . $id . round(111111, 9999999));
        foreach ($filhos as $value) {
            $data['key'] = $key;
            $data['user'] = $value['user'];
            $data['level'] = $value['level'];
            \DB::table('temp_unilevel')->insert($data);
        }
        $rede = \DB::table('temp_unilevel')->where('key', $key)->orderBy('level')->get();
        //\DB::table('temp_unilevel')->where('key', $key)->delete();
        return view('painel.pages.unilevel', compact('rede'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        if (Referrals::updateDirection($id, \Input::get('direcao'))) {
            return "<div class='alert alert-success'>Usuario ID: <b>$id</b> Atualizado!</div>";
        } else {
            return "<div class='alert alert-danger'>Não foi possivel atualizar o Usuario: <b>$id</b></div>";
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        //
    }

}
