<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\User;
use App\Carrinho;
use App\Produtos;

class MeuCarrinhoController extends Controller {

    public function index() {
        $carrinho = Carrinho::where('user_id', \Auth::user()->id)->get();
        
        if (count($carrinho) !== 0) {  // Produtos do carrinho do usuário
            $produtos = Carrinho::carrinhoAtual();
        } else {
            $produtos = [];
        }

        // Calcula o subtotal
        $subtotal = 0;
        $pesoTotal = 0;
        $subtotal2 = 0;
        foreach ($produtos as $produto) {

            $quantidade = $produto['quantidade'];
            $preco = $produto['produto']['preco'] * $quantidade;
            if (\Auth::user()->meu_desconto($produto['product_id'])) {
                $subtotal +=\Auth::user()->meu_desconto($produto['product_id']) * $quantidade;
            } else {
                $subtotal+=$preco;
            }
            $subtotal2 += $preco;

            $pesoTotal += $quantidade * $produto['produto']['peso'];
        }

        // Calcula o frete
        $frete = 0;
        $cep = null;
        $codigo = null;
        if (\Input::has('cep') && \Input::has('codigo')) {
            $cep = \Input::get('cep', null);
            $codigo = \Input::get('codigo', null);
            $cep_origem = 89300000;
            $cod_servico = $codigo;
            $cep_destino = $cep;

            $frete = $this->calculaFrete($cod_servico, $cep_destino, $pesoTotal);


            if (!preg_match('/[0-9]{5,5}([- ]?[0-9]{4})?$/', $cep)) {
                $cep = null;
            }
        }

        $total = $subtotal + $frete;
        return view('painel.pages.meu-carrinho', compact('produtos', 'frete', 'total', 'subtotal', 'subtotal2', 'cep', 'codigo'));
    }

    // Adiciona um produto no carrinho
    public function add($product_id) {

        $exists = Produtos::where('id', $product_id)->where('status', 1)->first();
        if ($exists) {

            $user_id = \Auth::user()->id;
            $carrinho = new Carrinho;
            $carrinho->user_id = $user_id;
            $carrinho->product_id = $product_id;

            $carrinho->save();
        }

        return redirect("/painel/meu-carrinho");
    }

    public function compraVoucher($product_id) {
        $produto = Produtos::where('id', $product_id)->where('qntd_voucher', '>', 0)->where('status', 1)->first();
        if (isset($produto['id'])) {
            $valor = \Auth::user()->meu_desconto($produto['id']);
            if (!$valor) {
                $valor = $produto['preco'];
            }
            $coluna = 'carteira_b';
            $saldoUser = \Auth::user()->{$coluna};



            if ($saldoUser >= $valor) {

                \Auth::user()->removeValueColumn(\Auth::user()->id, $valor, 'Compra', $coluna);
                $qntd_voucher = $produto['qntd_voucher'];
                url('voucher/create?key=' . env('KEY_VOUCHER_API') . '&qntd=' . $qntd_voucher);
                file_get_contents(url('voucher/create?key=' . env('KEY_VOUCHER_API') . '&qntd=' . $qntd_voucher . '&id=' . \Auth::user()->id));
                return redirect('/painel/produtos')->with('success', 'Parabéns! Sua compra foi efetuada com sucesso!.');
            } else {
                return redirect('/painel/produtos')->withErrors(['Saldo insuficiente.']);
            }
        } else {
            return redirect('/painel/produtos')->withErrors(['Produto não encontrado.']);
        }
    }

    public function qtd($product_id) {
        $params = '';
        $qtd = \Input::get('qtd', null);
        if (empty($qtd) || $qtd <= 0) {
            return redirect("/painel/meu-carrinho$params");
        }

        // Quantidade de produtos atuais
        $lenght = Carrinho::where('product_id', $product_id)->
                        where('user_id', \Auth::user()->id)->where('pedido', 0)->get()->count();

        // Foi diminuido a quantidade
        if ($lenght > $qtd) {
            $max = $lenght - $qtd;
            Carrinho::where('product_id', $product_id)->
                    where('user_id', \Auth::user()->id)->limit($max)->delete();
        }

        // Foi aumentada a quantidade
        if ($lenght < $qtd) {
            $size = $qtd - $lenght;
            $size = $size + 1;
            for ($i = 1; $i < $size; $i++) {
                $produto = Produtos::where('id', $product_id)->first();
                if ($produto['estoque'] >= $i) {
                    $carrinho = new Carrinho;
                    $carrinho->user_id = \Auth::user()->id;
                    $carrinho->product_id = $product_id;
                    $carrinho->pedido = 0;
                    $carrinho->save();
                } else {
                    return redirect('/painel/meu-carrinho')->withErrors(['Estoque insuficiente.']);
                }
            }
        }
        $params = '';

        if (isset($_GET['distributor_mode'])) {
            return redirect("/painel/meu-carrinho?distributor_mode=true&product_id=$product_id");
        } else {
            return redirect("/painel/meu-carrinho$params");
        }
    }

    // Remove um produto do carrinho ou muda a quantidade dele
    public function remove($product_id) {

        $params = '';
        if (\Input::has('codigo') && \Input::has('cep')) {
            $cep = \Input::get('cep', null);
            $codigo = \Input::get('codigo', null);
            $params = "?cep=$cep&codigo=$codigo";
        }

        // Verifica se é para mudar a quantidade
        if (\Input::has('qtd')) {
            $qtd = \Input::get('qtd', null);
            if (empty($qtd) || $qtd <= 0) {
                return redirect("/painel/meu-carrinho$params");
            }
        } else {
            Carrinho::where('product_id', $product_id)->
                    where('user_id', \Auth::user()->id)->delete();
            return redirect("/painel/meu-carrinho$params");
        }

        // Quantidade de produtos atuais
        $lenght = Carrinho::where('product_id', $product_id)->
                        where('user_id', \Auth::user()->id)->get()->count();

        // Foi diminuido a quantidade
        if ($lenght > $qtd) {
            $max = $lenght - $qtd;
            Carrinho::where('product_id', $product_id)->
                    where('user_id', \Auth::user()->id)->limit($max)->delete();
        }

        // Foi aumentada a quantidade
        if ($lenght < $qtd) {
            $size = $qtd - $lenght;

            for ($i = 0; $i < $size; $i++) {
                $carrinho = new Carrinho;
                $carrinho->user_id = \Auth::user()->id;
                $carrinho->product_id = $product_id;
                $carrinho->save();
            }
        }

        return redirect("/painel/meu-carrinho$params");
    }

}
