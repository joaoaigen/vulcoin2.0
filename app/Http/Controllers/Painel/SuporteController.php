<?php

namespace App\Http\Controllers\Painel;

use App\Suporte;
use App\SuporteMensagem;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;


class SuporteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $resolvidos = Suporte::where('user_id', Auth::user()->id)->where('status', 2)->get();
        $em_andamaneto = Suporte::where('user_id', Auth::user()->id)->where('status', 1)->get();
        $em_aberto = Suporte::where('user_id', Auth::user()->id)->where('status', 0)->get();

        return view('painel.pages.suporte')
            ->with('resolvido', $resolvidos != null ? $resolvidos : null)
            ->with('andamento', $em_andamaneto != null ? $em_andamaneto : null)
            ->with('aberto', $em_aberto != null ? $em_aberto : null);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dados = $request->all();

        $suporte = Suporte::create([
            'assunto' => $dados['assunto'],
            'status' => 0,
            'user_id' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $mensagem = SuporteMensagem::create([
            'id_suporte' => $suporte->id,
            'mensagem' => $dados['body'],
            'id_user' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        if (isset($suporte->id)) {
            return redirect('/painel/suporte')->with('status', 200)->with('msg', 'Mensagem enviada com sucesso!');
        } else {
            return redirect('/painel/suporte')->with('status', 400)->with('msg', 'Ocorreu um erro ao enviar a mensagem!');
        }


    }


    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request)
    {

        if ($request->has('mensagem')) {

            $suporte = Suporte::where('id', $request->mensagem)->first();
            $resolvidos = Suporte::where('user_id', Auth::user()->id)->where('status', 2)->get();
            $em_andamaneto = Suporte::where('user_id', Auth::user()->id)->where('status', 1)->get();
            $em_aberto = Suporte::where('user_id', Auth::user()->id)->where('status', 0)->get();
            $mensagem = SuporteMensagem::where('id_suporte', $request->mensagem)->get();

            return view('painel.pages.suporte')
                ->with('mensagens', $mensagem)
                ->with('suporte', $suporte)
                ->with('user_id', $suporte->user_id)
                ->with('suporte_user', $suporte->suporte_user)
                ->with('resolvido', $resolvidos != null ? $resolvidos : null)
                ->with('andamento', $em_andamaneto != null ? $em_andamaneto : null)
                ->with('aberto', $em_aberto != null ? $em_aberto : null);
        }

        if ($request->has('finalizadas'))
            $finalizadas = Suporte::where('status', '2')->all();

        if ($request->has('processo'))
            $processo = Suporte::where('status', '1')->all();

        if ($request->has('nao_respondida'))
            $nao_respondida = Suporte::where('status', '0')->all();

        return response()->json([
            'nao_respondida' => isset($nao_respondida) ? $nao_respondida : null,
            'processo' => isset($processo) ? $processo : null,
            'finalizadas' => isset($finalizadas) ? $finalizadas : null
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $dados = $request->all();

        $suporte = Suporte::where('id', $dados['id'])->update([
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        $mensagem = SuporteMensagem::create([
            'id_suporte' => $dados['id'],
            'mensagem' => $dados['body'],
            'id_user' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return response()->json($mensagem);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function sendAjax(Request $request)
    {
        $dados = $request->all();

        $mensagem = SuporteMensagem::create([
            'id_suporte' => $dados['id'],
            'mensagem' => $dados['body'],
            'id_user' => Auth::user()->id,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        return $mensagem;
    }


}
