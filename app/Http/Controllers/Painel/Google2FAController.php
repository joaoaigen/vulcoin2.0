<?php

namespace App\Http\Controllers\Painel;
use PragmaRX\Google2FA\Google2FA;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

class Google2FAController extends Controller
{
    public function teste(){
        $usr = new Google2FA();
        return $usr->generateSecretKey();
    }
}
