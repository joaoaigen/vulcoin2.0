<?php

namespace App\Http\Controllers\Painel;


use App\Referrals;
use App\Visitas;
use App\Pacote;
use Illuminate\Http\Request;
use App\Pagamentos;
use App\User;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class CadastroInternoController extends Controller {

    public function index() {
        $indicador = User::where('username', \Auth::user()->username)->orWhere('email', \Auth::user()->username)->first();
        $pacotes = Pacote::all();



        if ($indicador && $indicador->ativo == 1) {

            return view('painel.pages.register', compact('indicador', 'pacotes'));
        } else {
            abort(404);
            return false;
        }
    }

    public function store(Request $request) {
     
        $indicador = User::where('username', \Auth::user()->username)->first();
        $reffer = new Referrals();
        $systemId = $reffer->searchSystemId($indicador->id, $indicador->direcao);


        if ($indicador) {

            $validator = Validator::make($request->all(), [
                        'name' => 'required|max:255',
                        'email' => 'required|email|max:255|unique:users',
                        'username' => 'required|max:20|unique:users',
                        'password' => 'required|confirmed|min:6',
                        'pacote' => 'required|integer',
                        'cpf' => 'cpf',
                        'endereco' => 'required',
                        'bairro' => 'required',
                        'cidade' => 'required',
                        'estado' => 'required',
                        'telefone' => 'phone',
                        'nascimento' => 'date',
            ]);

            if ($validator->fails()) {
                return redirect('/painel/cadastro')
                                ->withErrors($validator)
                                ->withInput();
            }

            $data = \Input::all();

            $user = User::create([
                        'name' => $data['name'],
                        'email' => $data['email'],
                        'username' => $data['username'],
                        'password' => bcrypt($data['password']),
                        'cpf' => $data['cpf'],
                        'endereco' => $data['endereco'],
                        'bairro' => $data['bairro'],
                        'cidade' => $data['cidade'],
                        'sexo' => $data['sexo'],
                        'estado' => $data['estado'],
                        'nascimento' => date('Y-m-d', strtotime($data['nascimento'])),
                        'telefone' => $data['telefone'],
                        'pai_id' => $indicador->id,
                        'direcao' => $indicador->direcao,
                        'pacote' => $data['pacote']
            ]);

            if ($user) {
                $reffer->user_id = $user->id;
                $reffer->pai_id = $indicador->id;
                $reffer->system_id = $systemId['pai_id'];
                $reffer->direcao = $systemId['direcao'] ? $systemId['direcao'] : $indicador->direcao;
                $reffer->save();
            }

            if (\Auth::login($user)) {
                return redirect('/painel');
            } else {
                return redirect('/painel');
            }
        } else {
            abort(404);

            return false;
        }
    }

}
