<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\User;
use App\Produtos;
use Model;
use Validator;
use App\Pedidos;

class ProdutosController extends Controller {

    public function atualizar(){
       $produtos=Produtos::where('id','>',0)->get();
       foreach ($produtos as $key => $value) {
           $descontos=json_decode($value->descontos,true);
           $i=0;
           foreach ($descontos as $key=> $value2) {
            $descontos[$key]=$this->usdFormat($value2);
            $i++;
        }
        $value->update(['descontos'=>json_encode($descontos)]);
    }

}
public function index() {



  $busca = \Input::get('busca', null);
  $ctg = \Input::get('ctg', null);

  $perPage = 50;
  if (empty($ctg)) {
    if (empty($busca)) {
        $produtos = Produtos::where('estoque', '>', 0)->where('status', 1)->paginate($perPage);
        $paginate = Produtos::where('estoque', '>', 0)->where('status', 1)->simplePaginate($perPage);
    } else {
        $produtos = Produtos::where('nome', 'like', "%$busca%")->where('estoque', '>', 0)->where('status', 1)->paginate($perPage);
        $paginate = Produtos::where('nome', 'like', "%$busca%")->where('estoque', '>', 0)->where('status', 1)->simplePaginate($perPage);
    }
} else {
    $produtos = Produtos::where('estoque', '>', 0)->where('status', 1)->where('categoria', $ctg)->paginate($perPage);
    $paginate = Produtos::where('estoque', '>', 0)->where('status', 1)->where('categoria', $ctg)->simplePaginate($perPage);
}
return view('painel.pages.produtos', compact('produtos', 'busca', 'paginate'));
}

public function import(Request $dados) {
    $data = $dados->all();
    $file = \Input::file('import');
    $validator = Validator::make(
        [
        'file' => $file,
        'extension' => strtolower($file->getClientOriginalExtension()),
        ], [
        'file' => 'required',
        'extension' => 'required|in:csv',
        ]
        );
    if ($validator->fails()) {
        return redirect('admin/produtos/import')
        ->withErrors($validator)
        ->withInput();
    }

    $file_handle = fopen($file, 'r');
    while (!feof($file_handle)) {
        $line_of_text[] = fgetcsv($file_handle, 1024);
    }
    fclose($file_handle);
    $i = 0;
    foreach ($line_of_text as $value) {

            //Produtos::create(['']);
        if ($i > 1 and $value[0] <> '') {

            $data['nome'] = $value[0];
            $data['preco'] = $value[1];
            $data['img'] = $value[2];
            $data['produto_url'] = $value[3];
            $data['descricao'] = $data['nome'];
            $data['status'] = 1;
            $data['categoria'] = 'Aliexpress';
            $data['estoque'] = 99999;
            Produtos::create($data);
        }
        echo '<br>';
        $i++;
    }
    return redirect()->back()->with('success', 'Produtos importados com sucesso');
}

public function edit($id) {
    $this->atualizar();

    $dados = Produtos::where('id', $id)->first();
    if ($dados) {
        return view('painel.pages.produtosEdit', compact('dados'));
    } else {
        return redirect('/painel/')
        ->withErrors(['Produto não encontrado']);
    }
}

public function create() {
$this->atualizar();
    return view('painel.pages.produtosAdd');
}

public function voucherProduto() {

    return view('painel.pages.produtoVoucher');
}

public function createVoucher(Request $dados) {
    $data = $dados->all();
    $data['preco'] = $this->usdFormat($data['preco']);

    $validator = Validator::make($data, [
        'nome' => 'required|max:255',
        'preco' => 'required|numeric',
        'img' => 'image',
        'categoria' => 'required|max:100',
        'status' => 'required|integer',
        'qntd_voucher' => 'required|integer|min:1',
        ]);
    if ($validator->fails()) {
        return redirect()->back()
        ->withErrors($validator)
        ->withInput();
    }


        // Foi
    $int = mt_rand(1262055681, 1262055681);

    $fileName = md5(time() . $int) . "." . $dados->file('img')->getClientOriginalExtension();


    $dados->file('img')->move('uploads', $fileName);

    $dados = $data;
    $dados['img'] = 'uploads/' . $fileName;
    foreach ($dados['pre_pacote'] as $key => $value) {
        $dados['pre_pacote'][$key] = $this->usdFormat($value);
    }
    $dados['descontos'] = json_encode($dados['pre_pacote']);
    $i = 0;


    unset($dados['pre_pacote']);
    unset($dados['_token']);

    unset($dados['_method']);
    $dados['estoque'] = 9999999999999999999;
    $produto = Produtos::create($dados);
    if ($produto) {
        return redirect()->back()->with('success', 'Produto adicionado com sucesso');
    } else {
        return redirect()->back()->withErrors(['Ocorreu um erro ao salvar!']);
    }
}

public function createProductVoucher() {

    return view('painel.pages.produtosAdd');
}

public function importarProduto() {

    return view('painel.pages.importarProdutos');
}

public function update(Request $dados) {
    $data = $dados->all();
    $data['preco'] =$this->usdFormat($data['preco']);

    $validator = Validator::make($data, [
        'id' => 'required|integer',
        'nome' => 'required|max:255',
        'preco' => 'required|numeric',
        'peso' => 'required|numeric',
        'categoria' => 'required|max:100',
        'estoque' => 'required|integer',
        'status' => 'required|integer',
        ]);
    if ($validator->fails()) {
        return redirect()->back()
        ->withErrors($validator)
        ->withInput();
    }


        // Foi
    if ($dados->file('img')) {
        $int = mt_rand(1262055681, 1262055681);
        $fileName = md5(time() . $int) . "." . $dados->file('img')->getClientOriginalExtension();
        $dados->file('img')->move('uploads', $fileName);
        $data['img'] = 'uploads/' . $fileName;
    }
    $dados = $data;
    foreach ($dados['pre_pacote'] as $key => $value) {
        $dados['pre_pacote'][$key] = $this->usdFormat($value);
    }

    $dados['descontos'] = json_encode($dados['pre_pacote']);
    $i = 0;

    unset($dados['pre_pacote']);
    unset($dados['_token']);
    unset($dados['_method']);


    $produto = Produtos::where('id', $dados['id'])->update($dados);
    if ($produto) {
        return redirect()->back()->with('success', 'Produto atualizado com sucesso');
    } else {
        return redirect()->back()->withErrors(['Ocorreu um erro ao salvar!']);
    }
}

public function createPost(Request $dados) {
    $data = $dados->all();
    $data['preco'] = $this->usdFormat($data['preco']);

    $validator = Validator::make($data, [
        'nome' => 'required|max:255',
        'preco' => 'required|numeric',
        'peso' => 'required|numeric',
        'img' => 'image',
        'categoria' => 'required|max:100',
        'estoque' => 'required|integer',
        'status' => 'required|integer',
        ]);
    if ($validator->fails()) {
        return redirect()->back()
        ->withErrors($validator)
        ->withInput();
    }


        // Foi
    $int = mt_rand(1262055681, 1262055681);

    $fileName = md5(time() . $int) . "." . $dados->file('img')->getClientOriginalExtension();


    $dados->file('img')->move('uploads', $fileName);

    $dados = $data;
    $dados['img'] = 'uploads/' . $fileName;
    foreach ($dados['pre_pacote'] as $key => $value) {
        $dados['pre_pacote'][$key] = $this->usdFormat($value);
    }
    $dados['descontos'] = json_encode($dados['pre_pacote']);
    $i = 0;


    unset($dados['pre_pacote']);
    unset($dados['_token']);
    unset($dados['_method']);
    $produto = Produtos::create($dados);
    if ($produto) {
        return redirect()->back()->with('success', 'Produto adicionado com sucesso');
    } else {
        return redirect()->back()->withErrors(['Ocorreu um erro ao salvar!']);
    }
}

}
