<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Materiais extends Model
{
    protected $connection= 'mysql';

    protected $table = 'materiais';
    
    protected $fillable = ['description', 'link', 'download'];
}
