<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Pacote;
use App\Http\Controllers\Admin\VoucherController;
use DB;

class Voucher extends Model
{
    //

    protected $fillable = [
        'voucher', 'valor', 'user_id', 'status', 'activated_id',
    ];

    public function getUserNameActivated()
    {
        $user = User::where('id', $this->activated_id)->first();
        return $this->activated_id ? $user->name : null;
    }

    public function getAllPrices()
    {
        $packages = DB::table('pacotes')->get();
        $products = DB::table('produtos')->get();
        $prices = array_merge($packages, $products);
        return $prices;
    }

    public function active($id, $user)
    {
        $voucher = Voucher::find($id);
        $user = User::find($user);

        $package = Pacote::find($user->pacote);

        if($voucher->valor >= $package->valor)
        {
            $_voucher = [
                "status" => 1,
                "activated_id" => $user->id
            ];
            DB::table('vouchers')->where('id', $voucher->id)->update($_voucher);

            $voucher_controller = new VoucherController();
            $voucher_controller->ativarUsr($user->id);

            echo json_encode('{ "status" : "success", "message" : "Usuário foi ativado com sucesso!"}');
        }
        else
        {
            echo json_encode('{ "status" : "fail", "message" : "Valor do pacote do usuário é maior que o do Voucher!"}');
        }


    }
}
