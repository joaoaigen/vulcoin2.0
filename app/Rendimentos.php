<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rendimentos extends Model
{
    
    protected $fillable = ['id_rendimento', 'rendimento_id_usuario', 'rendimento_valor_investido', 'rendimento_percentual', 'rendimento_valor_investido_atual',
        'criado_em', 'atualizado_em', 'rendimento_valor_investido_maximo'
    ];

    protected $table = 'rendimentos';
    
     protected $connection= 'mysql'; 
}
