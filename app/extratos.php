<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Str;

class extratos extends Model {

    protected $connection= 'mysql';

    protected $table = 'extratos';
    
    protected $fillable = ['user_id', 'data', 'descricao', 'valor', 'beneficiado', 'comprovante'];

    public function userName($id) {
        $dat = User::where('id', $id)->first();
        if ($id == 1) {
            $dat['username'] = 'Administração';
        }
        return $dat['username'];
    }

    static function criarExtrato($beneficiado, $valor, $desc, $comprovante, $tipo, $admin = 1) {
        /*
         * tipo 1: pontos binários
         * tipo 2: novo pagamento
         * tipo 3: Upgrade
         * tipo 4: Bônus de indicação
         * tipo 5: saque
         * 
         * 
         * 
         */
        $today = date("Y-m-d");
        extratos::create([
            'user_id' => $admin,
            'data' => $today,
            'descricao' => $desc,
            'valor' => $valor,
            'beneficiado' => $beneficiado,
            'comprovante' => $comprovante
        ]);
    }

}
