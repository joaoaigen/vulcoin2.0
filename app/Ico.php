<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ico extends Model
{
    protected $connection = 'crypto';

    protected $table = 'icos';

    public $timestamps = true;

    protected $fillable = [
        'start',
        'end',
        'quant',
        'price',
        'sold',
        'status'
    ];

    public function getActive(){
        return $this->where('status', 1)->get();
    }

}
