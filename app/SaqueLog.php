<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaqueLog extends Model
{
    public $timestamps = true;

    protected $fillable = [
        'id',
        'user_id',
        'nome_usuario',
        'bitzpayer_id',
        'saldo_anterior',
        'saque_realizado',
        'saldo_restante',
        'mensagem',
        'status',
    ];

    public function user(){
        return $this->belongsTo(User::class);
    }
}
