<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suporte extends Model
{

    protected $connection= 'mysql';

    protected $table = 'suporte';

    protected $fillable = [
        'user_id', 'assunto', 'body', 'suporte_user', 'status', 'mensagem_author'
    ];
}
