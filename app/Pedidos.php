<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pedidos extends Model
{
    protected $connection= 'mysql';

    protected $table = 'pedidos';
    
    public $timestamps = false;
    protected $fillable = array('status','info');
}
