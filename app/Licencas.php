<?php

namespace App;

use Avatar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;
use Str;
use App\User;
use App\Binario;
use App\extratos;
use App\config;
use Hash;

class Licencas extends Model {

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['id', 'cliente', 'data', 'licenca'];

}
