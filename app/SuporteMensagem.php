<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SuporteMensagem extends Model
{
    protected $connection= 'mysql';

    protected $table = 'suporte_mensagens';

    protected $fillable = [
        'id', 'id_suporte', 'mensagem', 'id_user', 'created_at', 'updated_at'
    ];
}
