<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pacote extends Model {

    protected $fillable = ['teto_diario','valor_ponto_binario','nome', 'valor_binario', 'descricao', 'produtos', 'valor', 'status', 'binario', 'indicacao_direta'];

    protected $connection= 'mysql';

    protected $table = 'pacotes';
    
    public function expirados() {
        
    }

}
