<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Produtos extends Model {

    public $timestamps = false;
    
    protected $connection= 'mysql';

    protected $table = 'produtos';
    
    protected $fillable = array('porcentagem_pontos', 'vendas', 'nome', 'descricao', 'descontos', 'preco', 'peso', 'img', 'categoria', 'estoque', 'status');

    public function add_venda($produto, $qntd_vendas) {

        $prod = Produtos::where('id', $produto);
        if ($prod->first()['id']) {
            $total_vendas = $prod->first()['vendas'] + $qntd_vendas;
            $total_estoque = $prod->first()['estoque'] - $qntd_vendas;
            $prod->update(['vendas' => $total_vendas, 'estoque' => $total_estoque]);
        }
    }
    public static function atualizaFormatousd(){

       
    }

}
