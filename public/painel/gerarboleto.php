<?php

require 'autoloader.php';
require 'admin.ini.php';

use OpenBoleto\Banco\Itau;
use OpenBoleto\Banco\Santander;
use OpenBoleto\Banco\BancoDoBrasil;
use OpenBoleto\Banco\Bradesco;



if ($bancotype == "BancoDoBrasil") {
	$boleto = new BancoDoBrasil(array(
    // Parâmetros obrigatórios
    'dataVencimento' => new DateTime($datab),
    'valor' => $valorb,
    'sequencial' => $sequencialb, // 8 dígitos
    'sacado' => $sacado,
    'cedente' => $cedente,
    'agencia' => $agenciab, // 4 dígitos
    'carteira' => $carteirab, // 3 dígitos
    'conta' => $contab, // 5 dígitos
    

));
}elseif ($bancotype == "Bradesco") {
	$boleto = new Bradesco(array(
    // Parâmetros obrigatórios
    'dataVencimento' => new DateTime($datab),
    'valor' => $valorb,
    'sequencial' => $sequencialb, // 8 dígitos
    'sacado' => $sacado,
    'cedente' => $cedente,
    'agencia' => $agenciab, // 4 dígitos
    'carteira' => $carteirab, // 3 dígitos
    'conta' => $contab, // 5 dígitos
    

));
}elseif ($bancotype == "Itau") {
	$boleto = new Itau(array(
    // Parâmetros obrigatórios
    'dataVencimento' => new DateTime($datab),
    'valor' => $valorb,
    'sequencial' => $sequencialb, // 8 dígitos
    'sacado' => $sacado,
    'cedente' => $cedente,
    'agencia' => $agenciab, // 4 dígitos
    'carteira' => $carteirab, // 3 dígitos
    'conta' => $contab, // 5 dígitos
    

));
}
elseif ($bancotype == "Santander") {
	$boleto = new Santander(array(
    // Parâmetros obrigatórios
    'dataVencimento' => new DateTime($datab),
    'valor' => $valorb,
    'sequencial' => $sequencialb, // 8 dígitos
    'sacado' => $sacado,
    'cedente' => $cedente,
    'agencia' => $agenciab, // 4 dígitos
    'carteira' => $carteirab, // 3 dígitos
    'conta' => $contab, // 5 dígitos
    

));
}

echo $boleto->getOutput();
