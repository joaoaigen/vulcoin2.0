<?php

/**
 * Step 4
 *
 * In this step, you'll confirm on your database invoice payment
 *
 */
define('MY_SHOP_LOGIN', 'xxxxxxxxxxxxxx');
define('MY_SHOP_SECRET', 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa');

$shop_secret = MY_SHOP_SECRET;

$message = 'R' . $_POST['shop_login'] . $_POST['amount'] . $_POST['currency'] . $_POST['invoice_id'] . $_POST['datetime'];
$signature = strtoupper(hash_hmac('sha256', pack('A*', $message), pack('A*', $shop_secret)));

if ($signature === $_POST['signature']) {

    # load your invoice
    $invoice = mysql_query('SELECT * FROM my_invoice_table WHERE invoice_id = :invoice_id');

    # check if payment was not confirmed yet
    if ($invoice['payment_confirmed'] == false) {
        # if was not confirmed, confirm payment, save informaton and send goods to customer
        $query = mysql_query('UPDATE my_invoice_table SET payment_confirmed = true WHERE invoice_id = :invoice_id');
        # send goods to customer
    }

    # answer to the api know that the information was received successfully
    print 'Confirmed!';

    # end script execution
    exit;
} else {
    # or this there's some issue in your code or there's someone instead of us trying to confirm this transaction
}

# end script execution with empty response
exit;