<?php

/**
 * Step 1.a
 *
 * This page must only show your client a success message, it doesn't save or update anything on your database
 * You just have to implement your layout
 *
 */

echo '<p>Your payment was received successfully</p>';