<?php

/**
 * Step 1.b
 *
 * This page must only show your client an error message, it doesn't save or update anything on your database
 * You just have to implement your layout
 *
 */

echo '<p>An error has occurred trying proceed with your payment</p>';