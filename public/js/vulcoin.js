var url = location.origin;

//Validações do campo de investimento para ativação de usuário na view "inativo_gnet"
$('#qntd_vulcoins').blur(function(){
    $quantidade = parseFloat($('#qntd_vulcoins').val().toString().replace(',', '.'));

    if($quantidade < 12){
        $('#qntd_vulcoins').val(12);
        $quantidade = 12;
    }
        

    $('#qntd_total_usd').val($quantidade * parseFloat($('#valor_vulcoin').val()) + ' USD');    
});

$('#btn_submit_investir').click(function(){
    if($.trim($('#qntd_vulcoins').val()) === '' || $.trim($('#qntd_vulcoins').val()) === null){
        swal("Campos incorretos!", "Quantidade de vulcoins inválida!", "error");
    }else{
        var form = $('#form_investimento').serialize();
        console.log(form);
        $.ajax({
            url: url + '/investir',
            method: 'GET',
            dataType: 'JSON',
            data: form,
            beforeSend: function (xhr) {
                
            },
            success: function (data) { 
                if(data === true)
                    swal("Solicitação enviada!", "Sua solicitação foi enviada e será ativada em até 24hrs!", "success");
            },
            error: function (xhr, ajaxOptions, thrownError) {
                swal("Error!", "Ocorreu um erro durante a ativação, contate um administrador! Erro: " + thrownError + ' Status: ' + ajaxOptions, "error");
            }
    
        });
    }
    
});