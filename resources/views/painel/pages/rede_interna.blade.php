@extends('layouts.app')
@section('title-head')
    Minha Rede
@endsection
@section('title-body')
    Minha Rede  
@endsection
@section('page-css')

@endsection
@section('main-content')
<section class="content">
    <div class="row">
        <div class="col-12 col-md-2 col-xl-2">
            <a class="box box-link-pop text-center" href="javascript:void(0)">
                <div class="box-body">
                    <p class="font-size-40 text-warning">
                        <strong>{{Auth::user()->totalEsquerda()}}</strong>
                    </p>
                </div>
                <div class="box-body py-25 bg-light">
                    <p class="font-weight-600">
                        <i class="fa fa-ticket text-muted mr-5"></i> Total Esquerda
                    </p>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-8 col-xl-8">
            <a class="box box-link-pop text-center" href="javascript:void(0)">
                <div class="box-body">
                    <p> Buscar Usuario</p>
                    <form class="formAjax form-horizontal" action="{{url('painel/minha-rede/busca')}}" method="POST">
                        {{csrf_field()}}
                        <div class="form-group">
                            <label>                                
                                <input name="busca" class="form-control" placeholder="Login ou ID" style="width: auto; border-radius: 3px;height: auto">
                            </label>
                        </div>

                        <button type="submit" class="btn btn-primary">Buscar</button>
                    </form>
                </div>
            </a>
        </div>
        <div class="col-12 col-md-2 col-xl-2">
            <a class="box box-link-pop text-center" href="javascript:void(0)">
                <div class="box-body">
                    <p class="font-size-40 text-warning">
                        <strong>{{Auth::user()->totalDireita()}}</strong>
                    </p>
                </div>
                <div class="box-body py-25 bg-light">
                    <p class="font-weight-600">
                        <i class="fa fa-ticket text-muted mr-5"></i> Total Direita
                    </p>
                </div>
            </a>
        </div> 
    </div>   
    <!-- /.row -->
</section>

<section class="content">
    <?php
    $usr = new App\User;
    $users = $usr->getFilhos($user_interna->id);

    foreach ($users as $row) {
        $users2[$row->id] = $row->getFilhos();       

        if (isset($row->direcao) && $row->direcao == 'direita') {
            $direita[] = $row;
        } else {
            $esquerda[] = $row;
        }    
    }
    
    ?>
    {{-- PRIMEIRO USUÁRIO DA REDE   --}}
    <div class="row d-flex justify-content-center">
        <div class="col-lg-4 col-1">
            
        </div>
        
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>
                            
                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="{{ $user_interna->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">{{ $user_interna->name }}</a></h5>
                            <span>{{ $user_interna->username }}</span>
                        </div>
                    </div>
                </div>
            </div> 
        </div>	
        
        <div class="col-lg-6 col-1">
            
        </div>
    </div>
    {{-- FIM DO PRIMEIRO USUÁRIO DA REDE   --}}
    
    
    <div class="row">
        {{-- SEGUNDA FILEIRA DE USUÁRIOS DA REDE   --}}
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-lg-4 col-1">

                </div>

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>
                            
                        </div>
                        <div class="text-center pt-3">
                            <a href="{{isset($esquerda[0]->id) ? url('/painel/minha-rede/'.($esquerda[0]->id)) : '#'}}">
                                <img class="avatar avatar-xxl" src="{{ isset($esquerda[0]) ? $esquerda[0]->photo : 'https://bo.vulcoin.io/img/avatar-Masculino.png' }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{isset($esquerda[0]->id) ? url('/painel/minha-rede/'.($esquerda[0]->id)) : '#'}}">{{ isset($esquerda[0]) ? $esquerda[0]->name : 'Usuário Inativo' }}</a></h5>
                            <span>{{ isset($esquerda[0]) ? $esquerda[0]->username : 'Usuário Inativo' }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-lg-1 col-1">

                </div>
            </div> 
        </div> 
        
        <div class="col-lg-6 col-12">
            <div class="row">  
                <div class="col-lg-4 col-1">

                </div>

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-inverse bg-dark bg-hexagons-white pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox" checked>
                                <i class="fa fa-star"></i>
                            </label>
                            
                        </div>
                        <div class="text-center pt-3">
                            <a href="{{isset($direita[0]->id) ? url('/painel/minha-rede/'.($direita[0]->id)) : '#'}}">
                                <img class="avatar avatar-xxl" src="{{ isset($direita[0]) ? $direita[0]->photo : 'https://bo.vulcoin.io/img/avatar-Masculino.png' }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a class="text-white" href="{{isset($direita[0]->id) ? url('/painel/minha-rede/'.($direita[0]->id)) : '#'}}">{{ isset($direita[0]) ? $direita[0]->name : 'Usuário Inativo' }}</a></h5>
                            <span>{{ isset($direita[0]) ? $direita[0]->username : 'Usuário Inativo' }}</span>
                        </div>
                    </div> 
                </div>

                <div class="col-lg-1 col-1">

                </div>

            </div>
        </div>
        
        {{-- FIM SEGUNDA FILEIRA DE USUÁRIOS DA REDE   --}}
    </div>
    
    <div class="row">
        {{-- Terceira fileira de usuários lado esquerdo --}}
        @if(isset($esquerda[0]->id) && isset($users2[$esquerda[0]->id]) && count($users2[$esquerda[0]->id]) == 2)
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="{{ url('/painel/minha-rede/'.($users2[$esquerda[0]->id][1]->id))}}">
                                <img class="avatar avatar-xxl" src="{{ $users2[$esquerda[0]->id][1]->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{ url('/painel/minha-rede/'.($users2[$esquerda[0]->id][1]->id))}}">{{ $users2[$esquerda[0]->id][1]->name }}</a></h5>
                            <span>{{ $users2[$esquerda[0]->id][1]->username }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-1 col-lg-4">

                </div> 

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="{{ url('/painel/minha-rede/'.($users2[$esquerda[0]->id][0]->id))}}">
                                <img class="avatar avatar-xxl" src="{{ $users2[$esquerda[0]->id][0]->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{ url('/painel/minha-rede/'.($users2[$esquerda[0]->id][0]->id))}}">{{ $users2[$esquerda[0]->id][0]->name }}</a></h5>
                            <span>{{ $users2[$esquerda[0]->id][0]->username }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @elseif(isset($esquerda[0]->id) && isset($users2[$esquerda[0]->id]) && count($users2[$esquerda[0]->id]) == 1 && $users2[$esquerda[0]->id][0]->direcao == 'direita')
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="https://bo.worldcryptocoin.io/img/avatar-Masculino.png" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">Usuário Inativo</a></h5>
                            <span>Usuário Inativo</span>
                        </div>
                    </div>
                </div>

                <div class="col-1 col-lg-4">

                </div> 

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="{{ url('/painel/minha-rede/'.($users2[$esquerda[0]->id][0]->id))}}">
                                <img class="avatar avatar-xxl" src="{{ $users2[$esquerda[0]->id][0]->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{ url('/painel/minha-rede/'.($users2[$esquerda[0]->id][0]->id))}}">{{ $users2[$esquerda[0]->id][0]->name }}</a></h5>
                            <span>{{ $users2[$esquerda[0]->id][0]->username }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        @elseif(isset($esquerda[0]->id) && isset($users2[$esquerda[0]->id]) && count($users2[$esquerda[0]->id]) == 1 && $users2[$esquerda[0]->id][0]->direcao == 'esquerda')
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="{{ url('/painel/minha-rede/'.($users2[$esquerda[0]->id][0]->id))}}">
                                <img class="avatar avatar-xxl" src="{{ $users2[$esquerda[0]->id][0]->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{ url('/painel/minha-rede/'.($users2[$esquerda[0]->id][0]->id))}}">{{ $users2[$esquerda[0]->id][0]->name }}</a></h5>
                            <span>{{ $users2[$esquerda[0]->id][0]->username }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-1 col-lg-4">

                </div> 

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="https://bo.worldcryptocoin.io/img/avatar-Masculino.png" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">Usuário Inativo</a></h5>
                            <span>Usuário Inativo</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        @else
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="https://bo.worldcryptocoin.io/img/avatar-Masculino.png" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">Usuário Inativo</a></h5>
                            <span>Usuário Inativo</span>
                        </div>
                    </div>
                </div>

                <div class="col-1 col-lg-4">

                </div> 

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="https://bo.worldcryptocoin.io/img/avatar-Masculino.png" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">Usuário Inativo</a></h5>
                            <span>Usuário Inativo</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        @endif
        
        {{-- FIM Terceira fileira de usuários lado direito --}}
        
        @if(isset($direita[0]->id) && isset($users2[$direita[0]->id]) && count($users2[$direita[0]->id]) == 2)
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="{{ url('/painel/minha-rede/'.($users2[$direita[0]->id][1]->id))}}">
                                <img class="avatar avatar-xxl" src="{{ $users2[$direita[0]->id][1]->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{ url('/painel/minha-rede/'.($users2[$direita[0]->id][1]->id))}}">{{ $users2[$direita[0]->id][1]->name }}</a></h5>
                            <span>{{ $users2[$direita[0]->id][1]->username }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-1 col-lg-4">

                </div> 

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="{{ url('/painel/minha-rede/'.($users2[$direita[0]->id][0]->id))}}">
                                <img class="avatar avatar-xxl" src="{{ $users2[$direita[0]->id][0]->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{ url('/painel/minha-rede/'.($users2[$direita[0]->id][0]->id))}}">{{ $users2[$direita[0]->id][0]->name }}</a></h5>
                            <span>{{ $users2[$direita[0]->id][0]->username }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @elseif(isset($direita[0]->id) && isset($users2[$direita[0]->id]) && count($users2[$direita[0]->id]) == 1 && $users2[$direita[0]->id][0]->direcao == 'direita')
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="https://bo.worldcryptocoin.io/img/avatar-Masculino.png" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">Usuário Inativo</a></h5>
                            <span>Usuário Inativo</span>
                        </div>
                    </div>
                </div>

                <div class="col-1 col-lg-4">

                </div> 

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="{{ url('/painel/minha-rede/'.($users2[$direita[0]->id][0]->id))}}">
                                <img class="avatar avatar-xxl" src="{{ $users2[$direita[0]->id][0]->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{ url('/painel/minha-rede/'.($users2[$direita[0]->id][0]->id))}}">{{ $users2[$direita[0]->id][0]->name }}</a></h5>
                            <span>{{ $users2[$direita[0]->id][0]->username }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        @elseif(isset($direita[0]->id) && isset($users2[$direita[0]->id]) && count($users2[$direita[0]->id]) == 1 && $users2[$direita[0]->id][0]->direcao == 'esquerda')
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="{{ url('/painel/minha-rede/'.($users2[$direita[0]->id][0]->id))}}">
                                <img class="avatar avatar-xxl" src="{{ $users2[$direita[0]->id][0]->photo }}" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="{{ url('/painel/minha-rede/'.($users2[$direita[0]->id][0]->id))}}">{{ $users2[$direita[0]->id][0]->name }}</a></h5>
                            <span>{{ $users2[$direita[0]->id][0]->username }}</span>
                        </div>
                    </div>
                </div>

                <div class="col-1 col-lg-4">

                </div> 

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="https://bo.worldcryptocoin.io/img/avatar-Masculino.png" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">Usuário Inativo</a></h5>
                            <span>Usuário Inativo</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        @else
        <div class="col-lg-6 col-12">
            <div class="row">
                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="https://bo.worldcryptocoin.io/img/avatar-Masculino.png" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">Usuário Inativo</a></h5>
                            <span>Usuário Inativo</span>
                        </div>
                    </div>
                </div>

                <div class="col-1 col-lg-4">

                </div> 

                <div class="col-12 col-lg-4">
                    <div class="box box-body bg-hexagons-dark pull-up">
                        <div class="flexbox align-items-center">
                            <label class="toggler toggler-yellow">
                                <input type="checkbox">
                                <i class="fa fa-star"></i>
                            </label>

                        </div>
                        <div class="text-center pt-3">
                            <a href="#">
                                <img class="avatar avatar-xxl" src="https://bo.worldcryptocoin.io/img/avatar-Masculino.png" alt="">
                            </a>
                            <h5 class="mt-15 mb-0"><a href="#">Usuário Inativo</a></h5>
                            <span>Usuário Inativo</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>        
        @endif
    </div>
    
    
</section>
@endsection
@section('page-js')
    
	<!-- popper -->
	<script src="{{ asset('../assets/assets/vendor_components/popper/dist/popper.min.js') }}"></script>
	
@endsection

