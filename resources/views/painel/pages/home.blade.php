@extends('layouts.app')
@section('title-head')
    @lang('messages.dashboard')
@endsection
@section('title-body')
    @lang('messages.dashboard')
@endsection
@section('page-css')
    <link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
@endsection
@section('main-content')
    <!-- Main content -->
    <section class="content">
        <div class="row">

            @if(DB::table('mensagem_usuarios')->where('id', '=', 1)->first()->ativo == 1)
            <div class="col-lg-12 col-12">
                <div class="box">
                    <div class="box-body tickers-block">
                        <ul id="webticker-5">
                            <li>
                                <div><span class="text-danger">Mensagem importante:</span> {{ DB::table('mensagem_usuarios')->where('id', '=', 1)->first()->mensagem }}</div>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            @endif
            <?php
            /*$curl = curl_init();
            curl_setopt_array($curl, [
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_URL => "https://api.crex24.com/CryptoExchangeService/BotPublic/ReturnTicker?request=[NamePairs=BTC_VLC,BTC_LTC,BTC_DOGE,BTC_NSD,BTC_ETH]"
            ]);

            $result = json_decode(curl_exec($curl));
            curl_close($curl);*/
            ?>

                @isset($result->Tickers)
            <div class="col-lg-12 col-12">
                <div class="box">
                    <div class="box-body tickers-block">
                        <ul id="webticker-7">

                            @foreach($result->Tickers as $row)
                            <li>
                                <div class="coin-name">{{ str_replace('_', '/', $row->PairName )}}</div>
                                <div><span class="text-danger">Last:</span> {{ number_format($row->Last, 8) }}</div>
                                <div><span class="text-success">High:</span> {{ number_format($row->HighPrice, 8) }}</div>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
            @endisset


            <div class="col-xl-3 col-md-6 col-12">
                <!-- small box -->
                <div class="small-box bg-success pull-up bg-hexagons-white">
                    <div class="inner">
                        <h3>$ {{ e(number_format(Auth::user()->saldo, 2, ',', '.')) }}</h3>

                        <p>@lang('messages.saldo')</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-usd"></i>
                    </div>

                    <a href="#" class="small-box-footer">@lang('messages.valor-investido'):
                        <?php
                        /* $investido = isset(DB::table('plano_investimento')->where('usuario', '=', Auth::user()->id)->first()->valor_pacote) ? DB::table('plano_investimento')->where('usuario', '=', Auth::user()->id)->first()->valor_pacote : 0;

                          $valores = \DB::table('rendimentos')->where('rendimento_id_usuario', '=', Auth::user()->id)->select(\DB::raw('SUM(rendimento_valor_investido) as valor_investido'), \DB::raw('SUM(rendimento_valor_investido_atual) as valor_investido_atual'))->first();
                          $saldo = $valores->valor_investido + Auth::user()->valorinvestido + $investido;

                          echo e(number_format($saldo, 2, ',', '.')); */
                        $valores = \DB::table('plano_investimento')->where('usuario', '=', Auth::user()->id)->first();

                        if (isset($valores)) {
                            echo '$ ' . e(number_format($valores->valor_pacote, 2, ',', '.'));
                        } else {
                            echo '$ ' . e(number_format(0, 2, ',', '.'));
                        }
                        ?>
                    </a>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12">
                <!-- small box -->
                <div class="small-box bg-info pull-up bg-hexagons-white">
                    <div class="inner">
                        <h3><?php echo e(Auth::user()->display_children()); ?></h3>

                        <p>@lang('messages.total-rede')</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user"></i>
                    </div>

                    <a href="<?php echo e(url('painel/minha-rede')); ?>" class="small-box-footer">@lang('messages.visualizar-rede') <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12">
                <!-- small box -->
                <div class="small-box bg-purple pull-up bg-hexagons-white">
                    <div class="inner">
                        <h3><?php
                            $valores = \DB::table('rendimentos')->where('rendimento_id_usuario', '=', Auth::user()->id)->select(\DB::raw('SUM(rendimento_valor_investido) as valor_investido'), \DB::raw('SUM(rendimento_valor_investido_atual) as valor_investido_atual'))->first();

                            echo '$ '. e(number_format(Auth::user()->totalGanhos() + $valores->valor_investido_atual, 2, ',', '.'));
                            ?></h3>

                        <p>@lang('messages.total-ganhos')</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-university"></i>
                    </div>

                    <a href="<?php echo e(url('painel/transacoes')); ?>" class="small-box-footer">@lang('messages.extrato-financeiro') <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-xl-3 col-md-6 col-12">
                <!-- small box -->
                <div class="small-box bg-danger pull-up bg-hexagons-white">
                    <div class="inner">
                        <h3><?php
                            $saldo = Auth::user()->saldo_vulcoins;
                            $usr = new \App\User();
                            $vulcoins = $usr->saldoVulcoins();
                            if ($vulcoins) {
                                echo e($vulcoins);
                            } else {
                                echo '0';
                            }
                            ?>
                        </h3>

                        <p>@lang('messages.saldo-vulcoin')</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-viacoin"></i>
                    </div>

                    <a href="{{ '#'/*route('sacar')*/}}" class="small-box-footer">@lang('messages.sacar-vulcoin') <i class="fa fa-arrow-right"></i></a>
                </div>
            </div>

            <div class="col-xl-3 col-md-3 col-12">
                <a class="box box-link-pop text-center" href="javascript:void(0)">
                    <div class="box-body">
                        <p class="font-size-40 text-success">
                            <strong>{{ Auth::user()->binario_direita }} Pts</strong>
                        </p>
                    </div>
                    <div class="box-body py-25 bg-light">
                        <p class="font-weight-600">
                            Binário lado Direito
                        </p>
                    </div>
                </a>
                <a class="box box-link-pop text-center" href="javascript:void(0)">
                    <div class="box-body">
                        <p class="font-size-40 text-warning">
                            <strong>{{ Auth::user()->binario_esquerda }} Pts</strong>
                        </p>
                    </div>
                    <div class="box-body py-25 bg-light">
                        <p class="font-weight-600">
                            Binário lado Esquerdo
                        </p>
                    </div>
                </a>
            </div>


            <div class="col-xl-5 col-md-12 col-12">
                <div class="pull-left box collapsed-box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Link de indicação</h3>

                        <div class="box-tools pull-right">
                            <a href="{{ 'https://bo.worldcryptocoin.io/cadastro/'.Auth::user()->username }}" data-popup="tooltip" title="Cadastro" target="_blank">
                                <i class="fa fa-external-link fa-fw"></i>
                            </a>
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-plus"></i></button>
                        </div>
                        <!-- /.box-tools -->
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-link"></i></span>
                            <input type="text" class="form-control text-black" readonly onclick="this.select()" value="{{ 'https://bo.worldcryptocoin.io/cadastro/'.Auth::user()->username }}" />
                        </div><br><br>
                        <h3>DIREÇÃO DO BINÁRIO</h3>
                            <div class="demo-checkbox">
                                <input value="esquerda" class="change_dir" type="checkbox" id="esquerdo"  onclick="$('#direito').prop('checked', false);" {{ Auth::user()->direcao == 'esquerda' ? 'checked' : ''}} />
                                <label for="esquerdo">Esquerdo</label>
                                <input value="direita" class="change_dir" type="checkbox" id="direito" onclick="$('#esquerdo').prop('checked', false);" {{ Auth::user()->direcao == 'direita' ? 'checked' : ''}}/>
                                <label for="direito">Direito</label>
                            </div>
                        <br><br>
                        <h3>CARTEIRA VULCOIN</h3>

                        <div class="input-group">
                            <img src="data:image/png;base64, <?php  echo isset($qr_image) ? $qr_image : ''; ?> " />
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

                <div class="box box-widget widget-user" style="height: 241px">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-green-active">
                        <h3 class="widget-user-username">Patrocinador</h3>
                    </div>
                    <div class="widget-user-image">
                        @if(Auth::user()->sexo == 'Masculino')
                            <img class="rounded-circle" src="{{ Auth::user()->photo }}" alt="User Avatar" />
                        @endif
                        @if(Auth::user()->sexo == 'Feminino')
                            <img class="rounded-circle" src="{{ Auth::user()->photo }}" alt="User Avatar" />
                        @endif
                    </div>
                    <div class="box-footer">
                        <div class="row">
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">{{Auth::user()->getUserIndicador(Auth::user()->id)->username}} </h5>
                                    <span class="description-text">Usuário</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">{{Auth::user()->getUserIndicador(Auth::user()->id)->name}}</h5>
                                    <span class="description-text">Nome</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header">{{Auth::user()->getUserIndicador(Auth::user()->id)->telefone}}</h5>
                                    <span class="description-text">Telefone</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>

            </div>

            <div class="col-xl-4 col-md-4 col-12">
                <div class="flexbox flex-justified text-center bg-white">
                    <div class="no-shrink py-30">
                        <span class="icon-wallet font-size-50" style="color: #3b5998"></span>
                    </div>

                    <div class="py-30 bg-info bg-hexagons-white">
                        <div class="font-size-30">
                            <?php
                            $hoje = new DateTime(date('Y-m-d H:i:s'));
                            $data_saque = new DateTime(Auth::user()->block_saque);

                            $diff = $hoje->diff($data_saque);
                            $diff = $diff->format('%d');


                            if ($diff >= 1 && $data_saque > $hoje) {
                                echo $diff . ' dias';
                            }else{
                                echo "<i class='fa fa-smile-o' aria-hidden='true'></i>";
                            }
                            ?>
                        </div>
                        <span>
                            <?php
                            if ($diff >= 1 && $data_saque > $hoje) {
                                echo 'Próximo saque';
                            } else {
                                echo "Saque disponivel";
                            }
                            ?>
                        </span>
                    </div>
                </div>
                <br>
                <hr class="bg-green">
                <br>
                <div class="flexbox flex-justified text-center bg-white">
                    <div class="no-shrink py-30">
                        <span class="fa fa-repeat font-size-50" style="color: #3b5998"></span>
                    </div>

                    <div class="py-30 bg-teal bg-hexagons-white">
                        <div class="font-size-30">
                            <?php
                            $hoje = new DateTime(date('Y-m-d H:i:s'));
                            $data_saque = new DateTime(Auth::user()->block_conversao);


                            $diff = $hoje->diff($data_saque);
                            $diff = $diff->format('%d');

                            if ($diff >= 1 && $data_saque > $hoje) {
                                echo $diff . ' dias';
                            }else{
                                echo "<i class='fa fa-smile-o' aria-hidden='true'></i>";
                            }
                            ?>
                        </div>
                        <span>
                            <?php
                            if ($diff >= 1 && $data_saque > $hoje) {
                                echo 'Próxima conversão';
                            } else {
                                echo "Conversão disponivel";
                            }
                            ?>
                        </span>
                    </div>
                </div>
            </div>



            <div class="col-lg-6 col-12">
                <!-- Chart -->
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Plano de investimento</h3>

                        <div class="box-tools pull-right">
                            <button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                        </div>
                    </div>
                    <div class="box-body d-flex justify-content-center">
                        <div class="chart">
                            <div class="col-12">
                                <div id="chart" style="width: 600px;height: 400px;"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>


            <?php
                $curl = curl_init();
                curl_setopt_array($curl, [
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_URL => "https://api.crex24.com/CryptoExchangeService/BotPublic/ReturnTradeHistoryPub?request=[PairName=BTC_VLC][Count=5]"
                ]);

                $result = json_decode(curl_exec($curl));
                curl_close($curl);
                ?>

            @isset($result->Trades)
            <div class="col-xl-12 col-md-12 col-12">

                <div> </div>

                <div class="box box-inverse box-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">@lang('messages.historico-negocios') VLC/BTC</h3>
                    </div>
                    <div class="box-body">
                        <div class="table-responsive">
                            <table class="table no-margin no-border b-1 border-gray bg-dark">
                                <thead>
                                    <tr class="bg-light">
                                        <th style="color: #fff;">@lang('messages.hora') (UTC)</th>
                                        <th style="color: #fff;">@lang('messages.preco')</th>
                                        <th style="color: #fff;">@lang('messages.valor')</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($result->Trades as $row)
                                    <tr>
                                        <td>
                                            <a href="#" class="text-yellow hover-warning">{{ date('H:i:s', $row->DtCreateTS) }}</a>
                                        </td>
                                        @if($row->IsSell == 'true')
                                        <td class="text-red"> {{ number_format($row->CoinPrice, 8) }} </td>
                                        @else
                                        <td class="text-green"> {{ number_format($row->CoinPrice, 8) }} </td>
                                        @endif
                                        <td>{{ number_format($row->CoinCount, 8) }}</td>
                                    </tr>
                                    @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
            @endisset
        </div>
    </section>


    <!-- /.content -->
@endsection
@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- start - This is for export functionality only -->
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <script src="{{ asset('../assets/js/echarts.min.js') }}"></script>

    <script type="text/javascript">
        <?php
            $porcentagem_atual = DB::table('plano_investimento')->where('usuario', '=', Auth::user()->id)->first();
            $nome = isset($porcentagem_atual->porcentagem_atual) ? DB::table('pacotes_investimentos')->where('id', '=', $porcentagem_atual->pacote)->first()->nome : 'Sem investimentos';
        ?>
        $( document ).ready(function() {
            var myChart = echarts.init(document.getElementById('chart'));

        var option = {
            tooltip : {
                    formatter: "{a} <br/>{b} : {c}% <br/>"
            },
            toolbox: {
                feature: {
                    restore: {},
                    saveAsImage: {}
                }
            },
            series: [
            {
                name: '<?php echo $nome; ?>',
                type: 'gauge',
                detail: {formatter:'{value}%'},
                data: [{value: {{ isset($porcentagem_atual->porcentagem_atual) ? $porcentagem_atual->porcentagem_atual : 0 }}, name: '<?php echo $nome; ?>'}],
                max: 200
            }
           ]
        };


        myChart.setOption(option);


            $(".change_dir").click(function() {
        var lado = $(this).val();
        console.log(lado);
        $.ajax({
            'url': "muda_lado?lado=" + lado,
            dataType: 'html',
            'success': function(txt) {
                if (txt == 'ok') {
                    swal("Sucesso!", "O lado do binário foi alterado com sucesso!", "success");
                    // alert('O lado binário foi alterado com sucesso.');
                }
                if (txt == 'fail') {
                    swal("Erro!", "O lado não binário foi alterado com sucesso.", "error");
                }
            }
        });
    });
        });
    </script>

@endsection

