@extends('layouts.app')

@section('htmlheader_title')
    Dashboard
    @endsection

    @section('contentheader_title')

    @endsection

    @section('breadcrumb')
    @endsection

    @section('contentheader_description')

    @endsection

    @section('main-content')
            <!-- Small boxes (Stat box) -->
    <div class="row">

    </div><!-- /.row -->

    <div class="row">


    </div>

    <!-- Main row -->
    <div class="row">


        <section class="col-lg-4">

            <div class="panel panel-info text-center">
                <div class="panel-heading">
                    Efetuar Pagamento
                </div>
                <div class="panel-body">
                    <a href="{{url('painel/inativo/checkout')}}" target="_blank" class="btn btn-lg btn-primary">Clique aqui para efetuar o Pagamento</a>
                </div>
            </div>

            {{--<div class="nav-tabs-custom">--}}
                {{--<ul class="nav nav-tabs">--}}
                    {{--<li class="active"><a href="#cartao" data-toggle="tab"><i class="fa fa-credit-card"></i> Cartão de Crédito</a></li>--}}
                    {{--<li><a href="#boleto" data-toggle="tab"><i class="fa fa-bank"></i>Boleto*</a></li>--}}
                {{--</ul>--}}
                {{--<div class="tab-content">--}}
               
                    {{--<!-- /.tab-pane -->--}}
                    {{--<div class="tab-pane" id="boleto">--}}
                        {{--<img src="{{'/img/selo/selo02_200x60.gif'}}" class="pull-left" style="margin-bottom:10px;"/>--}}
                        {{--<form id="formBoleto" action="{{url('painel/inativo/boleto')}}" target="_blank" class="text-center" method="POST">--}}
                            {{--{{csrf_field()}}--}}
                            {{--<input type="hidden" name="senderHash" id="senderHash"/>--}}
                            {{--<input type="button" value="Gerar Boleto" id="gerarBoleto" disabled class="btn btn-lg btn-default">--}}

                            {{--<br/> <i class="pull-right">*Acrescimo de 1 Real.</i>--}}
                            {{--<br/>--}}


                        {{--</form>--}}

                    {{--</div>--}}
                    {{--<!-- /.tab-pane -->--}}
                {{--</div>--}}
                {{--<!-- /.tab-content -->--}}
            {{--</div>--}}

        </section><!-- /.Left col -->


    </div><!-- /.row (main row) -->

@endsection

{{--@section('page_scripts')--}}
    {{--<script src="{{ env('CFURL').('/plugins/jquery-validation/jquery.validate.min.js') }}"></script>--}}
    {{--<script src="{{ env('CFURL').('/plugins/jquery-validation/localization/messages_pt_BR.min.js') }}"></script>--}}
    {{--<script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>--}}
    {{--<script type="text/javascript" src="https://stc{{env('PAGSEGURO_SANDBOX') ? '.sandbox' : ''}}.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js"></script>--}}

    {{--<script type="text/javascript">--}}

        {{--function fBlockUi() {--}}
            {{--$.blockUI({--}}
                {{--message: "<h4>Por favor aguarde...</h4>",--}}
                {{--css: {--}}
                    {{--border: 'none',--}}
                    {{--padding: '5px',--}}
                    {{--backgroundColor: '#000',--}}
                    {{--'-webkit-border-radius': '5px',--}}
                    {{--'-moz-border-radius': '5px',--}}
                    {{--opacity: .5,--}}
                    {{--color: '#fff'--}}
                {{--}--}}
            {{--});--}}
        {{--}--}}

        {{--PagSeguroDirectPayment.setSessionId('{{$idSessao}}');--}}
        {{--$(document).ready(function () {--}}

            {{--$('#gerarBoleto').attr('disabled', false);--}}

            {{--$('#gerarBoleto').click(function () {--}}
                {{--fBlockUi();--}}
                {{--var senderHash = PagSeguroDirectPayment.getSenderHash();--}}
                {{--if(senderHash != undefined && senderHash != '') {--}}
                    {{--$("#senderHash").val(senderHash);--}}
                    {{--$("#formBoleto").submit();--}}
                {{--}--}}
                {{--$.unblockUI();--}}
            {{--});--}}


            {{--$('#pagarCartao').click(function () {--}}
                {{--fBlockUi();--}}

                {{--PagSeguroDirectPayment.getBrand({--}}

                    {{--cardBin: $("#numero-cartao").val(),--}}

                    {{--success: function (response) {--}}
                        {{--var cardBrand = response.brand.name;--}}
                        {{--var param = {--}}
                            {{--cardNumber: $("#numero-cartao").val(),--}}
                            {{--cvv: $("#cvv-cartao").val(),--}}
                            {{--expirationMonth: $("#mes-cartao").val(),--}}
                            {{--expirationYear: $("#ano-cartao").val(),--}}
                            {{--brand: cardBrand,--}}
                            {{--success: function (response) {--}}
                                {{--$("#cardToken").val(response.card.token);--}}
                                {{--$("#senderHashCard").val(PagSeguroDirectPayment.getSenderHash());--}}
                                {{--$("#formCreditCard").submit();--}}
                                {{--//$.unblockUI();--}}
                            {{--},--}}
                            {{--error: function (response) {--}}
                                {{--$.unblockUI();--}}
                            {{--},--}}
                            {{--complete: function (response) {--}}
                                {{--//console.log(response);--}}
                            {{--}--}}
                        {{--};--}}

                        {{--PagSeguroDirectPayment.createCardToken(param);--}}
                    {{--},--}}
                    {{--error: function (response) {--}}
                        {{--$.unblockUI();--}}
                    {{--},--}}
                    {{--complete: function (response) {--}}
                        {{--//tratamento comum para todas chamadas--}}
                    {{--}--}}
                {{--});--}}

            {{--});--}}

        {{--});--}}

    {{--</script>--}}
{{--@endsection--}}