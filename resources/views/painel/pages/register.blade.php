@extends('layouts.app')

@section('htmlheader_title')
Cadastro
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Cadastro
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">
   <div class="register-box" style="width: 650px">
  

        @if (isset($errors) && count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        <div class="register-box-body">
            <form action="{{ url('/painel/cadastro') }}" method="post">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <div class="col-sm-6">

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required placeholder="Usuario" maxlength="20" name="username" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required placeholder="Nome Completo" name="name" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control cpf" required placeholder="CPF" name="cpf" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" required placeholder="Email" name="email" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" required placeholder="Senha" name="password" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" required placeholder="Repita a senha" name="password_confirmation" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <select name="sexo" class="form-control">
                            <option value="Masculino" selected="">
                                Masculino
                            </option>
                            <option value="Feminino">
                                Feminino
                            </option>
                      
                        </select>
                    </div>
                </div>
                <div class="col-sm-6">


                    <div class="form-group has-feedback">
                        <input type="text" class="form-control data" placeholder="Data de Nascimento" name="nascimento" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control telefone" data-inputmask="'mask': ['99-9999-9999[9]', '+99 99 9999-9999[9]']" data-mask placeholder="Telefone" name="telefone" value=""/>
                    </div>


                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Endereço" name="endereco" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Bairro" name="bairro" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Cidade" name="cidade" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" placeholder="Estado" name="estado" value=""/>
                    </div>
                     <div class="form-group has-feedback">
                        <select name="pacote" class="form-control">

                        @foreach($pacotes as $pacote)
                           @if ($pacote->status==1)
                            <option value="{{$pacote->id}}">
                                {{$pacote->nome}}-R${{$pacote->valor}}
                            </option>
                            @endif
                        @endforeach
                        </select>
                    </div>
                </div>


                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Enviar</button>
                    </div><!-- /.col -->
                </div>

            </form>
        </div><!-- /.form-box -->
    </div><!-- /.register-box -->


</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function () {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [[ 1, "desc" ]]
    });
});
</script>
 @include('layouts.partials.scripts_auth')

    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });

            $('.data').inputmask("dd-mm-yyyy");
            $('.cpf').inputmask("999.999.999-99");
            $("[data-mask]").inputmask();
        });
    </script>
@endsection