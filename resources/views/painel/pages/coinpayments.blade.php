@extends('layouts.app')
@section('title-head')
    @lang('messages.dashboard')
@endsection
@section('title-body')
    @lang('messages.dashboard')
@endsection
@section('page-css')

@endsection
@section('main-content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-12">
                <div class="box">
                    <div class="box-header with-border">
                        <h3 class="box-title">Saldo Disponível: </h3>
                        <h5> <?php
                            $usr = new \App\User();

                            echo $usr->saldoVulcoins();
                            ?> moedas</h5>
                    </div>
                    <div class="box-body">
                        <form method="post" action="{{ route('saque') }}">
                        {{ csrf_field() }}
                        <!-- Date dd/mm/yyyy -->
                            <div class="form-group">
                                <label>Valor a sacar</label>

                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="input-group-text">
                                            <i class="fa fa-viacoin"></i>
                                        </div>
                                    </div>

                                    <input type="text" class="form-control valor" id="saque" required="required" placeholder="Informe o valor que deseja sacar." name="saqueValor">
                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <button class="btn btn-primary" type="submit">Sacar</button>
                            </div>
                        </form>
                        <!-- /.form group -->
                    </div>
                    <!-- /.box-body -->
                </div>

            </div>
        </div>
    </section>


    <!-- /.content -->
@endsection
@section('page-js')
    <!-- This is data table -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>
@endsection

