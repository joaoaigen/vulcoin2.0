@extends('layouts.app')

@section('htmlheader_title')
Meus Vouchers
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Meus Vouchers
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Vouchers</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th style="display: none;">#</th>
                            <th>Voucher</th>
                            <th>Valor</th>
                            <th>Usuario Ativado</th>
                            <th>Ativar</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('vouchers', 'App\Voucher')

                        @foreach($vouchers->where('user_id', Auth::user()->id)->orderByRaw("id DESC")->get() as $voucher)
                        <tr>
                            <td style="display: none;">{{$voucher->id}}</td>
                            <td class="bg-info"><b>{{$voucher->voucher}}</b></td>
                            <td>R${{ number_format($voucher->valor, 2, ',', '.') }}</td>
                            <td><b>{{$voucher->getUserNameActivated()}}</b></td>
                            <td>
                                @if($voucher->status != 1)
                                    <a href="#" data-voucher="{{ $voucher->voucher }}" data-voucherid="{{ $voucher->id }}" class="btn btn-success openModal" data-toggle="tooltip" title="Ativar usuário!"><i class="glyphicon glyphicon-ok"></i></a>
                                @else
                                    Voucher utilizado
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

<!-- Modal -->
<div id="voucherModal" class="modal fade" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Ativar Voucher: <strong><span id="voucher-text"></span></strong></h4>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <select class="form-control" name="voucher-user" id="voucher-user" required>
                        <option value="">Selecione um usuário</option>
                        @foreach($users as $user)
                        <option value="{{ $user->id }}">{{ $user->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                <button type="button" id="active-voucher" class="btn btn-success">Ativar</button>
            </div>
        </div>

    </div>
</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2"></script>
<script>
$(function () {
    var voucher_id = 0;

    $(".openModal").click(function(e){
        e.preventDefault();
        var voucher = $(this).data('voucher');
        voucher_id = $(this).data('voucherid');
        $("#voucher-text").text(voucher);
        $("#voucherModal").modal();

    })

    $("#active-voucher").click(function(e){
        e.preventDefault();
        var user = $("#voucher-user").val();
        if(user == "")
        {
            swal("Atenção", "Selecione um usuário", "info");
        }
        $.get('/painel/vouchers/active/' + voucher_id + '/' + user, function(response){
            var data = jQuery.parseJSON(response);
            data = jQuery.parseJSON(data);
            if(data.status == 'success')
            {
                swal("Sucesso!", data.message, "success");

                swal({
                    title: 'Sucesso!',
                    text: data.message,
                    type: 'success',
                    confirmButtonColor: '#3085d6',
                    confirmButtonText: 'Fechar'
                }).then((result) => {
                    if (result.value) {
                        window.location.reload();
                    }
                })
            }
            else
            {
                swal("Falha!", data.message, "warning");
            }
            $("#voucherModal").modal('toggle');
        });
    })

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "order": [[ 0, "DESC" ]],
        "info": true,
        "autoWidth": true,
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
</script>
@endsection
