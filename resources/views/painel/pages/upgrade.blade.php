@extends('layouts.app')

@section('htmlheader_title')
Upgrade
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Upgrade
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Upgrade</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body">
                <div class="container">
                    <h2>Upgrade</h2>
                    <p>Pacote Atual: {{$pacoteAtual['nome']}}
                    <div class="col-md-6">
                        <div class="form-group">
                            <select name="qntd_cotas" id="qntd_cotas" class="form-control"><?php $i = 0;
?>                                    <option value="" selected="">Selecione um pacote</option>
                                @foreach($pacotes as $pacote)
                                <?php
                                $pacoteValor = $pacote['valor'];

                                //$totalTitulo = Auth::user()->totalCotas() + (floor($pacoteValor / 200));
                                if ($pacote['valor'] > $pacoteAtual['valor']) {
                                    ?>
                                    <option value="{{$pacote['id']}}">{{$pacote['nome']}} - R${{$pacoteValor}} <?php
                                        if (env('ct_nU9jppZkP8vbcK6a') == 1) {
                                            echo $pacote->total_cotas . ' cota(s)';
                                        }
                                        ?></option>
                                    <?php $i++; ?>
                                <?php } ?>
                                @endforeach
                            </select>
                        </div>


                        <a class="upgrade" data-pacote="{{$pacote['id']}}" data-metodo='5'  style="color: #fff; cursor: pointer;">
                            <div class="panel-footer  bg-blue-gradient">
                                <span id="euQuero" class="pagar pagSeguro">Pagar com PagSeguro(boleto e cartão de crédito)</span> <i class="fa fa-arrow-circle-right"></i>

                            </div>
                        </a>

                        <br>
                        <p>Essa fatura será armazenada e pode ser paga posteriormente .Para acessar suas faturas ou pagar faturas com saldo <a href='faturas'>Clique Aqui</a>.</p>
                       
                    </div>
                </div>
                <?php $i++; ?>
                <div class="clearfix"></div>

            </div>
            <br>





        </div>
</div>


</div><!-- /.box-body -->
</div><!-- /.box -->

</section>

</div>
<div id='transferenciaBan' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>


            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Dados das contas</h4>

            </div>
            <div class="modal-body">
                <?= $config['deposito_contas'] ?>

                <div class="modal-footer">

                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    </p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div>
    @endsection

    @section('page_scripts')
    <!-- DataTables -->
    <script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
    @if(env('PAGSEGURO_SANDBOX'))
    <script type="text/javascript"
    src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
    @else
    <script type="text/javascript"
    src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
    @endif
    <script>
$(function () {

    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
});
$(".deposito").click(function () {
    $("#transferenciaBan").modal();
});

$(".upgrade").click(function () {
    pacote = $("#qntd_cotas").val();
    if (pacote == '') {
        alert('Selecione um pacote');
    } else {
        ele = this;
        $(ele).find('#euQuero').html('Por favor aguarde...');
        $(ele).find('.panel-footer').removeClass('bg-blue-gradient');
        $(ele).find('.panel-footer').removeClass('bg-red-gradient');
        $(ele).find('.panel-footer').removeClass('bg-green-gradient');
        $(ele).find('.panel-footer').addClass('bg-orange');
        if ($(ele).attr('data-metodo') == 5) {
            elemento = $(ele);
            $(".pagSeguro").html('Por favor aguarde...');

            $.getJSON("?novopagamento=1&metodo=" + elemento.attr('data-metodo') + "&pacote=" + pacote, function (data) {
                if (data.url == 'error') {
                    alert(data.message[0]);
                    return false;
                } else {
                    elemento.removeClass('btn-primary');
                    elemento.addClass('btn-success')
                    elemento.html('Carregando...');
                    PagSeguroLightbox({
                        code: data.code[0]
                    }, {
                        success: function () {
                            location.href = "<?= url('painel/faturas?payment_succes') ?>";

                        },
                        abort: function () {
                            location.reload();


                        }
                    });
                }

            });

        } else
        if ($(ele).attr('data-metodo') == 4) {
            $(ele).find('.panel-footer').removeClass('bg-orange');
            $(ele).find('.panel-footer').addClass('bg-green');
            $(ele).find('#euQuero').html('Redirecionando...');
            setTimeout(function () {
                url = '<?= url("/painel/ver_fatura?novopagamento=1&metodo=3&tipo=2&pacote=") ?>' + pacote
                location.href = url;
            }, 2000);
        } else {
            $.ajax({
                'url': "?novopagamento=1&metodo=" + $(ele).attr('data-metodo') + "&pacote=" + pacote,
                dataType: 'html',
                'success': function (txt) {
                    if (txt == '') {
                        alert('Houve uma falha ao executar a operação.Verifique se seus dados foram inseridos corretamente.');
                    } else if (txt == 'ok') {
                        $("#transferenciaBan").modal();
                        $(ele).find('.panel-footer').removeClass('bg-orange');
                        $(ele).find('.panel-footer').addClass('bg-green');
                        $(ele).find('#euQuero').html('Fatura gerada com sucesso');

                    } else {
                        $(ele).find('.panel-footer').removeClass('bg-orange');
                        $(ele).find('.panel-footer').addClass('bg-green');
                        $(ele).find('#euQuero').html('Redirecionando...');
                        setTimeout(function () {
                            location.href = txt;
                        }, 2000);
                    }


                }
            });
        }
    }

});


    </script>
    @endsection
