@extends('layouts.app')
@section('title-head')
    @lang('messages.dashboard')
@endsection
@section('title-body')
    @lang('messages.dashboard')
@endsection
@section('page-css')

@endsection
@section('main-content')
    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-xl-12 col-md-12 col-12">
                <!-- small box -->
                <div class="small-box bg-success pull-up bg-hexagons-white">
                    <div class="inner">
                        <h3>$ {{ e(number_format(Auth::user()->saldo, 2, ',', '.')) }}</h3>

                        <p>@lang('messages.saldo')</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-usd"></i>
                    </div>

                    <a href="#" class="small-box-footer">@lang('messages.valor-investido'):
                        <?php

                        $valores = \DB::table('rendimentos')->where('rendimento_id_usuario', '=', Auth::user()->id)->select(\DB::raw('SUM(rendimento_valor_investido) as valor_investido'), \DB::raw('SUM(rendimento_valor_investido_atual) as valor_investido_atual'))->first();
                        $saldo = $valores->valor_investido + Auth::user()->valorinvestido;

                        echo e(number_format($saldo, 2, ',', '.'));
                        ?>
                    </a>
                </div>
            </div>
        </div>
    </section>


    <!-- /.content -->
@endsection
@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- start - This is for export functionality only -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>
@endsection

