@extends('layouts.app')
@section('title-head')
    Rendimentos
@endsection
@section('title-body')
    Rendimentos
@endsection
@section('page-css')

@endsection
@section('main-content')

    @inject('rendimentos', 'App\Rendimentos')

    <?php
    if (!isset($_GET['de']) or ! isset($_GET['ate'])) {
        $rend = $rendimentos->where('rendimento_id_usuario', \auth()->user()->id);
        $rendimentos = $rend->get();
    } else {
        $rend = $rendimentos->where('rendimento_id_usuario', \auth()->user()->id)->where('created_at', '>=', $_GET['de'])->where('created_at', '<=', $_GET['ate']);
        $rendimentos = $rend->get();
    }
    ?>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pesquisa por data</h3>
                        <h6 class="box-subtitle">Selecione as datas abaixo para personalizar a sua pesquisa.</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                <label for="de">De: </label>&emsp;
                                <input type="date" required="" class="form-control" id="de" value="<?= @$_GET['de'] ?>" name="de">
                            </div>
                            <div class="form-group">
                                <label for="de">Até: </label>&emsp;
                                <input type="date" required="" class="form-control" value="<?= @$_GET['ate'] ?>" id="ate" name="ate">
                            </div>
                            <button type="submit" class="btn btn-primary">Exibir</button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-12">

                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Rendimentos</h3>
                        <h6 class="box-subtitle">Listagem de todos os rendimentos do usuário: {{ Auth::user()->username }}</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @if(\Auth::user()->valorinvestido > 0)
                            <div class="row">
                                <div class="col-md-4">
                                    <h6 class="box-subtitle btn-primary btn-xs"><strong>Saldo total anterior:</strong> $ {{ \Auth::user()->valorinvestido }}</h6>
                                    <h6 class="box-subtitle btn-success btn-xs"><strong>Novo saldo:</strong> $ <?php

                                        $valores = \DB::table('rendimentos')->where('rendimento_id_usuario', '=', Auth::user()->id)->select(\DB::raw('SUM(rendimento_valor_investido) as valor_investido'), \DB::raw('SUM(rendimento_valor_investido_atual) as valor_investido_atual'))->first();
                                        $saldo = $valores->valor_investido + $valores->valor_investido_atual;

                                        echo e(number_format($saldo, 2, ',', '.'));
                                        ?></h6>
                                </div>
                            </div>
                        @endif
                        <div class="table-responsive">
                            <table id="rendimentos" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                <tr>
                                    <th>Id</th>
                                    <th>Valor investido</th>
                                    <th>Valor atual</th>
                                    <th>Percentual</th>
                                    <th>Valor máximo</th>
                                    <th>Aplicado em</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rendimentos as $rendimento)
                                    <tr>
                                        <td>{{$rendimento->id_rendimento}}</td>
                                        <td>{{$rendimento->rendimento_valor_investido}} USD</td>
                                        <td>{{$rendimento->rendimento_valor_investido_atual}} USD</td>
                                        <td style="color: {{ $rendimento->rendimento_percentual == 300 ? 'green' : 'yellow' }};">{{$rendimento->rendimento_percentual }} %</td>
                                        <td>{{$rendimento->rendimento_valor_investido_maximo}} USD</td>
                                        <td>{{date ('d/m/Y H:i:s', strtotime($rendimento->created_at))}}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- start - This is for export functionality only -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>
    <script type="text/javascript">
    $('#rendimentos').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 3, "asc" ]]
    });
    </script> 
@endsection

