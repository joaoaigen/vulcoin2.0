@extends('layouts.app')

@section('htmlheader_title')
Graduações
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Graduações
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">
        <style>     
            .dataTables_filter {
                display: none; 
            }
            .dataTables_length {
                display: none; 
            }  
        </style>  <div class="box box-solid">
            <div class="box-body">
                <div align="center">

                    <h4>Direita:<b> {{Auth::user()->total_bin_dir}} </b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;       Esquerda:<b> {{Auth::user()->total_bin_esq}}</b></h4>

                </div>
            </div>
            <!-- /.box-body -->
        </div>
        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Graduações</h3>
                <div class="box-tools pull-right">
                    <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                </div><!-- /.box-tools -->
            </div><!-- /.box-header -->
            <div class="box-body table-responsive">

                <div id="mensagemAdicionarVouchers">

                </div>

                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Nome</th>
                            <th>Requisitos</th>
                        </tr>
                    </thead>
                    <tbody>
                        @inject('graduacoes', 'App\graduacoes')
                        @inject('usr', 'App\User')
                        @foreach($graduacoes->where('status',1)->get() as $graduacao)
                        <tr>
                            <td>{{$graduacao['name']}}</td>
                            <?php
                            $requisitos = $usr->requisitosGraduacao(Auth::user()->id, $graduacao['id']);
                            $grad = $graduacoes->where('id', $graduacao['grad_required'])->first();
                            $grad2 = $graduacoes->where('id', $graduacao['grad_required1'])->first();

                            $bin_ok = '';
                            if ($requisitos['pontuacao'] == 0) {

                                $bin_ok = '<i class="fa fa-check bg-green-active"></i>';
                            } else {
                                $bin_ok = '<i class="fa fa-close bg-red"></i>';
                            }
                            $qntd_ok = '';

                            if ($requisitos['qntd_graduados'] == 0) {

                                $qntd_ok = '<i class="fa fa-check bg-green-active"></i>';
                            } else {
                                $qntd_ok = '<i class="fa fa-close bg-red"></i>';
                            }
                            if ($requisitos['qntd_graduados'] == 0) {

                                $qntd_ok = '<i class="fa fa-check bg-green-active"></i>';
                            } else {
                                $qntd_ok = '<i class="fa fa-close bg-red"></i>';
                            }
                            echo'<td>';
                            if ($graduacao['grad_required'] == 0) {
                                ?>
                                Você precisa de mais {{$requisitos['pontuacao']}} pontos <?= @$bin_ok ?><br>Você precisa de mais {{$requisitos['qntd_graduados']}} indicados diretos <?= @$qntd_ok ?>  

                    <?php } else {
                        ?>

                        Você precisa de mais {{$requisitos['pontuacao']}} pontos <?= @$bin_ok ?><br>Você precisa de mais {{$requisitos['qntd_graduados']}} filho(s) com a seguinte graduação: {{$grad['name']}} <?= @$qntd_ok ?>  

                        <?php
                    }
                    if ($requisitos[2]['qntd_graduados'] == 0) {

                        $qntd_ok1 = '<i class="fa fa-check bg-green-active"></i>';
                    } else {
                        $qntd_ok1 = '<i class="fa fa-close bg-red"></i>';
                    }

                    if ($graduacao[2]['grad_required'] == 0) {
                        ?>
                        <br>Você precisa de mais {{$requisitos[2]['qntd_graduados']}} indicados diretos <?= @$qntd_ok1 ?>  

                    <?php } else {
                        ?>

                        <br>Você precisa de mais {{$requisitos[2]['qntd_graduados']}} filho(s) com a seguinte graduação: {{$grad2['name']}} <?= @$qntd_ok1 ?>  

                    <?php }
                    ?>
                    </td>





                    </tr>
                    @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>

@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script>
$(function () {

    $('#example2').DataTable({
        "paging": true, "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "order": [[0, "desc"]],
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
</script>
@endsection
