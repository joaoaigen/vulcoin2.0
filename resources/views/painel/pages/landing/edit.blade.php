<form class="formAjax" method="post" action="{{url('/painel/landingpage/salvar')}}">

    {{ csrf_field() }}
    <?php

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar</h4>

    </div>
    <div class="modal-body">
        <input type="hidden" class="form-control"  name="id" value="<?= $dados['id'] ?>"/>

        <div class="form-group">
            <label>Link para o seu Facebook</label>
            <input type="url" class="form-control" value="<?= $dados['facebook_link'] ?>"  name="facebook_link" />
        </div>
        <div class="form-group">
            <label>Link para o seu Twitter</label>
            <input type="url" class="form-control" value="<?= $dados['twitter_link'] ?>"  name="twitter_link" />
        </div>
        <div class="form-group">
            <label>Video que será exibido na home</label>
            <input type="url" class="form-control" value="<?= $dados['youtube_video'] ?>"  name="youtube_video" required=""/>
        </div>
        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" value="<?= $dados['email'] ?>"  name="email" required=""/>
        </div>





    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>

