@extends('layouts.app')
@section('title-head')
    Indicados diretos
@endsection
@section('title-body')
    Indicados diretos
@endsection
@section('page-css')

@endsection
@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-12">

                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Indicados Diretos</h3>
                        <h6 class="box-subtitle">Listagem das indicações diretas do usuário: {{ Auth::user()->username }}</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="directs" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nome</th>
                                    <th>Username</th>
                                    <th>Email</th>
                                    <th>Telefone</th>
                                    <th>Direção</th>
                                </tr>
                                </thead>
                                <tbody>
                                @inject('usuarios', 'App\User')
                                @foreach($usuarios->getIndicados() as $user)
                                    <tr>
                                        <td>{{$user->id}}</td>
                                        <td><b>{{$user->name}}</b></td>
                                        <td><b>{{$user->username}}</b></td>
                                        <td>{{$user->email}}</td>
                                        <td>{{$user->telefone}}</td>
                                        <td>{{ \App\Referrals::where('user_id', '=', $user->id)->first()->direcao }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>
@endsection

@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- start - This is for export functionality only -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>
    <script type="text/javascript">
    $('#directs').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 0, "desc" ]]
    });
    </script> 
@endsection