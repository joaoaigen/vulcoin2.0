@extends('layouts.app')

@section('htmlheader_title')
Dashboard
@endsection

@section('contentheader_title')

@endsection

@section('breadcrumb')
@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Main row -->
<div class="row">
    <div class="col-md-12">
        <div class="callout callout-info" style="margin-top:5px; margin-bottom: 30px; background: linear-gradient(135deg,#FF7B0C 0,#FFC727 100%)!important; border: none; border-radius: 3px; padding: 15px 15px 1px;">
            <h4 style="font-size: 17px; color: #fff; margin-bottom: 15px;"><i class="fa fa-check" aria-hidden="true"></i> Parabéns! O seu cadastro foi realizado com sucesso!</h4>

        </div>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="header" align="center" style="background: #f1f1f1; padding: 10px 0px 10px 0px;">
                <p><b>Bem vindo ao sistema Vulcoin! Para efetivar o seu cadastro clique no botão abaixo, no painel tem as informações que você irá precisar para efetuar a ativação! No campo "Carteira de investimento", é a sua carteira para depósito de Vulcoins dentro sistema, fique atento ao valor mínimo para ativação que está descrito no campo "Valor mínimo de Vulcoins". Para ativar basta fazer o investimento da quantidade mínima de Vulcoins ou acima e aguardar no máximo 24hrs que sua conta será ativada!</b><br><small class="text-muted" style="text-transform:none !important;"></small></p>
                <br>
                <a onclick="$('#boletoModal').modal('show')" class="btn btn-info">PROSSEGUIR</a>
            </div>            
        </div>
    </div>
</div>






<!-- Modal -->
<div id="boletoModal" class="modal" role="dialog">
    <div class="modal-dialog modal-lg">
        <!-- Modal content-->
        <div class="modal-content">
            <div class="col-md-4 ui-sortable">
            </div>
            <div class="col-md-4 ui-sortable">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h4 class="panel-title">Investir em VULCOINS
                        </h4>
                    </div>
                    <div class="panel-body text-center">
                        <ul class="list-group">
                            <form id="form_investimento" method="GET">
                                <li class="list-group-item">Carteira de investimento: <strong style="font-size: 11px"> <?php
                                                                                                                        echo \Auth::user()->carteira;
                                                                                                                        ?></strong></li>
                                <li class="list-group-item">Preço: <strong>
                                        <?php

                                        $price = \DB::connection('crypto')->table('icos')->select('price', 'id')->orderBy('id', 'desc')->first();

                                        echo $price->price . ' USD';
                                        ?>
                                        <input hidden id="valor_vulcoin" value="<?php echo str_replace(',', '.', $price->price); ?>">
                                        <input hidden name="id_ico" value="<?php echo str_replace(',', '.', $price->price); ?>">
                                        <input hidden name="carteira" value="<?php echo \Auth::user()->carteira; ?>">
                                        <input hidden name="id_user" value="{{\Auth::id()}}">
                                        <input hidden value="300" name="qntd_vulcoins" id="qntd_vulcoins">

                                    </strong></li>
                                <li class="list-group-item">
                                    Valor mínimo de Vulcoins: <strong> <?php

                                                                        $preco = str_replace('.', '', $price->price);
                                                                        $preco = str_replace(',', '.', $preco);

                                                                        echo 30 / $preco; ?> </strong>
                                </li>
                                <li class="list-group-item">Qntd. de Vulcoins: <strong>
                                        <?php
                                        $price = \DB::connection('crypto')->table('icos')->select('quant')->orderBy('id', 'desc')->first();
                                        echo $price->quant;
                                        ?>
                                    </strong></li>
                            </form>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- REQUIRED JS SCRIPTS -->
<div id='transferenciaBan' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>


                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Dados das contas</h4>

                </div>
                <div class="modal-body">
                    <?= $config['deposito_contas'] ?>

                    <div class="modal-footer">

                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
            </p>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>

@endsection

@section('page_scripts')
<!-- iCheck 1.0.1 -->
<script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/form/jquery.form.min.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ env('CFURL').('/js/vulcoin.js')}}"></script>
@if(env('PAGSEGURO_SANDBOX'))
<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
@else
<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
@endif
<script type='text/javascript'>
    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });
    $("#transferencia").click(function() {
        $("#transferenciaBan").modal();
    });
    $(".gerarBoleto").click(function() {
        elemento = $(this);
        elemento.html('Por favor aguarde...');
        link = "?novopagamento=1&metodo=" + elemento.attr('data-metodo');
        $.getJSON(link, function(data) {
            if (data.url == 'error') {
                alert(data.message[0]);
                return false;
            } else {
                elemento.removeClass('btn-primary');
                elemento.addClass('btn-success')
                elemento.html('Carregando...');
                PagSeguroLightbox({
                    code: data.code[0]
                }, {
                    success: function() {
                        location.href = "<?= url('painel/faturas?payment_succes') ?>";

                    },
                    abort: function() {
                        location.reload();


                    }
                });
            }

        });
    });
    /**/
</script>
<div id='voucher' class="modal">
    <div class="modal-dialog">
        <div class="modal-content">

            <p>


                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Selecionar quantidade de Vouchers</h4>

                </div>
                <div class="modal-body">

                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="1">01</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="2">02</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="3">03</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="4">04</label>
                    </div>
                    <div class="checkbox">
                        <label><input type="radio" name="qntd" class="qntd" value="0">Nenhum</label>
                    </div>
                    <p>Valor total a pagar: R$<span id="valorPagar">200</span></p>


                    <div class="modal-footer">
                        <div id="resUpdate">
                        </div>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                        <button type="submit" id="gerarBoleto" class="btn btn-primary">Gerar Boleto</button>
            </p>
        </div>
    </div><!-- /.modal-content -->
</div><!-- /.modal-dialog -->
</div>

@endsection

@section('page_scripts')
<!-- iCheck 1.0.1 -->
<script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/form/jquery.form.min.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/iCheck/icheck.min.js') }}"></script>
<script type='text/javascript'>
    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    //iCheck for checkbox and radio inputs
    $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
        checkboxClass: 'icheckbox_minimal-blue',
        radioClass: 'iradio_minimal-blue'
    });

    $("#gerarBoleto").click(function() {
        $('#gerarBoleto').html('Por favor aguarde...');
        $.ajax({
            'url': "?novopagamento=1",
            dataType: 'html',
            'success': function(txt) {
                if (txt == '') {
                    alert('Houve uma falha ao gerar o boleto');
                } else {
                    $('#gerarBoleto').removeClass('btn-primary');
                    $('#gerarBoleto').addClass('btn-success')
                    $('#gerarBoleto').html('Redirecionando...');
                    setTimeout(function() {
                        location.href = txt;
                    }, 2000);
                }

            }
        });

    });
    /**/
</script>

@endsection