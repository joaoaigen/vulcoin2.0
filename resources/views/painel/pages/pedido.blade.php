@extends('layouts.app')

@section('htmlheader_title')
Pedido número {{$pedido['id']}}
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Loja Virtual > Meus pedidos > Pedido número {{$pedido['id']}}
@endsection

@section('contentheader_description')

@endsection

@section('main-content')

<div class="container-fluid">

    <div class="box">
        <div class="box-body">
            <div class="container-fluid">
                <div class="row">

                    <div id="mensagemAdicionarVouchers">

                    </div>

                    <div class="col-md-5">
                        <h4>Endereco de entrega</h4>
                        <div class="row">
                            <div class="box box-info">
                                <div class="box-body">
                                    <p>{{$endereco['pais']}}</p>
                                    <p>{{$endereco['endereco']}}</p>
                                    <p>CEP {{$endereco['cep']}} - {{$endereco['cidade']}}, {{$endereco['estado']}}</p>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="col-md-offset-1 col-md-6">

                        <table class="table table-bordered">
                            <tr>
                                <td>
                                    ID do pedido
                                </td>
                                <td>
                                    {{$pedido['id']}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Status
                                </td>
                                <td>
                                    {{$pedido['status']}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Preco + frete
                                </td>
                                <td>
                                    $ {{number_format($pedido['preco'],2,'.', '')}}
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    Data
                                </td>
                                <td>
                                    {{Carbon\Carbon::parse($pedido['date'])->format('d/m/Y')}}
                                </td>
                            </tr>
                        </table>

                    </div>
                    <?php
                    if ($pedido['status'] <> 'Pago') {
                        $link = \App\Pagamentos::where('tipo', 'Compra')->where('reference',$pedido['id_pag'])->first()['paymentLink'];
                        echo '<a  id="paymentBtn" data-href="' . $link . '"><button   class="btn btn-lg btn-primary">Pagar Via PagSeguro</button></a>';
                    }
                    ?>
                    <!-- FINAL FORMULARIO BOTAO PAGSEGURO -->
                    <table id="ord-carrinho" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th> </th>
                                <th>Produto</th>
                                <th>Preço</th>
                                <th>Quantidade</th>
                                <th>Total</th>
                                <th> </th>
                            </tr>
                        </thead>
                        <?php
                        $produtos = json_decode($pedido['produtos'])
                        ?>
                        <tbody>

                            @foreach($produtos as $produto)
                            <tr>
                                <td style="
                                background-image: url('/{{$produto->img}}');
                                background-position: center;
                                background-size: cover;
                                height: 77px;">
                            </td>
                            <td>{{$produto->nome}}</td>
                            <td> <?php if (Auth::user()->apto_bonus(Auth::user()->id) and Auth::user()->meu_desconto($produto->product_id)) { ?>
                                $ {{number_format(Auth::user()->meu_desconto($produto->product_id),2,'.', '')}}
                                <?php } else { ?>$ {{number_format($produto->preco,2,'.', '')}}
                                <?php } ?></td>
                                <td>{{$produto->quantidade}}</td>
                                <td><?php if (Auth::user()->apto_bonus(Auth::user()->id) and Auth::user()->meu_desconto($produto->product_id)) { ?>
                                    $ {{number_format(Auth::user()->meu_desconto($produto->product_id)*$produto->quantidade, 2,'.', '')}}
                                    <?php } else { ?> $ {{number_format($produto->preco*$produto->quantidade,2,'.', '')}}
                                    <?php }?>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>



                </div>
            </div>
        </div>


    </div>

</div>

@endsection

@section('page_scripts')
<script>
    function exibirPagamento(link){
        link = link.split("code=");

        console.log(link);
        PagSeguroLightbox({
            code: link[1]
        }, {
            success: function () {

                location.href = "<?= url('?payment_return=1') ?>";

            },
            abort: function () {
             location.href = "<?= url('?payment_return=0') ?>";


         }
     });
    }


    $(document).ready(function(){
    $("#paymentBtn").click(function(){
        link=$(this).attr('data-href');
        exibirPagamento(link);
        return false;
    });
    });
</script>
@if(env('PAGSEGURO_SANDBOX'))
<script type="text/javascript" src="https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
@else
<script type="text/javascript" src="https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.lightbox.js"></script>
@endif

@endsection
