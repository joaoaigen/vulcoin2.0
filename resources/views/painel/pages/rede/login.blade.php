@extends('layouts.auth')

@section('htmlheader_title')
Login
@endsection

@section('content')
<body class="hold-transition login-page">
    <div class="login-box">
        

        @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            <?= Lang::trans('site.login_error') ?> <br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
		
			<div class="login-logo">
				<a href="{{ url('/') }}">
					<img src="{{url('/img/logo-login.png')}}">
				</a>
			</div>
        <div class="login-box-body" style="border-radius:8px;">
            <form action="{{ url('/painel/login') }}" method="post">
                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="<?= Lang::trans('site.login_user') ?>" name="username"/>
                    <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" placeholder="<?= Lang::trans('site.login_password') ?>" name="password"/>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat"><?= Lang::trans('site.acess_painel') ?></button>
                    </div><!-- /.col -->
                </div>
            </form>
            <br>
			<div align="center">
				<div class="dropdown">
					<button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
						<?= Lang::trans('site.languages') ?>
						<span class="caret"></span>
					</button>
					<ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
						<?php

						function lang_name($lang) {
							if ($lang == 'pt-br') {
								$name = 'Português';
							}if ($lang == 'es') {
								$name = 'Español';
							} elseif ($lang == 'en') {
								$name = 'English';
							}
							return $name;
						}
						?>
						@foreach(config('app.languages') as $lang)
						<?php
						if ($lang == 'pt-br') {
							$flag = url('img/flags/pt-br.jpg');
						} elseif ($lang == 'es') {
							$flag = url('img/flags/es.png');
						} else {
							$flag = url('img/flags/en.png');
						}
						?>

						<li class="{{config('app.locale')  == $lang ? 'active' : '' }}">
							<a href="/language?lang={{$lang}}"><img src='<?= $flag ?>' width='15'> {{lang_name($lang)}}</a>
						</li>
						@endforeach
					</ul>
				</div>
				<br>
				<a href="{{ url('/painel/password/reset') }}"><?= Lang::trans('site.forg_password') ?></a><br>
			</div>

        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

    @include('layouts.partials.scripts_auth')

    <script>
        $(function () {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-72220910-1', 'auto');
        ga('send', 'pageview');

    </script>
</body>

@endsection
