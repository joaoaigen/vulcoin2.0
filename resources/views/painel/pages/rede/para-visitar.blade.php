@extends('layouts.app')

@section('htmlheader_title')
Tarefas 
@endsection

@section('page_css')
<!-- DataTables -->
<link rel="stylesheet" href="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.css')}}">
@endsection

@section('contentheader_title')
Tarefas 

@endsection

@section('contentheader_description')

@endsection

@section('main-content')
<!-- Small boxes (Stat box) -->
<div class="row">

</div><!-- /.row -->

<div class="row">

    <section class="col-md-12">

        <div class="box box-warning">
            <div class="box-header with-border">
                <h3 class="box-title ">Tarefas

                </h3>

            </div><!-- /.box-header -->
            <div class="box-body">
                <div id="mensagemAjax">

                </div>
                <div id="mensagemAdicionarVouchers">

                </div>
                <?php
                $status = @$_GET['status'];
                if ($status == '') {
                    $status = 1;
                }
                $anuncios = \App\anuncios::where('status', $status)->where('user_id', Auth::user()->id)->get();

                function getStatus($status) {
                    if ($status == 1) {
                        return "<a   class='btn btn-sm btn-success'><i class='fa fa-check'></i>Publicado</a>";
                    }if ($status == 2) {
                        return "<a  class='btn btn-sm btn-warning'><i class='fa fa-clock-o'></i>Pendente</a>";
                    }
                    if ($status == 3) {
                        return "<a   class='btn btn-sm btn-danger'><i class='fa fa-close'></i>Recusado</a>";
                    }
                }

                function getAcao($status) {
                    if ($status == 1) {
                        $res = "<a  class='btn btn-sm btn-info'><i class='fa fa-external-link'>Visualização</i></a>";
                        return $res;
                    }if ($status == 2) {
                        $res = "<a   class='btn btn-sm btn-info'><i class='fa fa-facebook'></i>Compartilhamento</a>";
                        return $res;
                    }
                }

                $usr = new App\User();
                ?>
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Título</th>
                            <th>O que eu preciso fazer?</th>
                            <th>Data de vencimento</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $tarefas = DB::table('tarefas')->where('user_id', Auth::user()->id)->where('status', 0)->get();
                        ?>
                        @foreach($tarefas as $tarefa)
                        <?php
                        $anuncio = App\anuncios::where('id', $tarefa->anuncio_id)->first();
                        if ($anuncio['status'] == 1) {
                            ?>
                            <tr>
                                <td>{{$tarefa->id}}</td>
                                <td>
                                    {{$anuncio['nome']}}
                                </td>

                                <td><?php
                                    if ($anuncio['acao'] == 1) {
                                        $link = url('/painel/anuncio/visualizar/' . $tarefa->id);
                                        $res = "<a  target='_blank' href='{$link}' class='btn btn-sm btn-info'><i class='fa fa-external-link'>Visitar site</i></a>";
                                    }if ($anuncio['acao'] == 2) {
                                        $url = base64_encode($anuncio['url']);
                                        $res = "<a   class='btn btn-sm btn-info compartilhar' data-link='{$url}' data-id='{$tarefa->id}'><i class='fa fa-facebook'></i>Compartilhar link  no facebook</a>";
                                    }
                                    echo $res;
                                    ?></td>
                                <td>{{$tarefa->data}}</td>


                            </tr>

                        <?php } ?>
                        @endforeach
                    </tbody>
                </table>

            </div><!-- /.box-body -->
        </div><!-- /.box -->

    </section>

</div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">

            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" data-backdrop="static" 
     data-keyboard="false" id="modalValidar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="modalValidarTitle">Para contabilizar essa visualização é necessário acertar o desafio</h4>
            </div>
            <div class="modal-body" id='modalValidarBody'>
                <form id='enviarView' action="{{url('/painel')}}/validar_visualizacao2">
                    <input type="hidden" name='id' value="" id="idTarefa" required="">
                    <input type="hidden" name='link' value="" id="link" required="">

                    <center><div class='g-recaptcha'  data-sitekey='6LdPNRsTAAAAAAPrpcPLZmHFzAwKOv_kMpV0ivd2'></div></center>
            </div>
            <div id="mensagem_anuncio"></div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">Enviar</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('page_scripts')
<!-- DataTables -->
<script src="{{ env('CFURL').('/plugins/datatables/jquery.dataTables.min.js')}}"></script>
<script src="{{ env('CFURL').('/plugins/datatables/dataTables.bootstrap.min.js')}}"></script>
<script src="{{ asset('/plugins/form/jquery.form.min.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/blockUi/jquery.blockUI.js') }}"></script>

<script>

$(function () {

    function fBlockUi() {
        $.blockUI({
            message: "<h4>Por favor aguarde...</h4>",
            css: {
                border: 'none',
                padding: '5px',
                backgroundColor: '#000',
                '-webkit-border-radius': '5px',
                '-moz-border-radius': '5px',
                opacity: .5,
                color: '#fff'
            }
        });
    }

    $(".delete").click(function () {
        fBlockUi();
        $.ajax({
            url: '{{url(' / admin / pacote')}}' + '/' + $(this).attr('data-id'),
            method: 'DELETE',
            data: {_token: "<?php echo csrf_token(); ?>"},
            type: 'DELETE',
            success: function (result) {
                $("#mensagemAjax").html(result);
                $.unblockUI();
                window.setTimeout('location.reload()', 1500); //Reloads after three seconds
            }
        });
    });
    $(".openModal").on('click', function () {

        $('#myModal').removeData('bs.modal');
        $('#myModal').modal(
                {
                    remote: $(this).attr('data-href')
                }
        );
        $('#myModal').modal('show');
        $('#myModal').on('loaded.bs.modal', function (e) {

// bind form using ajaxForm
            var form = $('.formAjax').ajaxForm({
// target identifies the element(s) to update with the server response
                target: '#mensagemAjax',
                beforeSubmit: function () {
                    fBlockUi();
                    $('#myModal').modal('hide');
                    $('input').attr('disabled', true);
                },
                // success identifies the function to invoke when the server response
                // has been received; here we apply a fade-in effect to the new content
                success: function () {
                    $('.formAjax').clearForm();
                    $('#mensagemAjax').fadeIn('slow');
                    $.unblockUI();
                    $('input').attr('disabled', false);
                    window.setTimeout('location.reload()', 1500); //Reloads after three seconds
                }
            });
        });
    });
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "language": {
            "sEmptyTable": "Nenhum registro encontrado",
            "sInfo": "Mostrando de _START_ até _END_ de _TOTAL_ registros",
            "sInfoEmpty": "Mostrando 0 até 0 de 0 registros",
            "sInfoFiltered": "(Filtrados de _MAX_ registros)",
            "sInfoPostFix": "",
            "sInfoThousands": ".",
            "sLengthMenu": "_MENU_ resultados por página",
            "sLoadingRecords": "Carregando...",
            "sProcessing": "Processando...",
            "sZeroRecords": "Nenhum registro encontrado",
            "sSearch": "Pesquisar",
            "oPaginate": {
                "sNext": "Próximo",
                "sPrevious": "Anterior",
                "sFirst": "Primeiro",
                "sLast": "Último"
            },
            "oAria": {
                "sSortAscending": ": Ordenar colunas de forma ascendente",
                "sSortDescending": ": Ordenar colunas de forma descendente"
            }
        }
    });
});
window.fbAsyncInit = function () {
    FB.init({
        appId: '1678567435738320',
        xfbml: true,
        version: 'v2.5'
    });
};

(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {
        return;
    }
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
$(".compartilhar").click(function () {
    link = $(this).attr('data-link');
    link2 = Base64.decode($(this).attr('data-link'));

    id = $(this).attr('data-id');

    FB.ui({
        method: 'share',
        href: link2,
    }, function (response) {
        if (response && !response.error_code) {
            $("#idTarefa").val(id);
            $("#link").val(link);
            $("#modalValidar").modal();
        } else {
            alert('Post was not published.');
        }


    });


});
$("#valorDist2").remove();

$("#enviarView").submit(function () {
    enviarForm($(this));
    return false;
});
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>

@endsection
