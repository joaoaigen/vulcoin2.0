@extends('layouts.app')
@section('title-head')
    Extrato Financeiro
@endsection
@section('title-body')
    Extrato Financeiro
@endsection
@section('page-css')

@endsection
@section('main-content')
    @inject('extratos', 'App\extratos')

    <?php
    if (!isset($_GET['de']) or ! isset($_GET['ate'])) {
        $extract = $extratos->where('beneficiado', Auth::user()->id)->where('descricao', '<>', 'Pontos Binários');
        $allExtratos = $extract->orderBy('id', 'asc')->get();
    } else {
        $extract = $extratos->where('beneficiado', Auth::user()->id)->where('descricao', '<>', 'Pontos Binários')->where('data', '>=', $_GET['de'])->where('data', '<=', $_GET['ate']);
        $allExtratos = $extract->get();
    }
    ?>
    <section class="content">
        <div class="row">
            <div class="col-12">
                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Pesquisa por data</h3>
                        <h6 class="box-subtitle">Selecione as datas abaixo para personalizar a sua pesquisa.</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <form class="form-inline" role="form">
                            <div class="form-group">
                                <label for="de">De: </label>&emsp;
                                <input type="date" required="" class="form-control" id="de" value="<?= @$_GET['de'] ?>" name="de">
                            </div>
                            <div class="form-group">
                                <label for="de">Até: </label>&emsp;
                                <input type="date" required="" class="form-control" value="<?= @$_GET['ate'] ?>" id="ate" name="ate">
                            </div>
                            <button type="submit" class="btn btn-primary">Exibir</button>
                        </form>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-12">

                <div class="box box-solid bg-dark">
                    <div class="box-header with-border">
                        <h3 class="box-title">Extrato financeiro</h3>
                        <h6 class="box-subtitle">Listagem de todas as movimentações do usuário: {{ Auth::user()->username }}</h6>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="table-responsive">
                            <table id="extratos" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                                <thead>
                                <tr>
                                    <th class="sorting_desc">Id</th>
                                    <th>Usuario</th>
                                    <th>Descrição</th>
                                    <th>Data</th>
                                    <th>Valor</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($allExtratos as $extrato)
                                    <tr>
                                        <td>{{$extrato['id']}}</td>
                                        <td>@if($extrato['descricao']=='Pagamento de binário')
                                                {{'Administração'}}
                                            @endif
                                            @if($extrato['descricao']!='Pagamento de binário')
                                                {{$extrato->userName($extrato['user_id'])}}
                                            @endif
                                        </td>
                                        <td><?= $extrato['descricao']?></td>
                                        <td>{{ date( 'd/m/Y' , strtotime($extrato['data']))}}</td>

                                        <td style="color: {{ substr($extrato['valor'], 0, 1) == '-' ? 'red' : 'green' }};">{{ strpos($extrato['valor'], '-') ? '$ ' . number_format(str_replace('-', '', $extrato['valor']), 2, '.', '') : '$ ' . number_format(str_replace('-', '', $extrato['valor']), 2, '.', '') }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->

            </div>
            <!-- /.col -->
        </div>
        <!-- /.row -->
    </section>

@endsection
@section('page-js')

    <!-- This is data table -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
    <!-- start - This is for export functionality only -->
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>    
    <script type="text/javascript">
    $('#extratos').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
        "order": [[ 0, "desc" ]]
    });
    </script>    
@endsection
