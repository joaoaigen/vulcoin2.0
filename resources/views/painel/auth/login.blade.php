<?php
$curl = curl_init();
curl_setopt_array($curl, [
    CURLOPT_RETURNTRANSFER => 1,
    CURLOPT_URL => "http://ip-api.com/json/".Request::ip()
]);

$result = json_decode(curl_exec($curl));
curl_close($curl);

if (isset($result->countryCode) && strtolower($result->countryCode) == 'br'){
    Lang::setLocale('pt-BR');
}

if (isset($result->countryCode) && strtolower($result->countryCode) == 'us'){
    Lang::setLocale('en');
}

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="{{ asset('img/favicon.png') }}">

    <title>WorldCrypto - @lang('messages.entrar') </title>

    <!-- Bootstrap 4.0-->
    <link rel="stylesheet" href="{{ asset('../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">

    <!-- Bootstrap extend-->
    <link rel="stylesheet" href="{{ asset('../assets/css/bootstrap-extend.css') }}">

    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('../assets/css/master_style.css') }}">

    <!-- Crypto_Admin skins -->
    <link rel="stylesheet" href="{{ asset('../assets/css/skins/_all-skins.css') }}">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="{{ url('painel/login') }}"><b>WorldCrypto</b> BackOffice</a>
    </div>
    <!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">@lang('messages.login-boas-vindas')</p>

         @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Houve alguns problemas com a sua entrada.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error == 'The g-recaptcha-response field is required.' ? 'É preciso verificar o Captcha!' : $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <form action="{{ url('painel/login') }}" method="post" class="form-element">
            @csrf
            <div class="form-group has-feedback">
                <input type="text" class="form-control" name="username" placeholder="@lang('messages.usuario')">
                <span class="ion ion-email form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" class="form-control" name="password" placeholder="@lang('messages.senha')">
                <span class="ion ion-locked form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="checkbox">
                        <input type="checkbox" id="basic_checkbox_1" >
                        <label for="basic_checkbox_1">@lang('messages.lembre-se')</label>
                    </div>
                </div>
                <!-- /.col -->
                <div class="col-6">
                    <div class="fog-pwd">
                        <a href="{{ url('/painel/password/reset') }}"><i class="ion ion-locked"></i> @lang('messages.perdeu-a-senha')</a><br>
                    </div>   
                </div>
                <!-- /.col -->
                
                <div class="col-12 text-center d-flex justify-content-center">
                   {!! Recaptcha::render() !!}
                </div>
                
                <div class="col-12 text-center ">
                    <button type="submit" class="btn btn-info btn-block margin-top-10">@lang('messages.entrar')</button>
                </div>
                <!-- /.col -->
            </div>
        </form>

        {{--<div class="social-auth-links text-center">
            <p>- @lang('messages.ou') -</p>
            <a href="#" class="btn btn-social-icon btn-circle btn-facebook"><i class="fa fa-facebook"></i></a>
            <a href="#" class="btn btn-social-icon btn-circle btn-google"><i class="fa fa-google-plus"></i></a>
        </div>--}}
        <!-- /.social-auth-links -->

    </div>
    <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


<!-- jQuery 3 -->
<script src="{{ asset('../assets/assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>

<!-- popper -->
<script src="{{ asset('../assets/assets/vendor_components/popper/dist/popper.min.js') }}"></script>

<!-- Bootstrap 4.0-->
<script src="{{ asset('../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>
