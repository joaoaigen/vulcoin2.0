<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../images/favicon.ico">

    <title>WorldCrypto - Autenticação</title>
  
	<!-- Bootstrap 4.0-->
	<link rel="stylesheet" href="{{ asset('../assets/assets/vendor_components/bootstrap/dist/css/bootstrap.min.css') }}">
	
	<!-- Bootstrap extend-->
	<link rel="stylesheet" href="{{ asset('../assets/css/bootstrap-extend.css') }}">

	<!-- Theme style -->
	<link rel="stylesheet" href="{{ asset('../assets/css/master_style.css') }}">

	<!-- Crypto_Admin skins -->
	<link rel="stylesheet" href="{{ asset('../assets/css/skins/_all-skins.css') }}">	

	<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
	<!--[if lt IE 9]>
	<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
	<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="../../index.html"><b>WorldCrypto</b>BackOffice</a>
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body pb-20">
      <p class="login-box-msg text-uppercase">Autenticação em dois fatores, caso não possua <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=pt_BR">clique aqui</a> para baixar.</p>
    
    <form action="/google2fa/authenticate" method="post" class="form-element">
        @csrf
      <div class="form-group has-feedback">
        <input name="code" type="text" class="form-control" placeholder="Código">
        <span class="ion ion-email form-control-feedback"></span>
      </div>  
        
      <div class="row">
          <div class="col-12 d-flex justify-content-center">
              <img src="{{ isset($QR_Image) ? $QR_Image : '' }}"/>
          </div>
          <hr>
        <!-- /.col -->
        <div class="col-12 text-center"><br>
          <button type="submit" class="btn btn-info btn-block text-uppercase">Autenticar</button>          
        </div>
        <!-- /.col -->
      </div>
    </form>
  </div>
  <!-- /.login-box-body -->
</div>
<!-- /.login-box -->


	<!-- jQuery 3 -->
	<script src="{{ asset('../assets/assets/vendor_components/jquery/dist/jquery.min.js') }}"></script>
	
	<!-- popper -->
	<script src="{{ asset('../assets/assets/vendor_components/popper/dist/popper.min.js') }}"></script>
	
	<!-- Bootstrap 4.0-->
	<script src="{{ asset('../assets/assets/vendor_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

</body>
</html>
