@extends('layouts.auth')

@section('htmlheader_title')
Login
@endsection

@section('content')
<style type="text/css">
    .goog-te-banner-frame.skiptranslate{display:none!important;}
    body{top:0px!important;}
</style>
<style>

body{
  overflow: hidden !important;
  }
  
</style>
<body class="hold-transition login-page" style="background: url('{{url('img/2.jpg')}}'); background-size: cover;">
    <div class="login-box">

        @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong> Houve alguns problemas com a sua entrada.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="login-box-body" style="border-radius:5px;">
        	        <div class="login-logo">
		            <a href="{{ url('/') }}">
		                <img src="{{url('/img/logo-login.png')}}" style="max-width:100%">
		            </a>
		        </div><!-- /.login-logo -->
		        <br>
            <form id="confirmar_codigo" action="{{ url('/painel/login') }}" method="post">
                <input type="hidden" id="token" name="_token" value="{{ csrf_token() }}">
                <input type="hidden" id="username" name="username" value="{{ $username }}">
                <input type="hidden" id="password" name="password" value="{{ $password }}">
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" placeholder="Código de verificação" name="codigo_login"/>
                    <span class="glyphicon glyphicon-barcode form-control-feedback"></span>
                </div>
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Verificar código</button>
                    </div><!-- /.col -->
                </div>
            </form>
            
            
            <div align="center"><br>
				<button class="btn btn-default" type="button" onclick="reenviarCodigo()">Reenviar código</button><br>
			</div>
            
            <br>
            
            <div align="center" class="row">

                 <a href="/language?lang=pt" class="item-lang"><img src='/img/flags/pt-br.png' width='40'></a>
            <a href="/language?lang=en" class="item-lang"><img src='/img/flags/en-us.png' width='40'></a>
            <a href="/language?lang=es" class="item-lang"><img src='/img/flags/es-es.png' width='40'></a>
            <a href="/language?lang=it" class="item-lang"><img src='/img/flags/pt-it.png' width='40'></a>
            </div>
			
        </div><!-- /.login-box-body -->

    </div><!-- /.login-box -->

    @include('layouts.partials.scripts_auth')

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script>
        function reenviarCodigo(){
            $.post('/painel/login/confirmar-codigo', {
                username: $('#username').val(),
                password: $('#password').val(),
                _token: $('#token').val(),
                ajax: 1
            }, function(msg){
                swal("Sucesso!", 'Código reenviado com sucesso!', 'success');  
            });
        }
        $(function () {
            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
        });
    </script>
    <script>
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-72220910-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script src="/js/jquery.cookie.js"></script>	
    <script type="text/javascript">
		$.cookie('googtrans', '/pt/' + $.cookie('lang'), { expires: 7 });
	</script>
		
    <div id="google_translate_element" style="display: none;"></div>
    <script type="text/javascript">
        function googleTranslateElementInit() {
          new google.translate.TranslateElement({pageLanguage: 'pt', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
        }
    </script>
    <script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
</body>

@endsection
