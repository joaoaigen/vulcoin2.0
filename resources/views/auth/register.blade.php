<?php ?>
@extends('layouts.auth')

@section('htmlheader_title')
Cadastre-se
@endsection
<style>
    @media (min-width:320px) { 

    }
    @media (min-width:480px) {

    }
    @media (min-width:600px) { 


    }
    @media (min-width:801px) { 
        .register-box{ width: 650px !important;}

    }
    @media (min-width:1200px) { 
        .register-box{ width: 650px !important;}
    }
</style>
@section('content')
<link type="text/css" rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css"/>

<body class="hold-transition register-page" style="background-image: url('https://stillmed.olympic.org/media/Images/OlympicOrg/IOC/What_We_Do/Promote_Olympism/Women_In_Sport/Background/Women-in-Sport/Background-in-Leadership.jpg'); background-size: cover;">
    <div class="register-box" >

        @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger">
            <strong>Whoops!</strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
        <?php if (@$_GET['huebr'] == 666) { ?>

            <div class="register-box-body">
				<br>
				        <div class="register-logo">
							<a href="{{ url('/') }}">
								<img src="{{url('/img/logo-login.png')}}">
							</a>
						</div>
				<br>
                <?php if (isset($indicador->id)) { ?>
                    <p class="login-box-msg">Você está sendo indicado por:  <b>{{$indicador->name. ' - '. $indicador->email}}</b></p>
                <?php } ?>
                <form action="{{ url('/cadastro/') }}" method="post" id="formcadastro">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <input type="hidden" class="form-control" id='cadastro_exterior' name="cadastro_exterior" value="{{old('cadastro_exterior')}}"/>

                    <div class="col-sm-6">
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="ID do patrocinador" value=admin maxlength="20" name="indicador" pattern="[a-zA-Z0-9]+" value="{{old('indicador')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Seu Login" maxlength="20" name="username" pattern="[a-zA-Z0-9]+" value="teste<?=rand(0,9999999)?>"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control" required placeholder="Nome completo" name="name" value="Teste testando <?=rand(0,9999999)?>"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control data" placeholder="Data de Nascimento" name="nascimento" value="27-07-1996" required=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cpf" required placeholder="CPF" name="cpf" value="075.596.595-78{{old('cpf')}}"/>
                        </div>


                        <div class="form-group has-feedback">
                            <input type="email" class="form-control" required placeholder="Email" name="email" value="teste_<?=rand(0,9999999)?>@gmail.com"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="Senha" name="password" value="123456"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="password" class="form-control" required placeholder="Repita sua senha" value="123456" name="password_confirmation" value=""/>
                        </div>

                        <div class="form-group has-feedback">
                            <select name="sexo" class="form-control">
                                <option value="Masculino" selected="">
                                    Masculino
                                </option>
                                <option value="Feminino">
                                    Feminino

                                </option>

                            </select>
                        </div>
                    </div>
                    <div class="col-sm-6">




                        <div class="form-group has-feedback">
                            <input type="text" class="form-control telefone" placeholder="Telefone" name="telefone" value="(75) 981495203"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control cep" placeholder="CEP" name="cep" value="44010125"/>
                        </div>
                        <div class="form-group has-feedback">
                            <div class="form-group">
                                <select  data-required="true" class="form-control m-t" name="pais" id="pais_fm" required>
                                    <option value=""><?= Lang::trans('site.select-an-option') ?></option>
                                    <option value="Afganistan">Afghanistan</option>
                                    <option value="Albania">Albania</option>
                                    <option value="Algeria">Algeria</option>
                                    <option value="American Samoa">American Samoa</option>
                                    <option value="Andorra">Andorra</option>
                                    <option value="Angola">Angola</option>
                                    <option value="Anguilla">Anguilla</option>
                                    <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                    <option value="Argentina">Argentina</option>
                                    <option value="Armenia">Armenia</option>
                                    <option value="Aruba">Aruba</option>
                                    <option value="Australia">Australia</option>
                                    <option value="Austria">Austria</option>
                                    <option value="Azerbaijan">Azerbaijan</option>
                                    <option value="Bahamas">Bahamas</option>
                                    <option value="Bahrain">Bahrain</option>
                                    <option value="Bangladesh">Bangladesh</option>
                                    <option value="Barbados">Barbados</option>
                                    <option value="Belarus">Belarus</option>
                                    <option value="Belgium">Belgium</option>
                                    <option value="Belize">Belize</option>
                                    <option value="Benin">Benin</option>
                                    <option value="Bermuda">Bermuda</option>
                                    <option value="Bhutan">Bhutan</option>
                                    <option value="Bolivia">Bolivia</option>
                                    <option value="Bonaire">Bonaire</option>
                                    <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                    <option value="Botswana">Botswana</option>
                                    <option value="Brasil" selected="">Brasil</option>
                                    <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                    <option value="Brunei">Brunei</option>
                                    <option value="Bulgaria">Bulgaria</option>
                                    <option value="Burkina Faso">Burkina Faso</option>
                                    <option value="Burundi">Burundi</option>
                                    <option value="Cambodia">Cambodia</option>
                                    <option value="Cameroon">Cameroon</option>
                                    <option value="Canada">Canada</option>
                                    <option value="Canary Islands">Canary Islands</option>
                                    <option value="Cape Verde">Cape Verde</option>
                                    <option value="Cayman Islands">Cayman Islands</option>
                                    <option value="Central African Republic">Central African Republic</option>
                                    <option value="Chad">Chad</option>
                                    <option value="Channel Islands">Channel Islands</option>
                                    <option value="Chile">Chile</option>
                                    <option value="China">China</option>
                                    <option value="Christmas Island">Christmas Island</option>
                                    <option value="Cocos Island">Cocos Island</option>
                                    <option value="Colombia">Colombia</option>
                                    <option value="Comoros">Comoros</option>
                                    <option value="Congo">Congo</option>
                                    <option value="Cook Islands">Cook Islands</option>
                                    <option value="Costa Rica">Costa Rica</option>
                                    <option value="Cote DIvoire">Cote D'Ivoire</option>
                                    <option value="Croatia">Croatia</option>
                                    <option value="Cuba">Cuba</option>
                                    <option value="Curaco">Curacao</option>
                                    <option value="Cyprus">Cyprus</option>
                                    <option value="Czech Republic">Czech Republic</option>
                                    <option value="Denmark">Denmark</option>
                                    <option value="Djibouti">Djibouti</option>
                                    <option value="Dominica">Dominica</option>
                                    <option value="Dominican Republic">Dominican Republic</option>
                                    <option value="East Timor">East Timor</option>
                                    <option value="Ecuador">Ecuador</option>
                                    <option value="Egypt">Egypt</option>
                                    <option value="El Salvador">El Salvador</option>
                                    <option value="Equatorial Guinea">Equatorial Guinea</option>
                                    <option value="Eritrea">Eritrea</option>
                                    <option value="Estonia">Estonia</option>
                                    <option value="Ethiopia">Ethiopia</option>
                                    <option value="Falkland Islands">Falkland Islands</option>
                                    <option value="Faroe Islands">Faroe Islands</option>
                                    <option value="Fiji">Fiji</option>
                                    <option value="Finland">Finland</option>
                                    <option value="France">France</option>
                                    <option value="French Guiana">French Guiana</option>
                                    <option value="French Polynesia">French Polynesia</option>
                                    <option value="French Southern Ter">French Southern Ter</option>
                                    <option value="Gabon">Gabon</option>
                                    <option value="Gambia">Gambia</option>
                                    <option value="Georgia">Georgia</option>
                                    <option value="Germany">Germany</option>
                                    <option value="Ghana">Ghana</option>

                                </select>
                            </div>
                        </div>

                        <div class="form-group has-feedback estado_select">
                            <?php
                            $estados = DB::table('estado')->get();
                            ?>
                            <select name="estado" class="estados form-control">
                                <?php
                                $estados_html = '';
                                $estados_html.='<select name="estado" onchange="carregar_cidades()" class="estados form-control">';
                                foreach ($estados as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                    $estados_html .= '<option value="' . $value->id . '">' . $value->nome . '</option>';
                                }
                                $estados_html.='</select><p id="descCep" data-cep="0"></p>';
                                ?>
                            </select>
                            <p id="descCep" data-cep='0'></p>
                        </div>
                        <div class="form-group has-feedback cidade_select" disabled="" title="<?= Lang::trans('site.select-an-option') ?>">

                            <select name="cidade"  class="cidades form-control" >
                                <?php
                                foreach (DB::table('cidade')->where('estado', 1)->get() as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                }
                                ?>
                                <?php
                                $cidades_html = '';
                                $cidades_html.='<select name="cidade" class="cidades form-control">';
                                foreach (DB::table('cidade')->where('estado', 1)->get() as $value) {
                                    echo "<option value='$value->id'> $value->nome</option>";
                                    $cidades_html .= '<option value="' . $value->id . '">' . $value->nome . '</option>';
                                }
                                $cidades_html.='</select>';
                                ?>
                            </select>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="text" class="form-control endereco" placeholder="Endereço" name="endereco" value="teste testeiro testando{{old('endereco')}}"/>
                        </div>


                        <div class="form-group has-feedback">
                            <input type="text" class="form-control bairro" placeholder="Bairro"  name="bairro" required="" value="teste{{old('bairro')}}"/>
                        </div>

                        <div class="form-group has-feedback">
                            <input type="text" class="form-control complemento" placeholder="Complemento" name="complemento" required="" value="sssdd{{old('complemento')}}"/>
                        </div>
                        <div class="form-group has-feedback">
                            <input type="number" class="form-control n_complemento" placeholder="Número do Complemento" required="" name="n_complemento" value="22{{old('n_complemento')}}"/>
                        </div>


                        <div class="form-group has-feedback" >
                            <select name="pacote" class=" pacote form-control">

                                @foreach($pacotes as $pacote)
                                @if ($pacote->status==1)
                                <option value="{{$pacote->id}}">
                                    {{$pacote->nome}} - R$ {{$pacote->valor}}
                                </option>
                                @endif
                                @endforeach
                            </select>
                        </div>
                    </div>


                    <div class="row">
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block btn-flat">Finalizar Cadastro</button>
                        </div><!-- /.col -->
                    </div>

                </form>
            </div><!-- /.form-box -->
        </div><!-- /.form-box -->
    <?php } else { ?>

        <div class="register-box-body">
			<br>
						<div class="register-logo">
							<a href="{{ url('/') }}">
								<img src="{{url('/img/logo-login.png')}}" style="max-width: 80%">
							</a>
						</div>
            <?php if (isset($indicador->id)) { ?>
                <p class="login-box-msg">Você foi indicado por:  <b>{{$indicador->name. ' - '. $indicador->email}}</b></p>
            <?php } ?>
            <form action="{{ url('/cadastro/') }}" method="post" id="formcadastro">
                {{ csrf_field() }}
                {{ method_field('POST') }}
                <input type="hidden" class="form-control" id='cadastro_exterior' name="cadastro_exterior" value="{{old('cadastro_exterior')}}"/>

                <div class="col-sm-6">
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required placeholder="ID do patrocinador" value="<?= @$indicador->username ?>" maxlength="20" name="indicador" pattern="[a-zA-Z0-9]+" value="{{old('indicador')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required placeholder="Seu Login" maxlength="20" name="username" pattern="[a-zA-Z0-9]+" value="{{old('username')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control" required placeholder="Nome completo" name="name" value="{{old('name')}}"/>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control data" placeholder="Data de Nascimento" name="nascimento" value="{{old('nascimento')}}" required=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control cpf" required placeholder="CPF" name="cpf" value="{{old('cpf')}}"/>
                    </div>


                    <div class="form-group has-feedback">
                        <input type="email" class="form-control" required placeholder="Email" name="email" value="{{old('email')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" required placeholder="Senha" name="password" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="password" class="form-control" required placeholder="Repita sua senha" name="password_confirmation" value=""/>
                    </div>

                    <div class="form-group has-feedback">
                        <select name="sexo" class="form-control">
                            <option value="Masculino" selected="">
                                Masculino
                            </option>
                            <option value="Feminino">
                                Feminino

                            </option>

                        </select>
                    </div>
                </div>
                <div class="col-sm-6">




                    <div class="form-group has-feedback">
                        <input type="text" class="form-control telefone" placeholder="Celular com DDD" name="telefone" value="{{old('telefone')}}"/>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control cep" placeholder="CEP" name="cep" value="{{old('cep')}}"/>
                    </div>
                    <div class="form-group has-feedback">
                        <div class="form-group">
                            <select  data-required="true" class="form-control m-t" name="pais" id="pais_fm" required>
                                <option value=""><?= Lang::trans('site.select-an-option') ?></option>
                                <option value="Afganistan">Afghanistan</option>
                                <option value="Albania">Albania</option>
                                <option value="Algeria">Algeria</option>
                                <option value="American Samoa">American Samoa</option>
                                <option value="Andorra">Andorra</option>
                                <option value="Angola">Angola</option>
                                <option value="Anguilla">Anguilla</option>
                                <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                                <option value="Argentina">Argentina</option>
                                <option value="Armenia">Armenia</option>
                                <option value="Aruba">Aruba</option>
                                <option value="Australia">Australia</option>
                                <option value="Austria">Austria</option>
                                <option value="Azerbaijan">Azerbaijan</option>
                                <option value="Bahamas">Bahamas</option>
                                <option value="Bahrain">Bahrain</option>
                                <option value="Bangladesh">Bangladesh</option>
                                <option value="Barbados">Barbados</option>
                                <option value="Belarus">Belarus</option>
                                <option value="Belgium">Belgium</option>
                                <option value="Belize">Belize</option>
                                <option value="Benin">Benin</option>
                                <option value="Bermuda">Bermuda</option>
                                <option value="Bhutan">Bhutan</option>
                                <option value="Bolivia">Bolivia</option>
                                <option value="Bonaire">Bonaire</option>
                                <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                                <option value="Botswana">Botswana</option>
                                <option value="Brasil" selected="">Brasil</option>
                                <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                                <option value="Brunei">Brunei</option>
                                <option value="Bulgaria">Bulgaria</option>
                                <option value="Burkina Faso">Burkina Faso</option>
                                <option value="Burundi">Burundi</option>
                                <option value="Cambodia">Cambodia</option>
                                <option value="Cameroon">Cameroon</option>
                                <option value="Canada">Canada</option>
                                <option value="Canary Islands">Canary Islands</option>
                                <option value="Cape Verde">Cape Verde</option>
                                <option value="Cayman Islands">Cayman Islands</option>
                                <option value="Central African Republic">Central African Republic</option>
                                <option value="Chad">Chad</option>
                                <option value="Channel Islands">Channel Islands</option>
                                <option value="Chile">Chile</option>
                                <option value="China">China</option>
                                <option value="Christmas Island">Christmas Island</option>
                                <option value="Cocos Island">Cocos Island</option>
                                <option value="Colombia">Colombia</option>
                                <option value="Comoros">Comoros</option>
                                <option value="Congo">Congo</option>
                                <option value="Cook Islands">Cook Islands</option>
                                <option value="Costa Rica">Costa Rica</option>
                                <option value="Cote DIvoire">Cote D'Ivoire</option>
                                <option value="Croatia">Croatia</option>
                                <option value="Cuba">Cuba</option>
                                <option value="Curaco">Curacao</option>
                                <option value="Cyprus">Cyprus</option>
                                <option value="Czech Republic">Czech Republic</option>
                                <option value="Denmark">Denmark</option>
                                <option value="Djibouti">Djibouti</option>
                                <option value="Dominica">Dominica</option>
                                <option value="Dominican Republic">Dominican Republic</option>
                                <option value="East Timor">East Timor</option>
                                <option value="Ecuador">Ecuador</option>
                                <option value="Egypt">Egypt</option>
                                <option value="El Salvador">El Salvador</option>
                                <option value="Equatorial Guinea">Equatorial Guinea</option>
                                <option value="Eritrea">Eritrea</option>
                                <option value="Estonia">Estonia</option>
                                <option value="Ethiopia">Ethiopia</option>
                                <option value="Falkland Islands">Falkland Islands</option>
                                <option value="Faroe Islands">Faroe Islands</option>
                                <option value="Fiji">Fiji</option>
                                <option value="Finland">Finland</option>
                                <option value="France">France</option>
                                <option value="French Guiana">French Guiana</option>
                                <option value="French Polynesia">French Polynesia</option>
                                <option value="French Southern Ter">French Southern Ter</option>
                                <option value="Gabon">Gabon</option>
                                <option value="Gambia">Gambia</option>
                                <option value="Georgia">Georgia</option>
                                <option value="Germany">Germany</option>
                                <option value="Ghana">Ghana</option>

                            </select>
                        </div>
                    </div>

                    <div class="form-group has-feedback estado_select">
                        <?php
                        $estados = DB::table('estado')->get();
                        ?>
                        <select name="estado" class="estados form-control">
                            <?php
                            $estados_html = '';
                            $estados_html.='<select name="estado" onchange="carregar_cidades()" class="estados form-control">';
                            foreach ($estados as $value) {
                                echo "<option value='$value->id'> $value->nome</option>";
                                $estados_html .= '<option value="' . $value->id . '">' . $value->nome . '</option>';
                            }
                            $estados_html.='</select><p id="descCep" data-cep="0"></p>';
                            ?>
                        </select>
                        <p id="descCep" data-cep='0'></p>
                    </div>
                    <div class="form-group has-feedback cidade_select" disabled="" title="<?= Lang::trans('site.select-an-option') ?>">

                        <select name="cidade"  class="cidades form-control" >
                            <?php
                            foreach (DB::table('cidade')->where('estado', 1)->get() as $value) {
                                echo "<option value='$value->id'> $value->nome</option>";
                            }
                            ?>
                            <?php
                            $cidades_html = '';
                            $cidades_html.='<select name="cidade" class="cidades form-control">';
                            foreach (DB::table('cidade')->where('estado', 1)->get() as $value) {
                                echo "<option value='$value->id'> $value->nome</option>";
                                $cidades_html .= '<option value="' . $value->id . '">' . $value->nome . '</option>';
                            }
                            $cidades_html.='</select>';
                            ?>
                        </select>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="text" class="form-control endereco" placeholder="Endereço" name="endereco" value="{{old('endereco')}}"/>
                    </div>


                    <div class="form-group has-feedback">
                        <input type="text" class="form-control bairro" placeholder="Bairro"  name="bairro" required="" value="{{old('bairro')}}"/>
                    </div>

                    <div class="form-group has-feedback">
                        <input type="text" class="form-control complemento" placeholder="Complemento" name="complemento" required="" value="{{old('complemento')}}"/>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="number" class="form-control n_complemento" placeholder="Número do Complemento" required="" name="n_complemento" value="{{old('n_complemento')}}"/>
                    </div>


                    <div class="form-group has-feedback" >
                        <select name="pacote" class=" pacote form-control" id="inputError">

                            @foreach($pacotes as $pacote)
                            @if ($pacote->status==1)
                            <option value="{{$pacote->id}}">
                                {{$pacote->nome}} - R$ {{$pacote->valor}}
                            </option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>


                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block btn-flat">Finalizar Cadastro</button>
                    </div><!-- /.col -->
                </div>

            </form>
        </div><!-- /.form-box -->
    <?php } ?>

</div><!-- /.register-box -->
<div  id='modal_pais' class="modal" data-backdrop="static" data-keyboard="false" >
    <div class="modal-dialog">
        <div class="modal-content">
            <p>

            <div class="modal-header">
                <h4 class="modal-title">Selecione o seu país!</h4>
            </div>
            <div class="modal-body">

                <div class="form-group">
                    <select  data-required="true" class="form-control m-t" name="pais" id="pais" required>
                        <option value=""><?= Lang::trans('site.select-an-option') ?></option>
                        <option value="Afganistan">Afghanistan</option>
                        <option value="Albania">Albania</option>
                        <option value="Algeria">Algeria</option>
                        <option value="American Samoa">American Samoa</option>
                        <option value="Andorra">Andorra</option>
                        <option value="Angola">Angola</option>
                        <option value="Anguilla">Anguilla</option>
                        <option value="Antigua &amp; Barbuda">Antigua &amp; Barbuda</option>
                        <option value="Argentina">Argentina</option>
                        <option value="Armenia">Armenia</option>
                        <option value="Aruba">Aruba</option>
                        <option value="Australia">Australia</option>
                        <option value="Austria">Austria</option>
                        <option value="Azerbaijan">Azerbaijan</option>
                        <option value="Bahamas">Bahamas</option>
                        <option value="Bahrain">Bahrain</option>
                        <option value="Bangladesh">Bangladesh</option>
                        <option value="Barbados">Barbados</option>
                        <option value="Belarus">Belarus</option>
                        <option value="Belgium">Belgium</option>
                        <option value="Belize">Belize</option>
                        <option value="Benin">Benin</option>
                        <option value="Bermuda">Bermuda</option>
                        <option value="Bhutan">Bhutan</option>
                        <option value="Bolivia">Bolivia</option>
                        <option value="Bonaire">Bonaire</option>
                        <option value="Bosnia &amp; Herzegovina">Bosnia &amp; Herzegovina</option>
                        <option value="Botswana">Botswana</option>
                        <option value="Brasil" selected="">Brasil</option>
                        <option value="British Indian Ocean Ter">British Indian Ocean Ter</option>
                        <option value="Brunei">Brunei</option>
                        <option value="Bulgaria">Bulgaria</option>
                        <option value="Burkina Faso">Burkina Faso</option>
                        <option value="Burundi">Burundi</option>
                        <option value="Cambodia">Cambodia</option>
                        <option value="Cameroon">Cameroon</option>
                        <option value="Canada">Canada</option>
                        <option value="Canary Islands">Canary Islands</option>
                        <option value="Cape Verde">Cape Verde</option>
                        <option value="Cayman Islands">Cayman Islands</option>
                        <option value="Central African Republic">Central African Republic</option>
                        <option value="Chad">Chad</option>
                        <option value="Channel Islands">Channel Islands</option>
                        <option value="Chile">Chile</option>
                        <option value="China">China</option>
                        <option value="Christmas Island">Christmas Island</option>
                        <option value="Cocos Island">Cocos Island</option>
                        <option value="Colombia">Colombia</option>
                        <option value="Comoros">Comoros</option>
                        <option value="Congo">Congo</option>
                        <option value="Cook Islands">Cook Islands</option>
                        <option value="Costa Rica">Costa Rica</option>
                        <option value="Cote DIvoire">Cote D'Ivoire</option>
                        <option value="Croatia">Croatia</option>
                        <option value="Cuba">Cuba</option>
                        <option value="Curaco">Curacao</option>
                        <option value="Cyprus">Cyprus</option>
                        <option value="Czech Republic">Czech Republic</option>
                        <option value="Denmark">Denmark</option>
                        <option value="Djibouti">Djibouti</option>
                        <option value="Dominica">Dominica</option>
                        <option value="Dominican Republic">Dominican Republic</option>
                        <option value="East Timor">East Timor</option>
                        <option value="Ecuador">Ecuador</option>
                        <option value="Egypt">Egypt</option>
                        <option value="El Salvador">El Salvador</option>
                        <option value="Equatorial Guinea">Equatorial Guinea</option>
                        <option value="Eritrea">Eritrea</option>
                        <option value="Estonia">Estonia</option>
                        <option value="Ethiopia">Ethiopia</option>
                        <option value="Falkland Islands">Falkland Islands</option>
                        <option value="Faroe Islands">Faroe Islands</option>
                        <option value="Fiji">Fiji</option>
                        <option value="Finland">Finland</option>
                        <option value="France">France</option>
                        <option value="French Guiana">French Guiana</option>
                        <option value="French Polynesia">French Polynesia</option>
                        <option value="French Southern Ter">French Southern Ter</option>
                        <option value="Gabon">Gabon</option>
                        <option value="Gambia">Gambia</option>
                        <option value="Georgia">Georgia</option>
                        <option value="Germany">Germany</option>
                        <option value="Ghana">Ghana</option>

                    </select>
                </div>
            </div>

            <div class="modal-footer">

                <button type="submit" id="selected_pais" class="btn btn-primary">Prosseguir com cadastro</button>
                </p>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div>
</div>
@include('layouts.partials.scripts_auth')

<script>
    function mascaraTelefone(campo) {

        function trata(valor, isOnBlur) {

            valor = valor.replace(/\D/g, "");
            valor = valor.replace(/^(\d{2})(\d)/g, "($1)$2");
            if (isOnBlur) {

                valor = valor.replace(/(\d)(\d{4})$/, "$1-$2");
            } else {

                valor = valor.replace(/(\d)(\d{3})$/, "$1-$2");
            }
            return valor;
        }

        campo.onkeypress = function (evt) {

            var code = (window.event) ? window.event.keyCode : evt.which;
            var valor = this.value

            if (code > 57 || (code < 48 && code != 8)) {
                return false;
            } else {
                this.value = trata(valor, false);
            }
        }

        campo.onblur = function () {

            var valor = this.value;
            if (valor.length < 13) {
                this.value = ""
            } else {
                this.value = trata(this.value, true);
            }
        }

        campo.maxLength = 14;
    }
    if ($("#cadastro_exterior").val() == '') {
        $("#modal_pais").modal();
    }
    $("#selected_pais").click(function () {
        pais = $("#pais").val();
        if (pais == 'Brasil') {
            $('.cpf').inputmask("999.999.999-99");
            $('.cep').inputmask("99999-999", {"oncomplete": function () {
                    buscaCep();
                }});
            $("[data-mask]").inputmask();
            $("#cadastro_exterior").val('0');
            mascaraTelefone(formcadastro.telefone);
        } else {
            $("#cadastro_exterior").val('1');
            $('.cpf').attr('placeholder', 'Document');
            $('.estado_select').empty();
            $('.cidade_select').empty();
            $('.estado_select').html('<div class="form-group has-feedback"><input type="text" class="form-control " placeholder="<?= Lang::trans('site.state') ?>" name="estado" value="<?= old('estado') ?>"/></div>')
            $('.cidade_select').html('<div class="form-group has-feedback"><input type="text" class="form-control " placeholder="<?= Lang::trans('site.city') ?>" name="cidade" value=""/></div>')

        }
        $("#pais_fm").val(pais);
        $("#modal_pais").modal('hide');
    });
    $("#pais_fm").change(function () {
        pais = $("#pais_fm").val();
        if (pais == 'Brasil') {
            $('.cpf').inputmask("999.999.999-99");
            $('.cep').inputmask("99999-999", {"oncomplete": function () {
                    buscaCep();
                }});
            $("[data-mask]").inputmask();
            $("#cadastro_exterior").val('0');
            mascaraTelefone(formcadastro.telefone);
            $('.cpf').attr('placeholder', 'CPF');
            $('.estado_select').empty();
            $('.cidade_select').empty();
            estados_html = '<?= $estados_html ?>';
            $('.estado_select').html(estados_html);
            cidades_html = '<?= $cidades_html ?>';
            $('.cidade_select').html(cidades_html);
        } else {
            $("#cadastro_exterior").val('1');
            $('.cpf').attr('placeholder', 'Documento');
            $('.cpf').inputmask('remove');
            $('.cep').inputmask('remove');
            $('.estado_select').empty();
            $('.cidade_select').empty();
            $('.estado_select').html('<div class="form-group has-feedback"><input type="text" class="form-control " placeholder="<?= Lang::trans('site.state') ?>" name="estado" value="<?= old('estado') ?>"/></div>');
            $('.cidade_select').html('<div class="form-group has-feedback"><input type="text" class="form-control " placeholder="<?= Lang::trans('site.city') ?>" name="cidade" value="<?= old('cidade') ?>"/></div>');

        }
        $("#pais_fm").val(pais);
        $("#modal_pais").modal('hide');
    });
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
        $('.data').inputmask("dd-mm-yyyy");
    });</script>
.<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
    ga('create', 'UA-72220910-1', 'auto');
    ga('send', 'pageview');</script>
<script src="<?= env('CFURL') ?>/dist/js/jcombo.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/i18n/pt-BR.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
<script type="text/javascript">
    $(document).ready(function () {
        //$(".estados").select2();
        /// $(".pacote").select2();

    });
    function carregar_cidades() {
        if ($("#descCep").attr('data-cep') == 0) {
            $(".cidades").removeAttr('tabindex');
            $(".cidades").removeAttr('aria-hidden');
            // $(".cidades").select2();
            //$(".cidades").select2("destroy");
            $('.cidades')
                    .find('option')
                    .remove()
                    .end();
            valor = $(".estados").val();
            estadoSelecionado = valor;
            url = '<?= env('SITE_URL') ?>localizacao/cidade?id=' + estadoSelecionado + "&tp=2"
            $.get(url, function (data) {
                $(".cidades").html(data);
            });
            // $(".cidades").select2();
        }
    }
    $(".estados").change(function () {
        carregar_cidades();
    });
    function buscaCep() {
        cep = $('.cep').val();
        cep.replace("-", "");
        $("#descCep").html('Procurando....');
        url = 'https://viacep.com.br/ws/' + cep + '/json/';
        $.getJSON(url, function (data) {
            $("#descCep").html('');
            cidade = data.localidade;
            uf = data.uf;
            bairro = data.bairro;
            endereco = data.logradouro;
            console.log(data);
            if (uf != '') {
                $("#descCep").attr('data-cep', 1);
                urlEstado = "<?= env('SITE_URL') ?>localizacao/estado?uf=" + uf;
                $.getJSON(urlEstado, function (dataEstado) {
                    estadoSelecionado = dataEstado[0].id;
                    $(".estados").jCombo({url: '<?= env('SITE_URL') ?>localizacao/estado', dataType: "json", selected_value: estadoSelecionado});
                    //$(".estados").select2();
                    if (cidade != '') {
                        urlCidade = "<?= env('SITE_URL') ?>localizacao/cidade?uf=" + uf + '&nome=' + cidade;
                        $.getJSON(urlCidade, function (dataCidade) {
                            cidadeSelecionada = dataCidade[0].id;
                            url = '<?= env('SITE_URL') ?>localizacao/cidade?id=' + estadoSelecionado;
                            $(".cidades").jCombo({url: url, dataType: "json", selected_value: cidadeSelecionada});
                            //$(".cidades").select2();
                            $(".bairro").val(bairro);
                            $(".endereco").val(endereco);
                            $("#descCep").attr('data-cep', 0);
                        });
                    }

                });
            } else {
                $(".estados").jCombo({url: '<?= env('SITE_URL') ?>localizacao/estado', dataType: "json"});
            }


        });
    }


</script>

</body>

@endsection