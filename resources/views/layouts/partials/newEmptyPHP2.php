<?php

namespace App\Http\Controllers\Admin;

use App\Voucher;
use Illuminate\Http\Request;
use App\User;
use App\Pacote;
use App\extratos;
use App\Referrals;
use App\Pagamentos;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Binario;
use App\config;

class VoucherController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {


        $usuarios = User::all();
        return view('admin.pages.vouchers', compact('usuarios'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($user) {
        $usuario = User::find($user);
        return view('admin.pages.addvoucher', compact('usuario'));
    }

    public function remover($user) {
        $usuario = User::find($user);
        return view('admin.pages.removevoucher', compact('usuario'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id, \Faker\Generator $faker) {
        $data = \Input::all();

        $data['user_id'] = $id;

        for ($i = 0; $i < $data['quantidade']; $i++) {
            $data['voucher'] = $faker->sha1;
            $create = Voucher::create($data);
        }

        if ($create) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers Gerados Com Successo!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($user) {
        $data = \Input::all();
        for ($i = 0; $i < $data['quantidade']; $i++) {
            $voucher = Voucher::where('user_id', $user)->first();
            if ($voucher) {
                $destroy = Voucher::destroy($voucher->id);
            } else {
                $destroy = true;
                break;
            }
        }

        if ($destroy) {
            echo <<<EOL
                 <div class="alert alert-success fade in">
                      Vouchers removidos com sucesso!
                 </div>
EOL;
        } else {
            echo <<<EOL
       <div class="alert alert-danger fade in">
                      Ocorreu um erro! Por favor tente novamente.
                 </div>
EOL;
        }
    }

    public function ativarUsr($id = '') {


        if (@$id == '') {
            $id = @$_GET['id'];
        }
        $idAct = $id;
        $usr = new User();
        $bin = new Binario();

        $dataUs = User::where('id', $id)->first();
        $status = 1;

        if ($status == 0 or $status == 1) {
            
        } else {
            echo 'Operação Não realizada';
            exit();
        }
        if (is_numeric($id) and $dataUs['ativo'] <> 1) {
            $dataUser = User::where('id', $id)->first();
            $userPai = User::where('id', $dataUser['pai_id'])->first();
            $today = date("d-m-Y");
            $pacoteData = Pacote::where('id', $dataUser['pacote'])->first();

//pacotePai
            $porcentagemUsr = Pacote::where('id', $userPai['pacote'])->first()['porcentagem'];



            $pacoteValor = str_replace('.', '', $pacoteData['valor']);
            $pacoteValor = str_replace(',', '.', $pacoteValor);
            $porcentagemAdm = 1 - $porcentagemUsr;
            $valorBeneficiado = ($pacoteValor) * ($porcentagemUsr);
            $valorBeneficiado2 = ($pacoteValor) * ($porcentagemAdm);
            $valorBinario = ($pacoteValor) * ($pacoteData['porcentagem_bin']);

//ativa usr
            $user = new User();
            $user = User::find($id);
            $user->ativaUser();
//atualiza
            User::where('id', $id)->update(['pacote' => $dataUs['pacote'], 'pacote_status' => 1]);

            extratos::create(['user_id' => $id, 'data' => $today, 'descricao' => 'Novo pagamento', 'valor' => $valorBeneficiado, 'beneficiado' => $dataUser['pai_id']]);
            $novoSaldo = $valorBeneficiado + $userPai['saldo'];
            User::where('id', $dataUser['pai_id'])->update(['saldo' => $novoSaldo]);
            $beneficiado = Binario::where('user_id', $userPai['id'])->first();
            $pontosBeneficiado = $beneficiado['pontos'] + $valorBeneficiado;


            if ($pacoteData['binario'] == 1) {
                $pai = \App\Referrals::where('user_id', $dataUser['id'])->first()->system_id;
                $totalFilhos = $usr->getFilhos($pai, true);
                $beneficiado = Binario::where('user_id', $pai)->first();
                $pontosBeneficiado = $beneficiado['pontos'] + $valorBeneficiado;
                extratos::create(['user_id' => $id, 'data' => $today, 'descricao' => 'Pagamento', 'valor' => $valorBeneficiado2, 'beneficiado' => 1]);
                $bin->dist_binario($idAct, $valorBinario);
                $this->renova($pai);
            }
        }


        echo "Operação realizada com sucesso";
    }

    function mudarPacote($id = '', $pacote = '') {
        if (@$id <> '') {
            $_GET['id'] = $id;
        }
        if (@$pacote <> '') {
            $_GET['pacote'] = $pacote;
        }
        $pacote = $_GET['pacote'];
        $id = $_GET['id'];
        $idAct = $id;
        if (is_numeric($id) and is_numeric($pacote) and $id <> 1) {
            $dataUser = User::where('id', $id)->first();
            $userPai = User::where('id', $dataUser['pai_id'])->first();
            $today = date("d-m-Y");
            $pacoteData = Pacote::where('id', $pacote)->first();
//pacotePai
            $porcentagemUsr = Pacote::where('id', $pacote)->first()['porcentagem'];
            $pacoteValor = str_replace('.', '', $pacoteData['valor']);
            $pacoteValor = str_replace(',', '.', $pacoteValor);
            $porcentagemAdm = 1 - $porcentagemUsr;
            $valorBeneficiado = ($pacoteValor) * ($porcentagemUsr);
            $valorBeneficiado2 = ($pacoteValor) * ($porcentagemAdm);
            $valorBinario = ($pacoteValor) * ($pacoteData['porcentagem_bin']);

//ativa usr
            $user = new User();
            $user = User::find($id);
            $user->ativaUser();
//atualiza
//User::where('id', $id)->update(['pacote' => $dataUser['pacote'], 'pacote_status' => 1]);
            User::where('id', $id)->update(['pacote' => $pacote]);
            extratos::create(['user_id' => $id, 'data' => $today, 'descricao' => 'Novo pagamento', 'valor' => $valorBeneficiado, 'beneficiado' => $dataUser['pai_id']]);
            $novoSaldo = $valorBeneficiado + $userPai['saldo'];
            User::where('id', $dataUser['pai_id'])->update(['saldo' => $novoSaldo, 'pacote' => $pacote]);
            $beneficiado = Binario::where('user_id', $userPai['id'])->first();
            $pontosBeneficiado = $beneficiado['pontos'] + $valorBeneficiado;
            if ($pacoteData['binario'] == 1) {
                extratos::create(['user_id' => $id, 'data' => $today, 'descricao' => 'Pagamento', 'valor' => $valorBeneficiado2, 'beneficiado' => 1]);
                $bin->dist_binario($idAct,$valorBinario);
                $this->renova($userPai['id']);
            }
            echo 'Operação realizada.';
        } else {
            echo 'Operação não realizada.';
        }
    }

    function renova($id) {
        $config = new config();
        $config = $config->getConfig();
        if ($config['renovacao_status'] == 'sim') {
            $dataUser = User::where('id', $id)->first();
            $today = date("d-m-Y");
            $pacoteData = Pacote::where('id', $dataUser['pacote'])->first();
            //
            $ganhoLimite = $pacoteData['valor'] * $config['renovavao'];
            $saldo = extratos::where('beneficiado', $id)->where('descricao', 'Divisão de lucros')->sum('valor');

            if ($saldo >= $ganhoLimite and $dataUser['admin'] <> 1) {

                $novoSaldo = $dataUser['saldo'] - ($pacoteData['valor'] * 1.1);
                $data['saldo'] = $novoSaldo;
                User::where('id', $id)->update($data);
                extratos::where('beneficiado', $id)->where('descricao', 'Divisão de lucros')->delete();
                $valorRenovacao = ($pacoteData['valor'] * 1.1) * (-1);
                extratos::create(['user_id' => $id, 'data' => $today, 'descricao' => 'Renovação automática', 'valor' => ($pacoteData['valor'] * 1.1), 'beneficiado' => 1]);
                extratos::create(['user_id' => 1, 'data' => $today, 'descricao' => 'Renovação automática', 'valor' => $valorRenovacao, 'beneficiado' => $id]);
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    function statusSaque() {
        $id = $_GET['id'];
        if ($id == '' or ! is_numeric($id)) {
            echo 'Erro';
        } else {
            $dataUser = User::where('id', $id)->first();
            if ($dataUser['saque'] == 'ativo') {
                $status = 'inativo';
            } else {
                $status = 'ativo';
            }
            $data['saque'] = $status;
            User::where('id', $id)->update($data);
            echo "Operação realizada com sucesso";
        }
    }

}

