<!-- jQuery 2.1.4 -->
<script src="{{ env('CFURL').('/plugins/jQuery/jQuery-2.1.4.min.js') }}"></script>
<!-- Bootstrap 3.3.2 JS -->
<script src="{{ env('CFURL').('/js/bootstrap.min.js') }}" type="text/javascript"></script>
<!-- InputMask -->
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<!-- iCheck -->
<script src="{{ env('CFURL').('/plugins/iCheck/icheck.min.js') }}" type="text/javascript"></script>

<script>
    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-72220910-1', 'auto');
    ga('send', 'pageview');
</script>