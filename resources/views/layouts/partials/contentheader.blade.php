<!-- Content Header (Page header) -->

<div class="row">
    <div class="col-md-12">
      @if(isset($_GET['err_msg']) and @$_GET['err_msg']!='')
      <div class="alert alert-error fade in alert-dismissible flat  no-margin">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i> Atenção!</h4>
        {{$_GET['err_msg']}}
    </div>
    @endif
      @if(isset($_GET['sucess_msg']) && @$_GET['sucess_msg'] !='')
      <div class="alert alert-success fade in alert-dismissible flat  no-margin">
        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
        <h4><i class="icon fa fa-check"></i>Sucesso!</h4>
        {{$_GET['sucess_msg']}}
    </div>
    @endif
        @if(isset($_GET['payment_succes']) || @$_GET['payment_return']==1)
        <div class="alert alert-success fade in alert-dismissible flat  no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
            Pagamento realizado com sucesso.

        </div>
        @endif

        @if(session('success'))
        <div class="alert alert-success fade in alert-dismissible flat  no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
            {{session('success')}}
        </div>
        @endif
        @if(Auth::user()->renovaUsuario()  && Auth::user()->ativado() && !Auth::user()->isAdmin())
        <div class="alert alert-success fade in alert-dismissible flat  no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-check"></i> Sucesso!</h4>
            Sua ativação mensal foi realizada com sucesso. 

        </div>


        @endif

        @if (isset($errors) && count($errors) > 0)
        <div class="alert alert-danger alert-dismissible flat no-margin">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <strong>Whoops!</strong><br>
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        @endif
    </div>
</div>


<section class="content-header">
    <h1>
        @yield('contentheader_title', 'Page Header here')
        <small>@yield('contentheader_description')</small>
    </h1>
    <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home </a></li>
        @yield('breadcrumb', '<li class="active">Dashboard</li>')
    </ol>
</section>
