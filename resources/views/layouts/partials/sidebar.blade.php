<!-- Left side column. contains the logo and sidebar -->
<?php
global $request;
$uriPainel = $request->segment(1);
$uriPage = $request->segment(2);
?>
<aside class="main-sidebar">
    <!-- sidebar: folha de estilo se encontra em sidebar.less -->
    <section class="sidebar">
        @if (! Auth::guest() && !Auth::user()->ativo)
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{Auth::user()->photo}}" width="64" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{Auth::user()->getShortName()}}</p>
                    <!-- Status -->

                    <i class="fa fa-thumb-tack"></i>

                    </a>
                </div>
            </div>
            <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" placeholder="Search"/>
                    <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header"><?= Lang::trans('MENU PRINCIPAL') ?></li>
                <li class="{{$uriPage == 'inativo' ? 'active' : ''}}"><a href="{{ url('/'.$uriPainel.'/inativo') }}"><i class='fa fa-dashboard'></i><span>Dashboard</span></a></li>

                <li class="treeview"  class="{{$uriPage == 'minha-rede' ? 'active' : ''}}{{$uriPage == 'meus-indicados' ? 'active' : ''}}{{$uriPage == 'unilevel' ? 'active' : ''}}">
                    <a href="#">
                        <i class="fa fa-user-plus"></i><span><?= Lang::trans('Minha Rede') ?></span>
                        <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{$uriPage == 'minha-rede' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/minha-rede')}}"><i class='fa fa-circle-o'></i><span>Rede</span></a></li>
                    </ul>
                </li>
            </ul>
        @endif
        @if (! Auth::guest() && Auth::user()->ativo)
            <div class="user-panel">
                <div class="pull-left image">
                    <img onclick="" class="img-circle" src="{{Auth::user()->photo}}" width="128">
                </div>
                <div class="pull-left info">
                    <p>{{Auth::user()->getShortName()}}</p>
                    <!-- Status -->

                    <i class="fa fa-thumb-tack"></i> {{ \Auth::user()->username }}

                    </a>


                </div>
            </div>
        @endif


        @if(Auth::user()->ativo)
        <!-- search form (Optional) -->
            <form action="#" method="get" class="sidebar-form">
                <div class="input-group">
                    <input type="text" name="q" class="form-control" disabled placeholder="Pesquisar..."/>
                    <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i></button>
                </span>
                </div>
            </form>
            <!-- /.search form -->

            <!-- Sidebar Menu -->
            <ul class="sidebar-menu">
                <li class="header">MENU PRINCIPAL</li>
                <!-- Optionally, you can add icons to the links -->

                <li class="{{$uriPage == 'home' ? 'active' : ''}}"><a href="{{ url('/'.$uriPainel.'/home') }}"><i class='fa fa-dashboard'></i> <span>Painel de Controle</span></a></li>

                @if (! Auth::guest()  && $uriPainel == 'painel')


                    <li class="{{$uriPage == 'meus-dados' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-dados')}}"><i class='fa fa-user'></i> <span>Meus Dados</span></a></li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-shopping-cart "></i><span>Loja Virtual</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{$uriPage == 'produtos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/produtos')}}"><i class="fa fa-circle-o"></i>Produtos</a></li>
                            <li class="{{$uriPage == 'meu-carrinho' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meu-carrinho')}}"><i class="fa fa-circle-o"></i>Carrinho de Compras</a></li>
                            <li class="{{$uriPage == 'meus-pedidos' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-pedidos')}}"><i class="fa fa-circle-o"></i><span>Meus Pedidos</span></a></li>
                        </ul>
                    </li>
                    <!-- Fim do botão deslizador da Loja virtual -->


                    <li class="treeview"  class="{{$uriPage == 'minha-rede' ? 'active' : ''}}{{$uriPage == 'meus-indicados' ? 'active' : ''}}{{$uriPage == 'unilevel' ? 'active' : ''}}">
                    <li class="treeview"  class="{{$uriPage == 'minha-rede' ? 'active' : ''}}{{$uriPage == 'meus-indicados' ? 'active' : ''}}{{$uriPage == 'unilevel' ? 'active' : ''}}">
                        <a href="#">
                            <i class="fa fa-users"></i> <span>Minha Rede</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{$uriPage == 'meus-indicados' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/meus-indicados')}}"><i class='fa fa-circle-o'></i> <span>Indicados Diretos</span></a></li>
                            <li class="{{$uriPage == 'minha-rede' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/minha-rede')}}"><i class='fa fa-circle-o'></i> <span>Rede Binária</span></a></li>

                            <li class="{{$uriPage == 'unilevel' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/unilevel')}}"><i class='fa fa-circle-o'></i> <span>Rede Unilevel </span></a></li>

                        </ul>
                    </li>

                    <li class="treeview"  class="{{$uriPage == 'transacoes' ? 'active' : ''}}{{$uriPage == 'faturas' ? 'active' : ''}}{{$uriPage == 'saques' ? 'active' : ''}}{{$uriPage == 'gerenciar_saldo' ? 'active' : ''}}">
                        <a href="#">
                            <i class="fa fa-dollar"></i> <span>Financeiro</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{$uriPage == 'transacoes' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/transacoes')}}"><i class='fa fa-circle-o'></i> <span>Extrato financeiro</span></a></li>
                            <li><a href="#converterSaldo" data-toggle="modal"><i class='fa fa-dollar'></i> <span>Converter Vulcoins</span></a></li>
                            <li><a data-toggle="modal" data-target="#modal-left"><i class='fa fa-user'></i> <span>Ativar Usuário</span></a></li>
                            <li><a href="{{ url('painel/rendimentos') }}"><i class='fa fa-circle-o'></i> <span>Rendimentos</span></a></li>
                            <li><a href="{{ route('transacoes') }}"><i class='fa fa-circle-o'></i> <span>Consultar Transação</span></a></li>
                            <li><a href="{{url('Vulcoin-qt.zip')}}"> <i class="fas fa-wallet"> </i> &nbsp Carteira desktop </a></li>
                        </ul>
                    </li>

                    <li><a href="{{route('sacar')}}" data-toggle="modal"><i class="fa fa-money" aria-hidden="true"></i> <span>Sacar Vulcoins</span></a></li>
                    <li><a href="{{route('history.saque')}}" data-toggle="modal"><i class="fa fa-money" aria-hidden="true"></i> <span>Histórico de saque</span></a></li>

                    <li><a href="#void" onclick="$('#upgrade').modal('show');"><i class='fa fa-circle-o'></i> <span>Programa de investimentos</span></a></li>

                    <li class="{{$uriPage == 'materiais' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/materiais')}}"><i class='fa fa-download'></i> <span>Download</span></a></li>
                    <li class="{{$uriPage == 'materiais' ? 'active' : ''}}"><a href="{{ url('/'.$uriPainel.'/logout') }}"><i class='fa fa-ban'></i> <span>Sair</span></a></li>
                @endif
                @if (! Auth::guest() && Auth::user()->isAdmin() && $uriPainel == 'admin')
                <!-- Botão deslizador da Loja virtual -->

                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-shopping-cart"></i>Gerenciar Loja</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">

                            <li class="{{$uriPage == 'produtos/add' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/produtos/add')}}"><i class="fa fa-circle-o"></i>Adicionar Produtos</a></li>
                            <li class="{{$uriPage == 'produtos/list' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/produtos/list')}}"><i class="fa fa-circle-o"></i>Lista de Produtos</a></li>
                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-shopping-cart "></i>Pedidos</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{$uriPage == 'pedidos' ? 'active' : ''}}"><a href="{{url('/admin/pedidos')}}"><i class="fa fa-circle-o"></i>Todos</a></li>
                            <li class="{{$uriPage == 'pedidos' ? 'active' : ''}}"><a href="{{url('/admin/pedidos?status=Pendente')}}"><i class="fa fa-circle-o"></i>Pendentes</a></li>
                            <li class="{{$uriPage == 'pedidos' ? 'active' : ''}}"><a href="{{url('/admin/pedidos?status=Pago')}}"><i class="fa fa-circle-o"></i>Pagos</a></li>
                            <li class="{{$uriPage == 'pedidos' ? 'active' : ''}}"><a href="{{url('/admin/pedidos?status=Cancelado')}}"><i class="fa fa-circle-o"></i>Cancelados</a></li>
                        </ul>

                    </li>



                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-gears "></i> <span>Gerenciamento</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">


                            <li class="{{$uriPage == 'usuarios' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/usuarios')}}"><i class='fa fa-circle-o'></i> <span>Gerenciar Usuários</span></a></li>
                            <li class="{{$uriPage == 'pacote' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/pacote')}}"><i class='fa fa-circle-o'></i> <span>Gerenciar Pacotes</span></a></li>
                        <!--<li class="{{$uriPage == 'graduacoes' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/graduacoes')}}"><i class='fa fa-circle-o'></i> <span>Gerenciar Graduações</span></a></li>-->
                            <li class="{{$uriPage == 'config' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/config')}}"><i class='fa fa-circle-o'></i> <span>Configurações Gerais</span></a></li>
                            <li class="{{$uriPage == 'ico' ? 'active' : ''}}"><a href="{{route('ico.index')}}"><i class='fa fa-circle-o'></i> <span>Gerenciar valor do vulcoin</span></a></li>

                        </ul>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i class="fa fa-upload "></i> <span>Financeiro</span>
                            <i class="fa fa-angle-left pull-right"></i>
                        </a>
                        <ul class="treeview-menu">

                            <li class="{{$uriPage == 'relatorios' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/relatorios')}}"><i class='fa fa-circle-o'></i> <span>Relatórios</span></a></li>
                            <li><a onclick="if (!confirm('Você tem certeza?')) {
                                return false;
                            }" href="{{url('admin/rodar-binario')}}"><i class='fa fa-circle-o'></i> <span>Executar Binário</span></a></li>
                            <li><a href="javascript:$('#addSaldo').modal();"><i class='fa fa-circle-o'></i> <span>Divisão de Lucros</span></a></li>
                            <li><a href="{{ url('admin/investimentos/usuarios') }}"><i class='fa fa-circle-o'></i> <span>Investimentos</span></a></li>
                        </ul>
                    </li>

                    @if(Auth::user()->username == 'augusto')
                        <li class="{{$uriPage == 'licencas' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/licencas')}}"><i class='fa fa-lock'></i> <span>Licen&ccedil;as</span></a></li>
                    @endif
                    <li class="{{$uriPage == 'materiais' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/materiais')}}"><i class='fa fa-folder'></i> <span>Materiais</span></a></li>
                    <li class="{{$uriPage == 'saque' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/saque')}}"><i class='fa fa-dollar'></i> <span>Relatório de Saques</span></a></li>
                    <li class="{{$uriPage == 'materiais' ? 'active' : ''}}"><a href="{{ url('/'.$uriPainel.'/logout') }}"><i class='fa fa-ban'></i> <span>Sair</span></a></li>

                <?php
                if (isset($_GET['infoBin'])) {
                    echo "<script> alert('Operação realizada com sucesso')</script>";
                }
                ?>

            @endif
            <!--<li class="{{$uriPage == 'licencas' ? 'active' : ''}}"><a href="{{url('/'.$uriPainel.'/licencas/add')}}"><i class='fa fa-lock'></i> <span>Adicionar Licen&ccedil;as</span></a></li> -->

            </ul><!-- /.sidebar-menu -->
        @endif
    </section>
    <!-- /.sidebar -->
</aside>
