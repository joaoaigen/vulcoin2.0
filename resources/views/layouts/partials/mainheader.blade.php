<style type="text/css">
    .goog-te-banner-frame.skiptranslate{display:none!important;}
    body{top:0px!important;}
</style>
<!-- Main Header -->
<header class="main-header"><meta http-equiv="Content-Type" content="text/html; charset=euc-jp">
    <?php
    global $request;
    $uriPainel = $request->segment(1);
 ;
    $uriPainel = $request->segment(1);
    if (Auth::user()->isAdmin() and Auth::user()->ativo == 1 and $uriPainel != 'admin') {
        header('Location: ' . url('admin/home'));
        exit;
    }

    ?>

    <a href="{{ url('/'.$uriPainel.'/home') }}" class="logo" style="background: #FFFFFF;">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"> <img src="{{url('/img/logo-mini.png')}}" width='80%'/></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><img src="{{url('/img/logo-escritorio.png')}}" width='100%'  style="margin-top: -13px;" /></span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div style="width:280px; display: block; position: absolute; right:5%; margin-top: 10px; margin-right: 10px;">
            <style>
                .item-lang {
                    display: inline-block;
                    margin-right: 5px;
                }
                .item-lang img{
                    height: 30px;
                }
            </style>
            <a href="/language?lang=pt" class="item-lang"><img src='/img/flags/pt-br.png'></a>
            <a href="/language?lang=en" class="item-lang"><img src='/img/flags/en-us.png'></a>
            <a href="/language?lang=es" class="item-lang"><img src='/img/flags/es-es.png'></a>
            <a href="/language?lang=it" class="item-lang"><img src='/img/flags/pt-it.png'></a>
        </div>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                <li><a href="{{ url('/'.$uriPainel.'/login') }}">Login</a></li>
                @else
                @if (! Auth::guest() && !Auth::user()->ativo)
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{Auth::user()->photo}}" width="64" class="user-image" alt="User Image" />
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->name }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img src="{{Auth::user()->getAvatar()}}" class="img-circle" alt="User Image"/>
                            <p>
                                {{ Auth::user()->getShortName() }}
                                <small><?= Lang::trans('Membro desde') ?>{{Auth::user()->memberSince()}}</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('/'.$uriPainel.'/meus-dados') }}" class="btn btn-default btn-flat"><?= Lang::trans('Meus Dados') ?></a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('/'.$uriPainel.'/logout') }}" class="btn btn-default btn-flat"><?= Lang::trans('Encerrar Sessao') ?></a>
                            </div>
                        </li>
                    </ul>
                </li>
                @endif
                @if(Auth::user()->ativo)
                <!-- User Account Menu -->
                <li class="dropdown user user-menu">
                    <!-- Menu Toggle Button -->
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <!-- The user image in the navbar-->
                        <img src="{{Auth::user()->photo}}" width="64" class="user-image" alt="User Image"/>
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                        <span class="hidden-xs">{{ Auth::user()->getShortName() }}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- The user image in the menu -->
                        <li class="user-header">
                            <img onclick="" class="img-circle" src="{{Auth::user()->photo}}">
                            <p>
                                {{ Auth::user()->getShortName() }}
                                <small><?= Lang::trans('Membro desde') ?> {{Auth::user()->memberSince()}}</small>
                            </p>
                        </li>

                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="{{ url('/'.$uriPainel.'/meus-dados') }}" class="btn btn-default btn-flat"><?= Lang::trans('Meus Dados') ?></a>
                            </div>
                            <div class="pull-right">
                                <a href="{{ url('/'.$uriPainel.'/logout') }}" class="btn btn-default btn-flat"><?= Lang::trans('Encerrar Sessao') ?></a>
                            </div>
                        </li>
                    </ul>
                </li>
                @endif

                @endif

                <!-- Control Sidebar Toggle Button -->
                <?php if (!Auth::guest() && Auth::user()->isAdmin() && $uriPainel == 'admin') { ?>
                    <li>
                        <a href="{{ url('/'.$uriPainel.'/config') }}" ><i class="fa fa-gears"></i></a>
                    </li>
                <?php } else { ?>
                    <li>
                        <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
                    </li>
                <?php } ?>
            </ul>
        </div>
    </nav>
</header>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="/js/jquery.cookie.js"></script>	
<script type="text/javascript">
	$.cookie('googtrans', '/pt/' + $.cookie('lang'), { expires: 7 });
</script>
	
<div id="google_translate_element" style="display: none;"></div>
<script type="text/javascript">
    function googleTranslateElementInit() {
      new google.translate.TranslateElement({pageLanguage: 'pt', layout: google.translate.TranslateElement.InlineLayout.SIMPLE}, 'google_translate_element');
    }
</script>
<script type="text/javascript" src="//translate.google.com/translate_a/element.js?cb=googleTranslateElementInit"></script>
