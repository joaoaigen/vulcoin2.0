@extends('layouts.admin')
@section('title-head')
    Suporte Vulcoin
@endsection
@section('title-body')
    Suporte Vulcoin
@endsection

@section('page-css')
@endsection

@section('main-content')
    <section class="content">
        <div class="row">
            <div class="col-lg-<?php echo isset($mensagens) ? '3' : '12' ?> col-md-12">
                <div class="box">
                    <div class="box-header no-border bg-dark p-0 pt-10">
                        <div class="form-element">
                            <input class="form-control text-white p-20" type="text" placeholder="Mensagens">
                        </div>
                    </div>
                    <div class="box-body p-0">
                        <div class="media-list media-list-hover media-list-divided ">

                            @foreach($aberto as $row)
                                @if(!empty($row))
                                    <div class="media media-single">
                                        <div class="media-body">
                                            <h6>
                                                <a href="{{ url('/admin/suporte/mensagem/?mensagem=' . $row->id) }}">{{ $row->assunto }}</a>
                                            </h6>
                                            <small class="text-green">Aberto</small>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                            @foreach($andamento as $row)
                                @if(!empty($row))
                                    <div class="media media-single">
                                        <div class="media-body">
                                            <h6>
                                                <a href="{{ url('/admin/suporte/mensagem/?mensagem=' . $row->id) }}">{{ $row->assunto }}</a>
                                            </h6>
                                            <small class="text-warning">Lido / Em andamento</small>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                            @foreach($resolvido as $row)
                                @if(!empty($row))
                                    <div class="media media-single">
                                        <div class="media-body">
                                            <h6>
                                                <a href="{{ url('/admin/suporte/mensagem/?mensagem=' . $row->id) }}">{{ $row->assunto }}</a>
                                            </h6>
                                            <small class="text-red">Finalizado</small>
                                        </div>
                                    </div>
                                @endif
                            @endforeach

                        </div>
                    </div>
                </div>
                <!-- /. box -->
            </div>
            <!-- /.col -->
            @isset($mensagens)
                <div class="col-lg-9 col-md-12">
                    <div class="box direct-chat">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                Chat - Mensagens
                                @isset($mensagens)
                                    - {{ $suporte->assunto }}
                                @endisset
                            </h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <!-- Conversations are loaded here -->
                            <div id="chat-app" class="direct-chat-messages chat-app">


                            @foreach($mensagens as $row)
                                @if($row->id_user == $suporte->user_id)
                                    <!-- Message. Default to the left -->
                                        <div class="direct-chat-msg mb-30">
                                            <div class="clearfix mb-15">
                                                <span
                                                    class="direct-chat-name">{{ \App\User::where('id', $suporte->user_id)->select(['username'])->first()->username }}</span>
                                                <span
                                                    class="direct-chat-timestamp pull-right">{{ date('d/m/Y H:i:s', strtotime($row->updated_at ))}} </span>
                                            </div>
                                            <!-- /.direct-chat-info -->
                                            <img class="direct-chat-img avatar"
                                                 src="{{ \App\User::where('id', $suporte->user_id)->select(['photo'])->first()->photo }}"
                                                 alt="message user image">
                                            <!-- /.direct-chat-img -->
                                            <div class="direct-chat-text">
                                                {{ $row->mensagem }}
                                            </div>

                                            <!-- /.direct-chat-text -->
                                        </div>
                                        <!-- /.direct-chat-msg -->
                                @endif

                                @if($row->id_user == $suporte->suporte_user)
                                    <!-- Message to the right -->
                                        <div class="direct-chat-msg right mb-30">
                                            <div class="clearfix mb-15">
                                                <span
                                                    class="direct-chat-name pull-right">{{ \App\User::where('id', $suporte->suporte_user)->select(['username'])->first()->username }}</span>
                                                <span
                                                    class="direct-chat-timestamp">{{ date('d/m/Y H:i:s', strtotime($row->updated_at ))}} </span>
                                            </div>
                                            <!-- /.direct-chat-info -->
                                            <img class="direct-chat-img avatar"
                                                 src="{{ \App\User::where('id', $suporte->suporte_user)->select(['photo'])->first()->photo }}"
                                                 alt="message user image">
                                            <!-- /.direct-chat-img -->
                                            <div class="direct-chat-text">
                                                {{ $row->mensagem }}
                                            </div>

                                            <!-- /.direct-chat-text -->
                                        </div>
                                @endif
                                <!-- /.direct-chat-msg -->
                                @endforeach

                            </div>
                            <!--/.direct-chat-messages-->
                        </div>
                        <!-- /.box-body -->
                        <div class="box-footer">

                            <div class="input-group">
                                <input type="text" name="message" id="message" placeholder="Insira uma mensagem..."
                                       class="form-control">
                                <span class="input-group-btn">
                                        <input hidden type="text" name="id_suporte" id="id_suporte"
                                               value="{{ $suporte->id }}">
                                    <input hidden value="">
                        <button type="button" class="btn btn-warning" id="sendForm">Enviar</button>
                      </span>
                            </div>
                        </div>
                        <!-- /.box-footer-->
                    </div>
                    <!-- /. box -->
                </div>
        @endisset
        <!-- /.col -->
        </div>
    </section>
@endsection
@section('page-js')

    <!-- This is data table -->
    <script
        src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

    <!-- start - This is for export functionality only -->
    <script
        src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
    <script
        src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
    <script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
    <script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
    <script
        src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
    <script
        src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
    <!-- end - This is for export functionality only -->

    <!-- Crypto_Admin for Data Table -->
    <script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>
    <script src="{{ asset('../../assets/js/pages/app-chat.js') }}"></script>
    <script type="text/javascript">
        $('#extrato_saque').DataTable({
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            "order": [[0, "desc"]]
        });
    </script>

    <script type="text/javascript">

        $("#sendForm").click(function () {
            $.ajax({
                url: '{{ url('/painel/suporte/sendAjax') }}' ,
                method: 'get',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                },
                data: {
                    body: $("#message").val(),
                    id_suporte: $("#id_suporte").val(),
                    id: '{{ \Illuminate\Support\Facades\Auth::user()->id }}'
                },
                beforeSend: function () {

                }
            })
                .done(function (msg) {
                    var data = new Date(msg.updated_at);
                    var day = data.getDate();
                    var month = data.getMonth() + 1;
                    var year = data.getFullYear();

                    var hora = data.getHours();
                    var minuto = data.getMinutes();
                    var segundos = data.getSeconds();

                    if (month < 10) {
                        month = "0" + month;
                    }

                    var date = day + "/" + month + "/" + year + " " + hora + ":" + minuto + ":" + segundos;

                    $("#message").empty();

                    $('#chat-app').append('<div class="direct-chat-msg right mb-30"> ' +
                        '                                            <div class="clearfix mb-15"> ' +
                        '                                                <span\n' +
                        '                                                    class="direct-chat-name pull-right">' + msg.name + '</span> ' +
                        '                                                <span\n' +
                        '                                                    class="direct-chat-timestamp">' + date +'</span> ' +
                        '                                            </div>\n' +
                        '                                            <!-- /.direct-chat-info --> ' +
                        '                                            <img class="direct-chat-img avatar" ' +
                        '                                                 src=" ' + msg.photo + ' " ' +
                        '                                                 alt="message user image"> ' +
                        '                                            <!-- /.direct-chat-img --> ' +
                        '                                            <div class="direct-chat-text"> ' +
                        '                                                ' + msg.mensagem + ' ' +
                        '                                            </div> ' +
                        ' ' +
                        '                                            <!-- /.direct-chat-text --> ' +
                        '                                        </div>');
                })
                .fail(function (jqXHR, textStatus, msg) {
                    console.log('msg  >  ' + msg + '   status  > ' + textStatus);
                });
        });

    </script>
@endsection

