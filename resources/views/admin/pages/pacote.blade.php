@extends('layouts.admin')
@section('title-head')
Pacotes de investimento
@endsection
@section('title-body')
Pacotes de investimento
@endsection
@section('page-css')

@endsection
@section('main-content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-xl-12 col-md-12 col-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Novo Pacote</h3>                        
                </div>
                <div class="box-body">
                    @if (isset($errors) && count($errors) > 0)
                    <div class="alert alert-danger">
                        <strong>Whoops!</strong> Houve alguns problemas com a sua entrada.<br><br>
                        <ul>
                            @foreach ($errors->all() as $error)
                            <li>{{ $error == 'The g-recaptcha-response field is required.' ? 'É preciso verificar o Captcha!' : $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <form method="post" action="{{ url('admin/pacotes') }}">
                        {{ csrf_field() }}
                        <!-- Date dd/mm/yyyy -->
                        <div class="form-group">
                            <label>Nome do pacote</label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-archive"></i>
                                    </div>
                                </div>

                                <input type="text" class="form-control" required="required" placeholder="Informe o nome do pacote." name="nome">
                            </div>
                            <!-- /.input group -->
                        </div>

                        <div class="form-group">
                            <label>Valor</label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-dollar"></i>
                                    </div>
                                </div>

                                <input type="text" class="form-control valor" required="required" placeholder="Informe o valor do pacote." name="valor">
                            </div>
                            <!-- /.input group -->
                        </div>
                        
                        <div class="form-group">
                            <label>Porcentagem máxima</label>

                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <div class="input-group-text">
                                        <i class="fa fa-percent"></i>
                                    </div>
                                </div>

                                <input type="text" class="form-control porcentagem" required="required" placeholder="Informe o porcentagem máxima do pacote." name="porcentagem_maxima">
                            </div>
                            <!-- /.input group -->
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Novo pacote</button>
                        </div>
                    </form>
                    <!-- /.form group -->
                </div>
                <!-- /.box-body -->
            </div>
        </div>

        <div class="col-12">
            <div class="box box-solid bg-dark">
                <div class="box-header with-border">
                    <h3 class="box-title">Pacotes</h3>
                    <h6 class="box-subtitle">Listagem de todos os pacotes.</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="example" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Nome</th>
                                    <th>Valor</th>
                                    <th>Descrição</th>
                                    <th>Porcentagem Máxima</th>
                                    <th>Quantidade de dias</th>
                                </tr>
                            </thead>
                            <tbody>                                    
                                @foreach(DB::table('pacotes_investimentos')->get() as $row)
                                <tr>
                                    <td>{{$row->id}}</td>
                                    <td>{{$row->nome}}</td>
                                    <td>{{$row->valor}}</td>
                                    <td>{{$row->descricao}}</td>
                                    <td>{{$row->porcentagem_maxima}}</td>
                                    <td>{{$row->qntd_dias}}</td>                                       
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>      

    </div>
</section>
<!-- /.content -->
@endsection
@section('page-js')

<!-- This is data table -->
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

<!-- start - This is for export functionality only -->
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('../../assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<!-- end - This is for export functionality only -->

<!-- Crypto_Admin for Data Table -->
<script src="{{ asset('../../assets/js/pages/data-table.js') }}"></script>
@endsection

