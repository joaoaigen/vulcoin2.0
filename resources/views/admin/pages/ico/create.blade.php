@extends('layouts.admin')
@section('title-head')
Novo preço
@endsection
@section('title-body')
Novo preço
@endsection
@section('page-css')
<link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
@endsection
@section('main-content')

<section class="content">
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="box box-solid bg-dark">
                <div class="box-header with-border">
                    <h3 class="box-title">Novo preço</h3>
                    <h6 class="box-subtitle">Valor atual do Vulcoin {{App\Ico::where('status',1)->first()->price}}</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <form action="{{ route('ico.save') }}" method="post">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label>Preço</label>
                            <input type="text" class="form-control valor" name="price" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Criar</button>
                    </form>
                </div>                    
            </div>                 
        </div>
    </div>        
</section>
@endsection
@section('page-js')

<!-- This is data table -->
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('../assets/js/echarts.min.js') }}"></script>
<script type="text/javascript">
$('#extratos').DataTable({
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "order": [[0, "desc"]]
});
</script> 
@endsection