@extends('layouts.admin')
@section('title-head')
Preço Vulcoin
@endsection
@section('title-body')
Preço Vulcoin
@endsection
@section('page-css')
<link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
@endsection
@section('main-content')

<section class="content">
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="box box-solid bg-dark">
                <div class="box-header with-border">
                    <h3 class="box-title">Vulcoins</h3>
                    <h6 class="box-subtitle">Valor atual do Vulcoin {{App\Ico::where('status',1)->first()->price}}</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="extratos" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <td>Id</td>
                                    <td>Preço</td>
                                    <td>Status</td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(\App\Ico::all() as $i)
                                <tr>
                                    <td>{{$i->id}}</td>
                                    <td>{{$i->price}}</td>
                                    <td>{{$i->status == 1 ? 'Ativo' : 'Não ativo'}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    
                    <a href="{{route('ico.create')}}" class="btn btn-primary">Adicionar novo registro</a>
                </div>                    
            </div>                 
        </div>
    </div>        
</section>
@endsection
@section('page-js')

<!-- This is data table -->
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('../assets/js/echarts.min.js') }}"></script>
<script type="text/javascript">
$('#extratos').DataTable({
    dom: 'Bfrtip',
    buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
    ],
    "order": [[0, "desc"]]
});
</script> 
@endsection
