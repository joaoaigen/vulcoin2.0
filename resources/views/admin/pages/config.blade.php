<?php
$config = new App\config;
$config = $config->getConfig();
?>
@extends('layouts.admin')
@section('title-head')
 Configurações do sistema
@endsection
@section('title-body')
Configurações do sistema
@endsection
@section('page-css')
<link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
@endsection
@section('main-content')
<!-- Main content -->
<section class="content">
    <div class="row">

        <section class="col-lg-12">

            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#tab_1" data-toggle="tab" aria-expanded="false">Habilitar e Desabilitar</a></li>
                    <li class=""><a href="#tab_2" data-toggle="tab" aria-expanded="false">Bônus e valores</a></li>
                </ul>

                <form role="form" method="post" action="{{ url('/admin/config')}}">
                    {{ csrf_field() }}
                    {{ method_field('POST') }}
                    <div class="box-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="form-group has-feedback">
                                    <label>Saque</label>
                                    <select name="permitir_saque" class="form-control" style="width: 100%">
                                        <option value="sim" <?php echo $config->permitir_saque == 'sim' ? 'selected' : '' ?> >Ativo</option>
                                        <option value="nao" <?php echo $config->permitir_saque == 'nao' ? 'selected' : '' ?> >Inativo</option>
                                    </select>
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Mensalidade</label>
                                    <select name="mensalidade_ativo" class="form-control" style="width: 100%">
                                        <option value="sim" <?php echo $config->mensalidade_ativo == 'sim' ? 'selected' : '' ?> >Ativo</option>
                                        <option value="nao" <?php echo $config->mensalidade_ativo == 'nao' ? 'selected' : '' ?> >Inativo</option>
                                    </select>
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Planos de investimetnos</label>
                                    <select name="bloquear_investimento" class="form-control" style="width: 100%">
                                        <option value="1" <?php echo $config->bloquear_investimento == '1' ? 'selected' : '' ?> >Ativo</option>
                                        <option value="0" <?php echo $config->bloquear_investimento == '0' ? 'selected' : '' ?> >Inativo</option>
                                    </select>
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Bloquear conversão</label>
                                    <select name="bloquear_conversao" class="form-control" style="width: 100%">
                                        <option value="1" <?php echo $config->bloquear_conversao == '1' ? 'selected' : '' ?> >Ativo</option>
                                        <option value="0" <?php echo $config->bloquear_conversao == '0' ? 'selected' : '' ?> >Inativo</option>
                                    </select>
                                </div>

                                <div class="form-group has-feedback">
                                    <label>VLC a preco de mercado</label>
                                    <select name="vlc_preco_mercado" class="form-control" style="width: 100%">
                                        <option value="1" <?php echo $config->vlc_preco_mercado == '1' ? 'selected' : '' ?> >Ativo</option>
                                        <option value="0" <?php echo $config->vlc_preco_mercado == '0' ? 'selected' : '' ?> >Inativo</option>
                                    </select>
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Taxa de saque</label>
                                    <select name="vlc_preco_mercado" class="form-control" style="width: 100%">
                                        <option value="1" <?php echo $config->taxa_saque == 'sim' ? 'selected' : '' ?> >Ativo</option>
                                        <option value="0" <?php echo $config->taxa_saque == 'nao' ? 'selected' : '' ?> >Inativo</option>
                                    </select>
                                </div>
                            </div>

                            <div class="tab-pane" id="tab_2">
                                <div class="form-group has-feedback">
                                    <label>Período de saque (dias)</label>
                                    <input name="saque_dias" id="saque_dias" value="<?php echo $config->saque_dias ?>" class="form-control">
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Período de conversão (dias)</label>
                                    <input name="conversao_dias" id="conversao_dias" value="<?php echo $config->conversao_dias ?>" class="form-control">
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Bônus binário (porcentagem)</label>
                                    <input name="binario" id="binario" value="<?php echo $config->binario ?>" class="form-control">
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Bônus indicação (porcentagem)</label>
                                    <input name="indicacao" id="indicacao" value="<?php echo $config->indicacao ?>" class="form-control">
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Bônus diário (porcentagem)</label>
                                    <input name="taxa_diaria" id="taxa_diaria" value="<?php echo $config->taxa_diaria ?>" class="form-control">
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Bônus redimento binário (porcentagem) <span style="color: red">* Lembrando que a porcentagem está fracionário exemplos: 0.10 = 10% </span></label>
                                    <input name="binario_valor" id="binario_valor" value="<?php echo $config->binario_valor ?>" class="form-control">
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Valor da mensalidade</label>
                                    <input name="mensalidade" id="mensalidade" value="<?php echo $config->mensalidade ?>" class="form-control">
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Valor da moeda</label>
                                    <input name="vlc_preco" id="vlc_preco" value="<?php echo $config->vlc_preco ?>" class="form-control">
                                </div>

                                <div class="form-group has-feedback">
                                    <label>Valor da taxa de saque <span style="color: red">(Porcentagem) </span></label>
                                    <input name="valor_taxa_saque" id="valor_taxa_saque" value="<?php echo $config->valor_taxa_saque ?>" class="form-control">
                                </div>
                            </div>
                        </div>
                        <!-- /.tab-content -->
                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Atualizar</button>
                    </div>
                </form>
            </div><!-- /.box -->

        </section>

    </div>
</section>
<!-- /.content -->
@endsection
@section('page-js')

<!-- This is data table -->
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>

<!-- start - This is for export functionality only -->
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('../assets/js/echarts.min.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>

<script>
$(function () {
    $('.data').inputmask("99-99-9999");
    $('.cpf').inputmask("999.999.999-99");
    $('.vlc_preco').inputmask("9.99");
    $("[data-mask]").inputmask();
});
</script>
@endsection
