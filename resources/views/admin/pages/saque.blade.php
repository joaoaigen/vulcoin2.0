@extends('layouts.admin')
@section('title-head')
Relatório de saques
@endsection
@section('title-body')
Relatório de saques
@endsection
@section('page-css')
<link rel="stylesheet" href="{{ url('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.css') }}">
@endsection
@section('main-content')
<section class="content">
    <div class="row">
        <div class="col-12 col-md-12">
            <div class="box box-solid bg-dark">
                <div class="box-header with-border">
                    <h3 class="box-title">Relatório de saques</h3>
                    <h6 class="box-subtitle">Listagem de todos os saques</h6>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="table-responsive">
                        <table id="extratos" class="table table-bordered table-hover display nowrap margin-top-10 w-p100">
                            <thead>
                                <tr>
                                    <th>Usuário</th>
                                    <th>Nome Completo</th>
                                    <th>Wallet</th>
                                    <th>Saldo Anterior</th>
                                    <th>Valor Sacado</th>
                                    <th>Status</th>
                                    <th>Comprovante</th>
                                    <th>Data do Saque</th>
                                    <th>Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach(App\SaqueLog::all()->sortBy("id") as $dados)

                                <tr>
                                    <td>{{$dados->user->username}}</td>
                                    <td>{{$dados->nome_usuario}}</td>
                                    <td>{{$dados->bitzpayer_id}}</td>
                                    <td>{{$dados->saldo_anterior}}</td>
                                    <td>{{$dados->saque_realizado}}</td>
                                    <td>
                                        @if($dados->status == 0)
                                        Cancelado
                                        @elseif($dados->status == 1)
                                        Pendente
                                        @else
                                        Aprovado
                                        @endif
                                    </td>
                                    <td>
                                        @if($dados->comprovante == "")
                                        Sem comprovante
                                        @else
                                        {{$dados->comprovante}}
                                        @endif
                                    </td>
                                    <td>
                                        {{ date("d/m/Y", strtotime($dados->created_at)) }} às {{ date("H:i", strtotime($dados->created_at)) }}
                                    </td>
                                    <td>
                                        @if($dados->status == 0 || $dados->status == 2)
                                        Sem ação
                                        @else
                                        <a href="#aprovarSaque" data-toggle="modal" data-target="#aprovarSaque" class="btn btn-primary btn-sm" role="button" onclick="aprovarSaqueId({{$dados->id}})">Aprovar</a>
                                        <a href="cancelarSaque/{{$dados->id}}" class="btn btn-secondary btn-sm" role="button" >Cancelar</a>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>                    
            </div>  
        </div>
    </div>        
</section>
@endsection
@section('page-js')

<!-- This is data table -->
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/media/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.flash.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/jszip.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/pdfmake.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/ex-js/vfs_fonts.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.html5.min.js') }}"></script>
<script src="{{ asset('../assets/assets/vendor_plugins/DataTables-1.10.15/extensions/Buttons/js/buttons.print.min.js') }}"></script>
<script src="{{ asset('../assets/js/echarts.min.js') }}"></script>
<script type="text/javascript">
$('#extratos').DataTable({
dom: 'Bfrtip',
buttons: [
        'copy', 'csv', 'excel', 'pdf', 'print'
],
});
</script> 
@endsection