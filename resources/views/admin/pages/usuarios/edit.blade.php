@extends('layouts.admin')
@section('title-head')
Meus Dados
@endsection
@section('title-body')
Meus Dados
@endsection
@section('page-css')

@endsection
@section('main-content')
<!-- Main content -->
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="box">
                <div class="box-header box-solid bg-dark">
                    <h3 class="box-title ">Editar usuário: {{ $dados['username'] }}</h3>
                    <div class="box-tools pull-right">
                        <button class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i></button>
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    <form method="post" action="{{url('/admin/usuario/salvar')}}">

                        {{ csrf_field() }}
                        <?php

                        // print_r($dados);

                        function getStatus($status, $nm = '') {
                            if ($nm <> '') {
                                if ($status == 1) {
                                    return 'Sim';
                                } else {
                                    return 'Não';
                                }
                            } else {
                                if ($status == 1) {
                                    return 'Ativo';
                                } else {
                                    return 'Inativo';
                                }
                            }
                        }
                        ?>
                        <div class="form-group">
                            <label>Nome</label>
                            <input type="text" class="form-control" required placeholder="Nome" name="name" value="{{$dados['name']}}" />
                        </div>

                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" required placeholder="" name="email" value="{{$dados['email']}}" />
                        </div>
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" class="form-control"  placeholder="" disabled="" value="{{$dados['username']}}" />
                        </div>
                        <div class="form-group">
                            <label>Administrador </label>
                            <select class="form-control" name="admin" onchange="alert('O usuário terá acesso total ao sistema')">
                                <option value="{{$dados['admin']}}" selected="">{{getStatus($dados['admin'],1)}}</option>
                                <option value="0">Sim</option>
                                <option value="1">Não</option>

                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pago </label>
                            <select class="form-control" name="pago">
                                <option value="{{$dados['pago']}}" selected="">{{getStatus($dados['pago'],1)}}</option>
                                <option value="0">Sim</option>
                                <option value="1">Não</option>
                            </select>
                            <p>*Não gera bonificação</p>
                        </div>
                        <div class="form-group">
                            <label>Ativo </label>
                            <select class="form-control" name="ativo">
                                <option value="{{$dados['ativo']}}" selected="">{{getStatus($dados['ativo'],1)}}</option>
                                <option value="1">Sim</option>
                                <option value="0">Não</option>
                            </select>
                            <p>*Não gera bonificação</p>

                        </div>
                        <div class="form-group has-feedback">
                            <label>Direção da Rede:</label>
                            <select name="direcao" class="form-control">
                                <option {{($dados['ativo'] == 'esquerda') ? 'selected' : '' }} value="esquerda">
                                    Esquerda
                                </option>
                                <option {{($dados['ativo'] == 'direita') ? 'selected' : '' }} value="direita">
                                    Direita
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>Pontos na direita:</label>
                            <input type="text" class="form-control"  name="binario_direita" placeholder=""  value="{{$dados['binario_direita']}}" />
                        </div>
                        <div class="form-group">
                            <label>Pontos na esquerda:</label>
                            <input type="text" class="form-control"  name="binario_esquerda" placeholder=""  value="{{$dados['binario_esquerda']}}" />
                        </div>
                        <div class="form-group has-feedback">
                            <?php
                            $pacotes = App\Pacote::all();
                            $meuPacote = App\Pacote::where('id', $dados['pacote'])->first();
                            ?>
                            <label>Pacote:</label>

                            <select name="pacote" class="form-control">
                                @isset($meuPacote->id)
                                <option value = "{{$meuPacote->id}}" selected="">
                                    {{$meuPacote->nome}}-R${{$meuPacote->valor}}
                                </option>
                                @endisset

                                @foreach($pacotes as $pacote)
                                <option value="{{$pacote->id}}">
                                    {{$pacote->nome}}-R${{$pacote->valor}}
                                </option>
                                @endforeach
                            </select>
                            <p>*Não gera bonificação</p>

                        </div>
                        <div class="form-group">
                            <input type="hidden" class="form-control"   name="id" value="{{$dados['id']}}" />

                            <label>Nova Senha:</label>
                            <input type="password" class="form-control"   name="password" value="" />
                            <p>*Deixe esse campo vazio para manter a senha</p>
                        </div>
                        <div class="form-group">
                            <label>Confirme a senha:</label>
                            <input type="password" class="form-control"   name="password_confirmation" value="" />
                        </div>

                        <div class="form-group">
                            <label>Saldo</label>
                            <input type="text" class="form-control" required placeholder="" value="{{$dados['saldo']}}" name="saldo" id='saldo'/>
                        </div>

                        <div class="form-group">
                            <label>Valor investido</label>
                            <input type="text" class="form-control" required placeholder="" value="{{ empty($dados['valorinvestido']) ? 0 : $dados['valorinvestido']}}" name="valorinvestido" id='valorinvestido'/>
                        </div>

                        <div class="form-group has-feedback">
                            <label>Autenticação em dois fatores</label>
                            <select type="text" class="form-control" name="fa_ativo">
                                <option value="1" {{Auth::user()->fa_ativo == 1 ? 'selected' : ''}}>Sim</option>
                                <option value="0" {{Auth::user()->fa_ativo == 0 ? 'selected' : ''}}>Não</option>
                            </select>
                        </div>

                        <div class="form-group has-feedback">
                            <label>Mensalidade</label>
                            <select type="text" class="form-control" name="mensalidade_ativo">
                                <option value="1" {{Auth::user()->mensalidade_ativo == 1 ? 'selected' : ''}}>Sim</option>
                                <option value="0" {{Auth::user()->mensalidade_ativo == 0 ? 'selected' : ''}}>Não</option>
                            </select>
                        </div>

                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">Atualizar</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
@section('page-js')
<!-- InputMask -->
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.date.extensions.js') }}"></script>
<script src="{{ env('CFURL').('/plugins/input-mask/jquery.inputmask.extensions.js') }}"></script>
<script>
$(function () {
    $('.data').inputmask("99-99-9999");
    $('.cpf').inputmask("999.999.999-99");
    $("[data-mask]").inputmask();
});
function codigoSeg() {
    if (confirm('Tem certeza?Sò continue essa operação se você tiver acesso ao e-mail vinculado a esse conta.')) {

        $.ajax({
            url: 'novaChave', success: function (result) {
                alert(result);
            }
        });
    }
    return false;
}
</script>


<script>
$('#valor').click(function () {
    $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
});
$('#valorinvestido').click(function () {
    $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
});
$('#valorIndicacao').click(function () {
    $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
});
</script>
@endsection

