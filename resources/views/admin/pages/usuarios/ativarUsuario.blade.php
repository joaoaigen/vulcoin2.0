<form class="formAjax" method="post" action="{{url('/admin/usuario/salvar')}}">

    {{ csrf_field() }}
    <?php

    // print_r($dados);

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar Usuário</h4>
    </div>
    <div class="modal-body">

        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" name="name" value="" />
        </div>

        <div class="form-group">
            <label>Email</label>
            <input type="email" class="form-control" required placeholder="" name="email" value="" />
        </div>
        <div class="form-group">
            <label>Username</label>
            <input type="text" class="form-control"  placeholder="" disabled="" value="" />
        </div>
        <div class="form-group">
            <label>Administrador </label>
            <select class="form-control" name="admin" onchange="alert('O usuário terá acesso total ao sistema')">
                <option value="" selected=""></option>
                <option value="0">Sim</option>
                <option value="1">Não</option>

            </select>
        </div>
        <div class="form-group">
            <label>Pago </label>
            <select class="form-control" name="pago">
                <option value="" selected=""></option>
                <option value="0">Sim</option>
                <option value="1">Não</option>
            </select>
            <p>*Não gera bonificação</p>
        </div>
        <div class="form-group">
            <label>Ativo </label>
            <select class="form-control" name="ativo">
                <option value="" selected=""></option>
                <option value="1">Sim</option>
                <option value="0">Não</option>
            </select>
            <p>*Não gera bonificação</p>

        </div>
        <div class="form-group has-feedback">
            <label>Direção da Rede:</label>
            <select name="direcao" class="form-control">
                <option value="esquerda">
                    Esquerda
                </option>
                <option value="direita">
                    Direita
                </option>
            </select>
        </div>
        <div class="form-group">
            <label>Pontos na direita:</label>
            <input type="text" class="form-control"  name="binario_direita" placeholder=""  value="" />
        </div>
        <div class="form-group">
            <label>Pontos na esquerda:</label>
            <input type="text" class="form-control"  name="binario_esquerda" placeholder=""  value="" />
        </div>
        <div class="form-group has-feedback">
           
            <label>Pacote:</label>

            <select name="pacote" class="form-control">
                <option value = "" selected="">
                   
                </option>              
            </select>
            <p>*Não gera bonificação</p>

        </div>
        <div class="form-group">
            <input type="hidden" class="form-control"   name="id" value="" />

            <label>Nova Senha:</label>
            <input type="password" class="form-control"   name="password" value="" />
            <p>*Deixe esse campo vazio para mander a senha</p>
        </div>
        <div class="form-group">
            <label>Confirme a senha:</label>
            <input type="password" class="form-control"   name="password_confirmation" value="" />
        </div>

        <div class="form-group">
            <label>Saldo</label>
            <input type="text" class="form-control" required placeholder="" value="" name="saldo" id='saldo'/>
        </div>

        <div class="form-group">
            <label>Valor investido</label>
            <input type="text" class="form-control" required placeholder="" value="" name="valorinvestido" id='valorinvestido'/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Atualizar</button>
    </div>
</form>
<script>
    $('#valor').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $('#valorinvestido').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $('#valorIndicacao').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
</script>

