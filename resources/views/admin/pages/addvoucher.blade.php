<form class="formAdicionarVouchers" method="post" action="{{url('/admin/vouchers/salvar/'.$usuario->id)}}">
    {{ csrf_field() }}
    {{ method_field('POST') }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Vouchers para: <b>  {{$usuario->name}}</b></h4>

    </div>
    <div class="modal-body">

        <div class="form-group has-feedback">
            <select class="form-control" name="valor" required>
                <option value="">Selecione um Valor</option>
                @foreach($prices as $price)
                    @if(array_key_exists('preco', $price))
                    {
                        <option value="{{ $price->preco }}">{{ $price->nome . ' - R$' . $price->preco }}</option>
                    }
                    @else
                    {
                        <option value="{{ $price->valor }}">{{ $price->nome . ' - R$' . $price->valor }}</option>
                    }
                    @endif
                @endforeach
            </select>
        </div>

        <div class="form-group has-feedback">
            <input type="text" class="form-control" required placeholder="Quantidade" name="quantidade"/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </div>
</form>