<form class="formAjax" method="post" action="{{url('/admin/avisos')}}">
    {{ csrf_field() }}
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Aviso</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Conteúdo</label>
            <textarea name="conteudo">
            </textarea>
        </div>

        <div class="form-group">
         <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    <button type="submit" class="btn btn-primary">Adicionar</button>
</div>
</form>