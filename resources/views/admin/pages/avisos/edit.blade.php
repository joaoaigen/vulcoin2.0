<form class="formAjax" method="post" action="{{url('/admin/avisos/'.$dados->id)}}">
{{ Form::open(array('url' => url('/admin/avisos/'.$dados->id), 'method' => 'PUT', 'class'=>'formAjax')) }}

    {{ csrf_field() }}
     <?php

    function getStatus($status, $nm = '') {
        if ($nm <> '') {
            if ($status == 1) {
                return 'Sim';
            } else {
                return 'Não';
            }
        } else {
            if ($status == 1) {
                return 'Ativo';
            } else {
                return 'Inativo';
            }
        }
    }
    ?>
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar Aviso</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Conteúdo</label>
            <textarea name="conteudo">
            <?=$dados->conteudo?>
            </textarea>
        </div>

        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option selected="" value="<?= $dados->status ?>"><?= getStatus($dados->status) ?></option>
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
    <button type="submit" class="btn btn-primary">Salvar</button>
</div>
{{ Form::close() }}
