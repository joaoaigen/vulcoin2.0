<form class="formAjax" method="post" action="{{url('/admin/graduacoes')}}">

    {{ csrf_field() }}

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Adicionar Graduação</h4>

    </div>
    <div class="modal-body">
        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" name="name"/>
        </div>
        <div class="form-group">
            <label>Texto de boas vindas</label>
            <textarea name="boas_vindas" >

            </textarea>
        </div>

        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
        <div class="form-group">
            <label>Teto</label>
            <input type="number" step="any" class="form-control" required placeholder="Teto" name="teto"/>
        </div>
        <div class="form-group">
            <label>Pontuação necessária</label>
            <input type="number" class="form-control" required placeholder="Pontuação necessária" name="pontuacao"/>
        </div><?php
        $graduacoes = App\graduacoes::where('status', 1)->get();
        ?>
        <div class="form-group">
            <label>Graduação Requerida </label>
            <select class="form-control" name="grad_required" required="">
                <option value ='0'>Indicados diretos</option>
                <?php
                foreach ($graduacoes as $value) {

                    echo "<option value ='{$value['id']}'>{$value['name']}</option>";
                }
                ?>

            </select>
        </div>
        <div class="form-group">
            <label>Quantidade de pessoas</label>
            <input type="number" class="form-control" required placeholder="" name="qntd_required"/>
        </div>
          <div class="form-group">
            <label>Graduação Requerida(1) </label>
            <select class="form-control" name="grad_required1" required="">
                <option value ='0'>Indicados diretos</option>
                <?php
                foreach ($graduacoes as $value) {

                    echo "<option value ='{$value['id']}'>{$value['name']}</option>";
                }
                ?>

            </select>
        </div>
        <div class="form-group">
            <label>Quantidade de pessoas(1)</label>
            <input type="number" class="form-control" required placeholder="" name="qntd_required1"/>
        </div>
        <div class="form-group">
            <label>Ícone</label>
            <input type="url" class="form-control" required placeholder="Ícone" name="icone"/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Adicionar</button>
    </div>
</form>
<script>
    $('#valor').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $('#valorIndicacao').click(function () {
        $(this).maskMoney({prefix: 'R$ ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
</script>
