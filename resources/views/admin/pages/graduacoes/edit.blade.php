
<?php

function getStatus($status, $nm = '') {
    if ($nm <> '') {
        if ($status == 1) {
            return 'Sim';
        } else {
            return 'Não';
        }
    } else {
        if ($status == 1) {
            return 'Ativo';
        } else {
            return 'Inativo';
        }
    }
}
?>
<form class="formAjax" method="post" action="{{url('admin/graduacao/salvar')}}">
    {{ csrf_field() }}

    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">Editar Graduação</h4>

    </div>
<div class="modal-body">
        <input type="hidden" class="form-control"  placeholder="Nome" name="id" value="<?= $dados['id'] ?>"/>

        <div class="form-group">
            <label>Nome</label>
            <input type="text" class="form-control" required placeholder="Nome" name="name" value="<?= $dados['name'] ?>"/>
        </div>
        <div class="form-group">
            <label>Texto de boas vindas</label>
            <textarea name="boas_vindas" >
                <?= $dados['boas_vindas'] ?> 
            </textarea>

        </div>

        <div class="form-group">
            <label>Status </label>
            <select class="form-control" name="status">
                <option selected="" value="<?= $dados['status'] ?>"><?= getStatus($dados['status']) ?></option>
                <option value="1">Ativo</option>
                <option value="0">Inativo</option>
            </select>
        </div>
        <div class="form-group">
            <label>Teto</label>
            <input type="number" step="any" class="form-control" required placeholder="Teto" name="teto" value="<?= $dados['teto'] ?>"/>
        </div>
        <div class="form-group">
            <label>Pontuação necessária</label>
            <input type="number" class="form-control" required placeholder="Pontuação necessária" name="pontuacao" value="<?= $dados['pontuacao'] ?>"/>
        </div>
        <?php
        $graduacoes = App\graduacoes::where('status', 1)->get();
        ?>
        <div class="form-group">
            <label>Graduação Requerida </label>
            <select class="form-control" name="grad_required" required="">
                <option value ='0'>Indicados diretos</option>
                <?php
                foreach ($graduacoes as $value) {
                    if ($value['id'] == $dados['grad_required']) {
                        echo "<option value ='{$value['id']}' selected=''>{$value['name']}</option>";
                    } else {
                        echo "<option value ='{$value['id']}'>{$value['name']}</option>";
                    }
                }
                ?>

            </select>
        </div>
        <div class="form-group">
            <label>Quantidade de pessoas</label>
            <input type="number" class="form-control" required placeholder="" value="<?= $dados['qntd_required'] ?>" name="qntd_required"/>
        </div>
        <div class="form-group">
            <label>Graduação Requerida(1) </label>
            <select class="form-control" name="grad_required1" required="">
                <option value ='0'>Indicados diretos</option>
                <?php
                foreach ($graduacoes as $value) {
                    if ($value['id'] == $dados['grad_required1']) {
                        echo "<option value ='{$value['id']}' selected=''>{$value['name']}</option>";
                    } else {
                        echo "<option value ='{$value['id']}'>{$value['name']}</option>";
                    }
                }
                ?>

            </select>
        </div>
        <div class="form-group">
            <label>Quantidade de pessoas(1)</label>
            <input type="number" class="form-control" required placeholder="" value="<?= $dados['qntd_required1'] ?>" name="qntd_required1"/>
        </div>
        <div class="form-group">
            <label>Ícone</label>
            <input type="url" class="form-control" required placeholder="Ícone" name="icone" value="<?= $dados['icone'] ?>"/>
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
        <button type="submit" class="btn btn-primary">Salvar</button>
    </div>

</form>
<script>
    $('#valor').click(function () {
        $(this).maskMoney({prefix: 'BTC ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
    $('#valorIndicacao').click(function () {
        $(this).maskMoney({prefix: 'BTC ', allowNegative: false, thousands: '.', decimal: ',', affixesStay: false});
    });
</script>


